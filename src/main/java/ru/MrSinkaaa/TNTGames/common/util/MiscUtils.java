
package ru.MrSinkaaa.TNTGames.common.util;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.projectiles.ProjectileSource;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.misc.MetadataKeys;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author MrSinkaaa
 */
public class MiscUtils {
    
    private static Logger LOGGER;
    private static List<Material> MATERIAL_VALUES;

    
    static {
        LOGGER = LollipopTNTGames.getInstance().getLogger();

    }
    
    // logging
    
    public static void info(String message, Object ... args) {
        LOGGER.info(String.format(message, args));
    }
    
    public static void warning(String message, Object ... args) {
        LOGGER.warning(String.format(message, args));
    }
    
    public static void exception(Throwable exception) {
        StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        Bukkit.getLogger().warning(sw.toString());
    }

    //sync with bukkit

    /**
     * Run code sync in bukkit primary thread
     *
     * @param runnable code to run
     */

    public static void runBukkitSync(Runnable runnable) {
        if(Bukkit.getServer().isPrimaryThread())
            runnable.run();
        else
            try {
                Bukkit.getScheduler().callSyncMethod(LollipopTNTGames.getInstance(), () -> {
                    runnable.run();
                    return null;
                }).get();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
    }

    /**
     * Run code sync in bukkit primary thread and return a value
     *
     * @param supplier code to run with result value
     */
    public static <T> T supplyBukkitSync(Supplier<T> supplier) {
        if(Bukkit.getServer().isPrimaryThread()) {
            return supplier.get();
        } else {
            try {
                return Bukkit.getScheduler().callSyncMethod(LollipopTNTGames.getInstance(), supplier::get).get();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    //sneaky throw(rethrow exceptions)

    /**
     *
     * Sneaky throw any type of Throwable.
     */

    public static <T> T sneakyThrow(Throwable throwable) {
        MiscUtils.<RuntimeException>sneakyThrow0(throwable);
        throw new RuntimeException();
    }

    /**
     * Sneaky throw any type of Throwable.
     */
    private static <E extends Throwable> void sneakyThrow0(Throwable throwable) throws E {
        throw (E) throwable;
    }

    // position

    /**
     * Make position normalized:<br>
     * - Coords are ending with .5
     * - Yaw and Pitch are set to closest value (8 sides)
     *
     * @param position position
     * @return new normalized position
     */
    public static Position normalizePosition(Position position) {
        double x = (int) position.getX() + 0.5;
        double y = (int) position.getY() + 0.5;
        double z = (int) position.getZ() + 0.5;

        float yaw = Location.normalizeYaw(position.getYaw());
        yaw = Math.round(yaw / 45) * 45;

        float pitch = Location.normalizePitch(position.getPitch());
        pitch = Math.round(pitch / 45) * 45;

        return new Position(position.getWorldName(), x, y, z, yaw, pitch);
    }

    // duration formatting

    /**
     * Get number ending format, variations example:<br>
     * > секунда, секунды, секунд<br>
     * > минута, минуты, минут
     *
     * @param num number
     * @param variations variations
     * @return format
     */
    public static String getNumberEndingFormat(int num, String ... variations) {
        int type = getNumberEndingType(num);
        return variations[type];
    }

    /**
     * Get number ending type:<br>
     * 0 - (1) секунда<br>
     * 1 - (3) секунды<br>
     * 2 - (5) секунд
     *
     * @param num number
     * @return type
     */
    public static int getNumberEndingType(int num) {
        int lastTwo = num % 100;
        if (lastTwo > 10 && lastTwo < 15) return 2;

        int lastOne = num % 10;
        if (lastOne == 0 || lastOne > 4) return 2;
        if (lastOne == 1) return 0;
        else return 1; // lastOne > 1
    }

    // metadata

    public static void setMetadata(Metadatable metadatable, String key, Object value) {
        LollipopTNTGames plugin = LollipopTNTGames.getInstance();
        metadatable.setMetadata(key, new FixedMetadataValue(plugin, value));
    }

    @SuppressWarnings("unchecked")
    public static <T> T getMetadata(Metadatable metadatable, String key, Class<T> type) {
        LollipopTNTGames plugin = LollipopTNTGames.getInstance();
        List<MetadataValue> values = metadatable.getMetadata(key);
        if (values == null || values.isEmpty()) return null;
        for (MetadataValue value : values) {
            if (value.getOwningPlugin() == plugin)
                return (T) value.value();
        }
        return null;
    }

    public static void removeMetadata(Metadatable metadatable, String key) {
        LollipopTNTGames plugin = LollipopTNTGames.getInstance();
        metadatable.removeMetadata(key, plugin);
    }

    public static boolean hasMetadata(Metadatable metadatable, String key) {
        return getMetadata(metadatable, key, Object.class) != null;
    }

    // misc

    public static int getPermissionLimit(Player player, String permission) {
        if (player.hasPermission(permission + "unlimited"))
            return Integer.MAX_VALUE;

        int max = 0;
        for (PermissionAttachmentInfo info : player.getEffectivePermissions()) {
            String p = info.getPermission();
            if (!p.startsWith(permission)) continue;

            String data = p.substring(permission.lastIndexOf('.') + 1);
            try {
                int limit = Integer.parseInt(data);
                if (limit > max) max = limit;
            } catch (NumberFormatException ex) { /**/ }
        }

        return max;
    }

    public static double getDoublePermissionLimit(Player player, String permission) {
        double max = 0;
        for (PermissionAttachmentInfo info : player.getEffectivePermissions()) {
            String p = info.getPermission();
            if (!p.startsWith(permission)) continue;

            var data = p.substring(permission.lastIndexOf('.') + 1);
            try {
                double limit = Double.parseDouble(data);
                if (limit > max) max = limit;
            } catch (NumberFormatException ex) { /**/ }
        }

        return max;
    }

    public static List<Material> resolveMaterials(List<Pattern> patterns) {
        Pattern[] patternsArr = patterns.toArray(new Pattern[0]);
        return resolveMaterials(patternsArr);
    }

    public static List<Material> resolveMaterials(Pattern... patterns) {
        List<Material> result = new LinkedList<>();

        Matcher[] matchers = new Matcher[patterns.length];
        for (int i = 0; i < patterns.length; i++)
            matchers[i] = patterns[i].matcher("");

        for (Material material : MATERIAL_VALUES) {
            for (int i = 0; i < patterns.length; i++) {
                Matcher matcher = matchers[i];
                matcher.reset(material.name());
                if (matcher.find()) result.add(material);
            }
        }
        return result;
    }

    public static GamePlayer getDamagerFromSource(Entity damager) {
        if (damager instanceof Projectile) {
            ProjectileSource source = ((Projectile) damager).getShooter();
            if (source instanceof Player) {
                return LollipopTNTGames.getInstance().getPlayer((Player) source);
            } else return null;
        } else if (damager instanceof Player) {
            return LollipopTNTGames.getInstance().getPlayer((Player) damager);
        } else return null;
    }

    public static double distance(Location locFrom, Location locTo) {
        if (locFrom.getWorld() != locTo.getWorld() || locFrom.getWorld() == null) return -1;
        else return locFrom.distance(locTo);
    }

    public static double distance(Player playerFrom, Player playerTo) {
        return distance(playerFrom.getLocation(), playerTo.getLocation());
    }

    public static List<Player> getPlayersNearby(Location location, double radius) {
        return MiscUtils.supplyBukkitSync(() -> {
            List<Player> players = new ArrayList<>();
            double radiusSq = radius * radius;

            World world = location.getWorld();
            for (Player player : world.getPlayers()) {
                double distanceSq = player.getLocation().distanceSquared(location);
                if (distanceSq <= radiusSq)
                    players.add(player);
            }

            return players;
        });
    }

    public static void sendParticleAsync(Location location, ParticleBuilder builder) {
        sendParticleAsync(location, 128, builder);
    }

    public static void sendParticleAsync(Location location, double radius, ParticleBuilder builder) {
        builder.location(location);
        if (builder.receivers() == null)
            builder.receivers(getPlayersNearby(location, radius));
        ExecutorUtils.PARTICLES.submit(builder::spawn);
    }

    public static String toLowerEn(String str) {
        return str.toLowerCase(Locale.ENGLISH);
    }

    public static String toUpperEn(String str) {
        return str.toUpperCase(Locale.ENGLISH);
    }

    public static void setDamagerMarker(GamePlayer target, GamePlayer damager) {
        setMetadata(target, MetadataKeys.PLAYER_DAMAGER, damager);
        setMetadata(target, MetadataKeys.PLAYER_DAMAGE_TIME, System.currentTimeMillis());
    }
}
