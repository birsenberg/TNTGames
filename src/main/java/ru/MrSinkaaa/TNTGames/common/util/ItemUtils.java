package ru.MrSinkaaa.TNTGames.common.util;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import ua.i0xhex.lib.nbtapi.NBTItem;

import java.util.*;

/**
 *
 * @author MrSinkaaa
 */
public class ItemUtils {
    
    public static final String NBT_ID_NAME = "tntgames_item_id";
    
    
    public static ItemStack createItem(Material type) {
        return createItem(type, 1);
    }
    
    public static ItemStack createItem(Material type, int amount) {
        return builder(type, amount).build();
    }
    
    public static ItemStack createItem(Material type, int amount, int damage) {
        return builder(type, amount, damage).build();
    }
    
    public static ItemStack createItem(Material type, String name, String ... lore) {
        return builder(type, 1)
                .setLore(Arrays.asList(lore))
                .setName(name)
                .build();
    }
    
    public static ItemStack createItem(Material type, int amount, String name, String ... lore) {
        return builder(type, amount)
                .setLore(Arrays.asList(lore))
                .setName(name)
                .build();
    }
    
    public static ItemStack createItem(Material type, int amount, int damage, String name, String ... lore) {
        return builder(type, amount)
                .setDamage(damage)
                .setLore(Arrays.asList(lore))
                .setName(name)
                .build();
    }
    
    // builder
    
    public static Builder builder(Material type) {
        return builder(type, 1);
    }
    
    public static Builder builder(Material type, int amount) {
        return new Builder(type, amount);
    }
    
    public static Builder builder(Material type, int amount, int damage) {
        return new Builder(type, amount, damage);
    }
    
    public static Builder builder(ItemStack itemStack) {
        return new Builder(itemStack);
    }
    
    // nbt
    
    public static String getId(ItemStack item) {
        if (item == null || item.getType() == Material.AIR) 
            return null;
        
        NBTItem nbti = new NBTItem(item);
        if (nbti.hasKey(NBT_ID_NAME)) return nbti.getString(NBT_ID_NAME);
        else return null;
    }
    
    public static void setId(ItemStack item, String id) {
        NBTItem nbti = new NBTItem(item, true);
        if (id == null) nbti.removeKey(NBT_ID_NAME);
        else nbti.setString(NBT_ID_NAME, id);
    }
    
    public static class Builder {
        
        private ItemStack item;
        private ItemMeta meta;
        
        private String id;
        
        public Builder(Material type, int amount) {
            this.item = new ItemStack(type, amount);
            this.meta = this.item.getItemMeta();
        }
        
        public Builder(Material type, int amount, int damage) {
            this.item = new ItemStack(type, amount);
            this.meta = this.item.getItemMeta();
            
            if (this.meta instanceof Damageable)
                ((Damageable) this.meta).setDamage(damage);
        }
        
        public Builder(ItemStack itemStack) {
            this.item = itemStack.clone();
            this.meta = this.item.getItemMeta();
        }
        
        public Builder setItem(ItemStack item) {
            this.item = item;
            this.meta = item.getItemMeta();
            return this;
        }
        
        public Builder setType(Material type) {
            this.item.setType(type);
            this.meta = this.item.getItemMeta();
            return this;
        }
        
        public Builder setDamage(int damage) {
            if (this.meta instanceof Damageable)
                ((Damageable) this.meta).setDamage(damage);
            return this;
        }
        
        public Builder setName(String name) {
            meta.setDisplayName(name);
            return this;
        }
        
        public Builder setLore(String ... lore) {
            if (lore == null) meta.setLore(null);
            else meta.setLore(Arrays.asList(lore));
            return this;
        }
        
        public Builder setLore(Collection<? extends String> lore) {
            if (lore == null) meta.setLore(null);
            else meta.setLore(new ArrayList<>(lore));
            return this;
        }
        
        public Builder setLoreBlock(String lore) {
            if (lore == null) meta.setLore(null);
            else meta.setLore(Arrays.asList(lore.split("\n")));
            return this;
        }
        
        public Builder setCustomModelData(int data) {
            meta.setCustomModelData(data);
            return this;
        }
        
        public Builder setId(String id) {
            this.id = id;
            return this;
        }
        
        public Builder addLore(String... lines) {
            List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
            Collections.addAll(lore, lines);
            meta.setLore(lore);
            return this;
        }
        
        public Builder addEnchantment(Enchantment enchantment, int level) {
            meta.addEnchant(enchantment, level, true);
            return this;
        }
        
        public Builder addFlags(ItemFlag... itemFlags) {
            meta.addItemFlags(itemFlags);
            return this;
        }
        
        // type specified
        
        // -> potions
        
        public Builder setMainPotionEffect(PotionType potionType, boolean extended, boolean upgraded) {
            if (!(meta instanceof PotionMeta)) return this;
            PotionMeta potionMeta = (PotionMeta) meta;
            potionMeta.setBasePotionData(new PotionData(potionType, extended, upgraded));
            return this;
        }
        
        public Builder addPotionEffect(PotionEffectType effectType, int duration, int amplifier) {
            if (!(meta instanceof PotionMeta)) return this;
            PotionMeta potionMeta = (PotionMeta) meta;
            potionMeta.addCustomEffect(new PotionEffect(effectType, duration, amplifier), true);
            return this;
        }
        
        // functions
        
        public Builder removeLoreBlankLines() {
            List<String> lore = meta.getLore();
            if (lore != null) {
                boolean changed = false;
                Iterator<String> loreIt = lore.iterator();
                while (loreIt.hasNext()) {
                    String line = loreIt.next();
                    if (line.isBlank()) {
                        loreIt.remove();
                        changed = true;
                    }
                }
                
                if (changed) meta.setLore(lore);
            }
            
            return this;
        }
        
        public Builder removeNameIfBlank() {
            String name = meta.getDisplayName();
            if (name != null && name.isBlank())
                meta.setDisplayName(null);
            return this;
        }
        
        public Builder removeLoreIfBlank() {
            List<String> lore = meta.getLore();
            if (lore == null) return this;
            
            boolean blank = true;
            for (String line : lore) {
                if (!line.isBlank()) {
                    blank = false;
                    break;
                }
            }
            
            if (blank) meta.setLore(null);
            return this;
        }
        
        public ItemStack build() {
            item.setItemMeta(meta);
            
            if (id != null) {
                NBTItem nbti = new NBTItem(item);
                nbti.setString(NBT_ID_NAME, id);
                item = nbti.getItem();
            }
            
            return item.clone();
        }
    }
}

