/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.MrSinkaaa.TNTGames.common.util;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.bukkit.Bukkit;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 *
 * @author MrSinkaaa
 */
public class ExecutorUtils {

    public static Sideboard SIDEBOARD;
    public static Particles PARTICLES;

    public static void init() {
        SIDEBOARD = new Sideboard();
        PARTICLES = new Particles();
    }

    // classes
    public static class Sideboard extends AbstractBulkExecutor {

        public Sideboard() {
            super("tntgames-sideboard");
        }
    }

    public static class Particles extends AbstractBulkExecutor {

        public Particles() {
            super("tntgames-particles");
        }
    }

    private static abstract class AbstractBulkExecutor {

        private final Queue<Runnable> tasks = new LinkedList<>();

        public AbstractBulkExecutor(String threadName) {
            ThreadFactory factory = new ThreadFactoryBuilder()
                    .setNameFormat(threadName + "-%s")
                    .build();
            ExecutorService executor = Executors.newCachedThreadPool(factory);

            // schedule tasks each tick
            Bukkit.getScheduler().runTaskTimer(LollipopTNTGames.getInstance(), () -> {
                Queue<Runnable> tasksToRun;
                synchronized (tasks) {
                    tasksToRun = new LinkedList<>(tasks);
                    tasks.clear();
                }

                executor.submit(() -> {
                    try {
                        for (Runnable runnable : tasksToRun) {
                            runnable.run();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                });
            }, 0L, 1L);
        }

        public void submit(Runnable runnable) {
            synchronized (tasks) {
                tasks.add(runnable);
            }
        }
    }
}
