package ru.MrSinkaaa.TNTGames.common.util;

import org.bukkit.entity.Player;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author MrSinkaaa
 */
public class InvUtils {
    
    // inventory space
    
    public static int getInventorySpace(ItemStack item, Player player) {
        return getInventorySpace(item, player.getInventory(), 0, 36);
    }
    
    public static int getInventorySpace(ItemStack item, Inventory inventory, int slotFrom, int slotTo) {
        int maxStackSize = item.getMaxStackSize();
        
        int amount = 0;
        for (int slot = slotFrom; slot < slotTo; slot++) {
            ItemStack inventoryItem = inventory.getItem(slot);
            if (inventoryItem == null) {
                amount += maxStackSize;
            } else if (inventoryItem.isSimilar(item)) {
                amount += (maxStackSize - inventoryItem.getAmount());
            }
        }
        
        return amount / item.getAmount();
    }
    
    // clean
    
    /**
     * Clear player inventory, including some parts, not 
     * accessible by "inventory.clear();"
     * 
     * @param player player
     */
    public static void clearInventory(Player player) {
        // clear cursor
        player.setItemOnCursor(null);
        
        // clear base inventory
        player.getInventory().clear();
        
        // clear crafting table
        var inventory = player.getOpenInventory().getTopInventory();
        if (inventory instanceof CraftingInventory) {
            for (int i = 0; i < 5; i++)
                inventory.setItem(i, null);
        }
    }
}

