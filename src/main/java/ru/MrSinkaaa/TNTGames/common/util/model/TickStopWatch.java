package ru.MrSinkaaa.TNTGames.common.util.model;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author MrSinkaaa
 */
public class TickStopWatch {
    
    private LollipopTNTGames plugin;
    private int tick;
    private BukkitTask task;
    
    private List<Consumer<Integer>> triggersPerTick;
    private List<Consumer<Integer>> triggersPerSecond;
    
    public TickStopWatch(LollipopTNTGames plugin) {
        this.plugin = plugin;
        
        this.triggersPerTick = new LinkedList<>();
        this.triggersPerSecond = new LinkedList<>();
    }
    
    // getters
    
    public int getTick() {
        return tick;
    }
    
    // getters : func
    
    public int getSecond() {
        return tick / 20;
    }
    
    // setters
    
    public void setTick(int tick) {
        this.tick = tick;
    }
    
    // func
    
    public void start() {
        stop();
        task = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            for (Consumer<Integer> trigger : triggersPerTick)
                trigger.accept(tick);
            
            if (tick % 20 == 0) {
                int second = tick / 20;
                for (Consumer<Integer> trigger : triggersPerSecond)
                    trigger.accept(second);
            }
            
            tick++;
        }, 0L, 1L);
    }
    
    public void stop() {
        cancel();
    }
    
    public void reset() {
        tick = 0;
    }
    
    public void addTriggerPerTick(Consumer<Integer> trigger) {
        triggersPerTick.add(trigger);
    }
    
    public void addTriggerPerSecond(Consumer<Integer> trigger) {
        triggersPerSecond.add(trigger);
    }
    
    public void clearTriggersPerTick() {
        triggersPerTick.clear();
    }
    
    public void clearTriggersPerSecond() {
        triggersPerSecond.clear();
    }
    
    // private
    
    private void cancel() {
        if (task != null) {
            task.cancel();
            task = null;
        }
    }
}
