package ru.MrSinkaaa.TNTGames.common.debug;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author MrSinkaaa
 */
public enum DebugType {
    STATE,
    SHOP(true);
    
    private boolean temporary;
    
    private DebugType() {}
    
    private DebugType(boolean temporary) {
        this.temporary = temporary;
    }
    
    // getters
    
    public boolean isTemporary() {
        return temporary;
    }
    
    // static
    
    private static Map<String, DebugType> VALUES;
    
    static {
        VALUES = new HashMap<>();
        for (DebugType value : values())
            VALUES.put(value.name(), value);
    }
    
    public static DebugType get(String name) {
        return VALUES.get(name.toUpperCase());
    }
}
