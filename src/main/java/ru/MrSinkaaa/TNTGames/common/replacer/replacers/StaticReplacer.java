package ru.MrSinkaaa.TNTGames.common.replacer.replacers;

import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;

import java.util.HashMap;
import java.util.Map;

public class StaticReplacer extends Replacer {

    private Map<String, Object> mapping = new HashMap<>();

    public StaticReplacer() {
        //
    }

    // func

    @Override
    public String apply(String str) {
        return replace(str, mapping);
    }

    @Override
    public String apply(String str, Object context) {
        return apply(str);
    }

    // modifiers

    public StaticReplacer set(String placeholder, Object replacement) {
        mapping.put(placeholder, String.valueOf(replacement));
        return this;
    }
}
