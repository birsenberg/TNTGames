package ru.MrSinkaaa.TNTGames.common;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;

public abstract class Manager {
    
    protected LollipopTNTGames plugin;
    
    public Manager(LollipopTNTGames plugin) {
        this.plugin = plugin;
    }
    
    // func
    
    public void onEnable() {}
    
    public void onDisable() {}
    
    public void onReload() {}
    
    // getters
    
    public LollipopTNTGames getPlugin() {
        return plugin;
    }
}
