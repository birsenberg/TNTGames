package ru.MrSinkaaa.TNTGames.common;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;


public abstract class BukkitListener implements Listener {
    
    protected LollipopTNTGames plugin;
    
    public BukkitListener(LollipopTNTGames plugin) {
        this.plugin = plugin;
    }
    
    public void register() {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
    
    public void unreginter() {
        HandlerList.unregisterAll(this);
    }
    
    public LollipopTNTGames getPlugin() {
        return plugin;
    }
}
