/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.MrSinkaaa.TNTGames.common.action;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author MrSinkaaa
 */
public enum ActionType {
    
    PLAYER_COMMAND,
    CONSOLE_COMMAND, 
    KICK;
    
    // static
    
    private static Map<String, ActionType> VALUES;
    
    static {
        VALUES = new HashMap<>();
        for (ActionType value : values())
            VALUES.put(value.name(), value);
    }
    
    public static ActionType get(String name) {
        return VALUES.get(name.toUpperCase());
    }
}
