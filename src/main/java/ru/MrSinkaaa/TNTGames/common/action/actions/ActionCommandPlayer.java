/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.MrSinkaaa.TNTGames.common.action.actions;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.common.action.Action;

/**
 *
 * @author MrSinkaaa
 */
public class ActionCommandPlayer extends Action {
    
    private String command;

    public ActionCommandPlayer(String command) {
        this.command = command;
    }

    @Override
    public void execute(Player p) {
        if(p != null) {
            p.chat("/" + command);
        }
    }
    
    public static ActionCommandPlayer fromConfig(ConfigurationSection section) {
        String command = section.getString("command");
        return new ActionCommandPlayer(command);
    }
    
}
