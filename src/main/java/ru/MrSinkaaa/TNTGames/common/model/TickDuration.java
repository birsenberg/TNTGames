package ru.MrSinkaaa.TNTGames.common.model;

public class TickDuration {

    private double seconds;
    private int ticks;

    private TickDuration() {}

    // getters

    public double getSeconds() {
        return seconds;
    }

    public int getTicks() {
        return ticks;
    }

    // static

    public static TickDuration fromSeconds(double seconds) {
        TickDuration duration = new TickDuration();
        duration.seconds = seconds;
        duration.ticks = (int) (seconds * 20);
        return duration;
    }

    public static TickDuration fromTicks(int ticks) {
        TickDuration duration = new TickDuration();
        duration.ticks = ticks;
        duration.seconds = ((double) (ticks * 100 / 20)) / 100.0;
        return duration;
    }
}
