package ru.MrSinkaaa.TNTGames.common.model;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.NumberConversions;

public class Pos {

    private double x;
    private int y;
    private double z;

    public Pos(double x, int y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Block getBlock(World world, double ax, double az) {
        return world.getBlockAt(NumberConversions.floor(x + ax), y, NumberConversions.floor(z + az));
    }
}
