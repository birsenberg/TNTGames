package ru.MrSinkaaa.TNTGames.common.model;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.NumberConversions;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;

import java.util.Locale;
import java.util.Objects;


public class Position {

    private String worldName;
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    public Position(Position other) {
        this(other.worldName, other.x, other.y, other.z, other.yaw, other.pitch);
    }

    public Position(String worldName, double x, double y, double z) {
        this(worldName, x, y, z, 0, 0);
    }

    public Position(String worldName, double x, double y, double z, double yaw, double pitch) {
        this.worldName = worldName;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = (float) yaw;
        this.pitch = (float) pitch;
    }

    //func
    public Location toLocation() {
        World world = Bukkit.getWorld(worldName);
        if(world == null) throw new RuntimeException("World " + worldName + " not loaded.");
        return new Location(world, x, y, z, yaw, pitch);
    }

    public BlockPosition toBlockPosition() {
        int x = toBlockCoord(this.x);
        int y = toBlockCoord(this.y);
        int z = toBlockCoord(this.z);
        return new BlockPosition(worldName, x, y, z);
    }

    public Position add(double x, double y, double z) {
        this.x = this.x + x;
        this.y = this.y + y;
        this.z = this.z + z;
        return this;
    }

    public Position substract(double x, double y, double z) {
        return add(-x, -y, -z);
    }

    public double distance(Position position) {
        return Math.sqrt(distanceSquared(position));
    }

    public double distanceSquared(Position position) {
        if(!worldName.equals(position.worldName)) return 0;
        return NumberConversions.square(x - position.x)
              + NumberConversions.square(y - position.y)
              + NumberConversions.square(z - position.z);
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    public Position clone() {
        return new Position(this);
    }

    public String serialize() {
        return serialize(true);
    }

    public String serialize(boolean includeYawPitch) {
        if (includeYawPitch)
            return String.format(Locale.US, "%s %.2f %.2f %.2f %.2f %.2f", worldName, x, y, z, yaw, pitch);
        else
            return String.format(Locale.US, "%s %.2f %.2f %.2f", worldName, x, y, z);
    }

    //getters
    public String getWorldName() {
        return worldName;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float getYaw() {
        return yaw;
    }

    public float getPitch() {
        return pitch;
    }

    //private
    private int toBlockCoord(double coord) {
        final int floor = (int) coord;
        return floor == coord ? floor : floor - (int) (Double.doubleToRawLongBits(coord) >>> 63);
    }

    //object

    @Override
    public String toString() {
        return "{" + serialize(true) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return
              Double.compare(position.x, x) == 0 &&
                    Double.compare(position.y, y) == 0 &&
                    Double.compare(position.z, z) == 0 &&
                    Float.compare(position.yaw, yaw) == 0 &&
                    Float.compare(position.pitch, pitch) == 0 &&
                    worldName.equals(position.worldName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(worldName, x, y, z, yaw, pitch);
    }

    // static

    public static Position fromLocation(Location location) {
        String worldName = location.getWorld().getName();
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        float yaw = location.getYaw();
        float pitch = location.getPitch();

        return new Position(worldName, x, y, z, yaw, pitch);
    }

    public static Position deserialize(String str) {
        try {
            String[] data = str.split(" ");

            String worldName = data[0];
            double x = Double.parseDouble(data[1]);
            double y = Double.parseDouble(data[2]);
            double z = Double.parseDouble(data[3]);

            float yaw = 0;
            float pitch = 0;

            if (data.length > 4) {
                yaw = Float.parseFloat(data[4]);
                pitch = Float.parseFloat(data[5]);
            }

            return new Position(worldName, x, y, z, yaw, pitch);
        } catch (Exception ex) {
            MiscUtils.warning("Failed to deserialize position '%s'.", str);
            throw new RuntimeException(ex);
        }
    }
}
