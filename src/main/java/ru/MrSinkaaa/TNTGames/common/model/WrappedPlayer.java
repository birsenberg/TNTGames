package ru.MrSinkaaa.TNTGames.common.model;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.UnaryOperator;
import java.util.stream.Collector;

import com.destroystokyo.paper.ClientOption;
import com.destroystokyo.paper.Title;
import com.destroystokyo.paper.block.TargetBlockInfo;
import com.destroystokyo.paper.entity.TargetEntityInfo;
import com.destroystokyo.paper.profile.PlayerProfile;

import org.bukkit.*;
import org.bukkit.advancement.Advancement;
import org.bukkit.advancement.AdvancementProgress;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.PistonMoveReaction;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.entity.*;
import org.bukkit.entity.memory.MemoryKey;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.*;
import org.bukkit.map.MapView;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.audience.ForwardingAudience;
import net.kyori.adventure.audience.MessageType;
import net.kyori.adventure.bossbar.BossBar;
import net.kyori.adventure.identity.Identified;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.inventory.Book;
import net.kyori.adventure.sound.SoundStop;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.ComponentLike;
import net.kyori.adventure.text.event.HoverEvent;
import net.kyori.adventure.text.event.HoverEventSource;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;

public class WrappedPlayer implements CommandSender, Audience, Player {

    protected Player handle;

    public WrappedPlayer(Player handle) {
        this.handle = handle;
    }

    public Player getHandle() {
        return handle;
    }

    // func

    public Player unwrap(Player player) {
        if (player instanceof WrappedPlayer) {
            WrappedPlayer wrapped = (WrappedPlayer) player;
            return wrapped.getHandle();
        } else return player;
    }

    // object

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WrappedPlayer)) return false;
        WrappedPlayer that = (WrappedPlayer) o;
        return getUniqueId().equals(that.getUniqueId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUniqueId());
    }

    // delegate

    public @NotNull Identity identity() {
        return handle.identity();
    }

    public @NotNull Component displayName() {
        return handle.displayName();
    }

    public void displayName(@Nullable Component displayName) {
        handle.displayName(displayName);
    }

    @Deprecated
    @NotNull
    public String getDisplayName() {
        return handle.getDisplayName();
    }

    @Deprecated
    public void setDisplayName(@Nullable String name) {
        handle.setDisplayName(name);
    }

    public void playerListName(@Nullable Component name) {
        handle.playerListName(name);
    }

    public @Nullable Component playerListName() {
        return handle.playerListName();
    }

    public @Nullable Component playerListHeader() {
        return handle.playerListHeader();
    }

    public @Nullable Component playerListFooter() {
        return handle.playerListFooter();
    }

    @Deprecated
    @NotNull
    public String getPlayerListName() {
        return handle.getPlayerListName();
    }

    @Deprecated
    public void setPlayerListName(@Nullable String name) {
        handle.setPlayerListName(name);
    }

    @Deprecated
    @Nullable
    public String getPlayerListHeader() {
        return handle.getPlayerListHeader();
    }

    @Deprecated
    @Nullable
    public String getPlayerListFooter() {
        return handle.getPlayerListFooter();
    }

    @Deprecated
    public void setPlayerListHeader(@Nullable String header) {
        handle.setPlayerListHeader(header);
    }

    @Deprecated
    public void setPlayerListFooter(@Nullable String footer) {
        handle.setPlayerListFooter(footer);
    }

    @Deprecated
    public void setPlayerListHeaderFooter(@Nullable String header, @Nullable String footer) {
        handle.setPlayerListHeaderFooter(header, footer);
    }

    public void setCompassTarget(@NotNull Location loc) {
        handle.setCompassTarget(loc);
    }

    public @NotNull Location getCompassTarget() {
        return handle.getCompassTarget();
    }

    public @Nullable InetSocketAddress getAddress() {
        return handle.getAddress();
    }

    public void sendRawMessage(@NotNull String message) {
        handle.sendRawMessage(message);
    }

    @Deprecated
    public void kickPlayer(@Nullable String message) {
        handle.kickPlayer(message);
    }

    public void kick(@Nullable Component message) {
        handle.kick(message);
    }

    public void kick(@Nullable Component message, PlayerKickEvent.@NotNull Cause cause) {
        handle.kick(message, cause);
    }

    public void chat(@NotNull String msg) {
        handle.chat(msg);
    }

    public boolean performCommand(@NotNull String command) {
        return handle.performCommand(command);
    }

    @Deprecated
    public boolean isOnGround() {
        return handle.isOnGround();
    }

    public boolean isSneaking() {
        return handle.isSneaking();
    }

    public void setSneaking(boolean sneak) {
        handle.setSneaking(sneak);
    }

    public boolean isSprinting() {
        return handle.isSprinting();
    }

    public void setSprinting(boolean sprinting) {
        handle.setSprinting(sprinting);
    }

    public void saveData() {
        handle.saveData();
    }

    public void loadData() {
        handle.loadData();
    }

    public void setSleepingIgnored(boolean isSleeping) {
        handle.setSleepingIgnored(isSleeping);
    }

    public boolean isSleepingIgnored() {
        return handle.isSleepingIgnored();
    }

    public @Nullable Location getBedSpawnLocation() {
        return handle.getBedSpawnLocation();
    }

    public void setBedSpawnLocation(@Nullable Location location) {
        handle.setBedSpawnLocation(location);
    }

    public void setBedSpawnLocation(@Nullable Location location, boolean force) {
        handle.setBedSpawnLocation(location, force);
    }

    @Deprecated
    public void playNote(@NotNull Location loc, byte instrument, byte note) {
        handle.playNote(loc, instrument, note);
    }

    public void playNote(@NotNull Location loc, @NotNull Instrument instrument, @NotNull Note note) {
        handle.playNote(loc, instrument, note);
    }

    public void playSound(@NotNull Location location, @NotNull Sound sound, float volume, float pitch) {
        handle.playSound(location, sound, volume, pitch);
    }

    public void playSound(@NotNull Location location, @NotNull String sound, float volume, float pitch) {
        handle.playSound(location, sound, volume, pitch);
    }

    public void playSound(@NotNull Location location, @NotNull Sound sound, @NotNull SoundCategory category, float volume, float pitch) {
        handle.playSound(location, sound, category, volume, pitch);
    }

    public void playSound(@NotNull Location location, @NotNull String sound, @NotNull SoundCategory category, float volume, float pitch) {
        handle.playSound(location, sound, category, volume, pitch);
    }

    public void stopSound(@NotNull Sound sound) {
        handle.stopSound(sound);
    }

    public void stopSound(@NotNull String sound) {
        handle.stopSound(sound);
    }

    public void stopSound(@NotNull Sound sound, @Nullable SoundCategory category) {
        handle.stopSound(sound, category);
    }

    public void stopSound(@NotNull String sound, @Nullable SoundCategory category) {
        handle.stopSound(sound, category);
    }

    @Deprecated
    public void playEffect(@NotNull Location loc, @NotNull Effect effect, int data) {
        handle.playEffect(loc, effect, data);
    }

    public <T> void playEffect(@NotNull Location loc, @NotNull Effect effect, @Nullable T data) {
        handle.playEffect(loc, effect, data);
    }

    @Deprecated
    public void sendBlockChange(@NotNull Location loc, @NotNull Material material, byte data) {
        handle.sendBlockChange(loc, material, data);
    }

    public void sendBlockChange(@NotNull Location loc, @NotNull BlockData block) {
        handle.sendBlockChange(loc, block);
    }

    public void sendBlockDamage(@NotNull Location loc, float progress) {
        handle.sendBlockDamage(loc, progress);
    }

    @Deprecated
    public boolean sendChunkChange(@NotNull Location loc, int sx, int sy, int sz, @NotNull byte[] data) {
        return handle.sendChunkChange(loc, sx, sy, sz, data);
    }

    public void sendSignChange(@NotNull Location loc, @Nullable List<Component> lines) throws IllegalArgumentException {
        handle.sendSignChange(loc, lines);
    }

    public void sendSignChange(@NotNull Location loc, @Nullable List<Component> lines, @NotNull DyeColor dyeColor) throws IllegalArgumentException {
        handle.sendSignChange(loc, lines, dyeColor);
    }

    @Deprecated
    public void sendSignChange(@NotNull Location loc, @Nullable String[] lines) throws IllegalArgumentException {
        handle.sendSignChange(loc, lines);
    }

    @Deprecated
    public void sendSignChange(@NotNull Location loc, @Nullable String[] lines, @NotNull DyeColor dyeColor) throws IllegalArgumentException {
        handle.sendSignChange(loc, lines, dyeColor);
    }

    public void sendMap(@NotNull MapView map) {
        handle.sendMap(map);
    }

    public @Nullable BanEntry banPlayerFull(@Nullable String reason) {
        return handle.banPlayerFull(reason);
    }

    public @Nullable BanEntry banPlayerFull(@Nullable String reason, @Nullable String source) {
        return handle.banPlayerFull(reason, source);
    }

    public @Nullable BanEntry banPlayerFull(@Nullable String reason, @Nullable Date expires) {
        return handle.banPlayerFull(reason, expires);
    }

    public @Nullable BanEntry banPlayerFull(@Nullable String reason, @Nullable Date expires, @Nullable String source) {
        return handle.banPlayerFull(reason, expires, source);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason, boolean kickPlayer) {
        return handle.banPlayerIP(reason, kickPlayer);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason, @Nullable String source, boolean kickPlayer) {
        return handle.banPlayerIP(reason, source, kickPlayer);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason, @Nullable Date expires, boolean kickPlayer) {
        return handle.banPlayerIP(reason, expires, kickPlayer);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason) {
        return handle.banPlayerIP(reason);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason, @Nullable String source) {
        return handle.banPlayerIP(reason, source);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason, @Nullable Date expires) {
        return handle.banPlayerIP(reason, expires);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason, @Nullable Date expires, @Nullable String source) {
        return handle.banPlayerIP(reason, expires, source);
    }

    public @Nullable BanEntry banPlayerIP(@Nullable String reason, @Nullable Date expires, @Nullable String source, boolean kickPlayer) {
        return handle.banPlayerIP(reason, expires, source, kickPlayer);
    }

    @Deprecated
    public void sendActionBar(@NotNull String message) {
        handle.sendActionBar(message);
    }

    @Deprecated
    public void sendActionBar(char alternateChar, @NotNull String message) {
        handle.sendActionBar(alternateChar, message);
    }

    @Deprecated
    public void sendActionBar(@NotNull BaseComponent... message) {
        handle.sendActionBar(message);
    }

    @Override
    @Deprecated
    public void sendMessage(@NotNull BaseComponent component) {
        handle.sendMessage(component);
    }

    @Override
    @Deprecated
    public void sendMessage(@NotNull BaseComponent... components) {
        handle.sendMessage(components);
    }

    @Deprecated
    public void sendMessage(ChatMessageType position, BaseComponent... components) {
        handle.sendMessage(position, components);
    }

    @Deprecated
    public void setPlayerListHeaderFooter(@Nullable BaseComponent[] header, @Nullable BaseComponent[] footer) {
        handle.setPlayerListHeaderFooter(header, footer);
    }

    @Deprecated
    public void setPlayerListHeaderFooter(@Nullable BaseComponent header, @Nullable BaseComponent footer) {
        handle.setPlayerListHeaderFooter(header, footer);
    }

    @Deprecated
    public void setTitleTimes(int fadeInTicks, int stayTicks, int fadeOutTicks) {
        handle.setTitleTimes(fadeInTicks, stayTicks, fadeOutTicks);
    }

    @Deprecated
    public void setSubtitle(BaseComponent[] subtitle) {
        handle.setSubtitle(subtitle);
    }

    @Deprecated
    public void setSubtitle(BaseComponent subtitle) {
        handle.setSubtitle(subtitle);
    }

    @Deprecated
    public void showTitle(@Nullable BaseComponent[] title) {
        handle.showTitle(title);
    }

    @Deprecated
    public void showTitle(@Nullable BaseComponent title) {
        handle.showTitle(title);
    }

    @Deprecated
    public void showTitle(@Nullable BaseComponent[] title, @Nullable BaseComponent[] subtitle, int fadeInTicks, int stayTicks, int fadeOutTicks) {
        handle.showTitle(title, subtitle, fadeInTicks, stayTicks, fadeOutTicks);
    }

    @Deprecated
    public void showTitle(@Nullable BaseComponent title, @Nullable BaseComponent subtitle, int fadeInTicks, int stayTicks, int fadeOutTicks) {
        handle.showTitle(title, subtitle, fadeInTicks, stayTicks, fadeOutTicks);
    }

    @Deprecated
    public void sendTitle(@NotNull Title title) {
        handle.sendTitle(title);
    }

    @Deprecated
    public void updateTitle(@NotNull Title title) {
        handle.updateTitle(title);
    }

    @Deprecated
    public void hideTitle() {
        handle.hideTitle();
    }

    public void updateInventory() {
        handle.updateInventory();
    }

    public void setPlayerTime(long time, boolean relative) {
        handle.setPlayerTime(time, relative);
    }

    public long getPlayerTime() {
        return handle.getPlayerTime();
    }

    public long getPlayerTimeOffset() {
        return handle.getPlayerTimeOffset();
    }

    public boolean isPlayerTimeRelative() {
        return handle.isPlayerTimeRelative();
    }

    public void resetPlayerTime() {
        handle.resetPlayerTime();
    }

    public void setPlayerWeather(@NotNull WeatherType type) {
        handle.setPlayerWeather(type);
    }

    public @Nullable WeatherType getPlayerWeather() {
        return handle.getPlayerWeather();
    }

    public void resetPlayerWeather() {
        handle.resetPlayerWeather();
    }

    public void giveExp(int amount) {
        handle.giveExp(amount);
    }

    public void giveExp(int amount, boolean applyMending) {
        handle.giveExp(amount, applyMending);
    }

    public int applyMending(int amount) {
        return handle.applyMending(amount);
    }

    public void giveExpLevels(int amount) {
        handle.giveExpLevels(amount);
    }

    public float getExp() {
        return handle.getExp();
    }

    public void setExp(float exp) {
        handle.setExp(exp);
    }

    public int getLevel() {
        return handle.getLevel();
    }

    public void setLevel(int level) {
        handle.setLevel(level);
    }

    public int getTotalExperience() {
        return handle.getTotalExperience();
    }

    public void setTotalExperience(int exp) {
        handle.setTotalExperience(exp);
    }

    public void sendExperienceChange(float progress) {
        handle.sendExperienceChange(progress);
    }

    public void sendExperienceChange(float progress, int level) {
        handle.sendExperienceChange(progress, level);
    }

    public boolean getAllowFlight() {
        return handle.getAllowFlight();
    }

    public void setAllowFlight(boolean flight) {
        handle.setAllowFlight(flight);
    }

    @Deprecated
    public void hidePlayer(@NotNull Player player) {
        handle.hidePlayer(unwrap(player));
    }

    public void hidePlayer(@NotNull Plugin plugin, @NotNull Player player) {
        handle.hidePlayer(plugin, unwrap(player));
    }

    @Deprecated
    public void showPlayer(@NotNull Player player) {
        handle.showPlayer(unwrap(player));
    }

    public void showPlayer(@NotNull Plugin plugin, @NotNull Player player) {
        handle.showPlayer(plugin, unwrap(player));
    }

    public boolean canSee(@NotNull Player player) {
        return handle.canSee(unwrap(player));
    }

    public boolean isFlying() {
        return handle.isFlying();
    }

    public void setFlying(boolean value) {
        handle.setFlying(value);
    }

    public void setFlySpeed(float value) throws IllegalArgumentException {
        handle.setFlySpeed(value);
    }

    public void setWalkSpeed(float value) throws IllegalArgumentException {
        handle.setWalkSpeed(value);
    }

    public float getFlySpeed() {
        return handle.getFlySpeed();
    }

    public float getWalkSpeed() {
        return handle.getWalkSpeed();
    }

    @Deprecated
    public void setTexturePack(@NotNull String url) {
        handle.setTexturePack(url);
    }

    @Deprecated
    public void setResourcePack(@NotNull String url) {
        handle.setResourcePack(url);
    }

    public void setResourcePack(@NotNull String url, @NotNull byte[] hash) {
        handle.setResourcePack(url, hash);
    }

    public @NotNull Scoreboard getScoreboard() {
        return handle.getScoreboard();
    }

    public void setScoreboard(@NotNull Scoreboard scoreboard) throws IllegalArgumentException, IllegalStateException {
        handle.setScoreboard(scoreboard);
    }

    public boolean isHealthScaled() {
        return handle.isHealthScaled();
    }

    public void setHealthScaled(boolean scale) {
        handle.setHealthScaled(scale);
    }

    public void setHealthScale(double scale) throws IllegalArgumentException {
        handle.setHealthScale(scale);
    }

    public double getHealthScale() {
        return handle.getHealthScale();
    }

    public @Nullable Entity getSpectatorTarget() {
        return handle.getSpectatorTarget();
    }

    public void setSpectatorTarget(@Nullable Entity entity) {
        handle.setSpectatorTarget(entity);
    }

    @Deprecated
    public void sendTitle(@Nullable String title, @Nullable String subtitle) {
        handle.sendTitle(title, subtitle);
    }

    public void sendTitle(@Nullable String title, @Nullable String subtitle, int fadeIn, int stay, int fadeOut) {
        handle.sendTitle(title, subtitle, fadeIn, stay, fadeOut);
    }

    @Override
    public void resetTitle() {
        handle.resetTitle();
    }

    public void spawnParticle(@NotNull Particle particle, @NotNull Location location, int count) {
        handle.spawnParticle(particle, location, count);
    }

    public void spawnParticle(@NotNull Particle particle, double x, double y, double z, int count) {
        handle.spawnParticle(particle, x, y, z, count);
    }

    public <T> void spawnParticle(@NotNull Particle particle, @NotNull Location location, int count, @Nullable T data) {
        handle.spawnParticle(particle, location, count, data);
    }

    public <T> void spawnParticle(@NotNull Particle particle, double x, double y, double z, int count, @Nullable T data) {
        handle.spawnParticle(particle, x, y, z, count, data);
    }

    public void spawnParticle(@NotNull Particle particle, @NotNull Location location, int count, double offsetX, double offsetY, double offsetZ) {
        handle.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ);
    }

    public void spawnParticle(@NotNull Particle particle, double x, double y, double z, int count, double offsetX, double offsetY, double offsetZ) {
        handle.spawnParticle(particle, x, y, z, count, offsetX, offsetY, offsetZ);
    }

    public <T> void spawnParticle(@NotNull Particle particle, @NotNull Location location, int count, double offsetX, double offsetY, double offsetZ, @Nullable T data) {
        handle.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ, data);
    }

    public <T> void spawnParticle(@NotNull Particle particle, double x, double y, double z, int count, double offsetX, double offsetY, double offsetZ, @Nullable T data) {
        handle.spawnParticle(particle, x, y, z, count, offsetX, offsetY, offsetZ, data);
    }

    public void spawnParticle(@NotNull Particle particle, @NotNull Location location, int count, double offsetX, double offsetY, double offsetZ, double extra) {
        handle.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ, extra);
    }

    public void spawnParticle(@NotNull Particle particle, double x, double y, double z, int count, double offsetX, double offsetY, double offsetZ, double extra) {
        handle.spawnParticle(particle, x, y, z, count, offsetX, offsetY, offsetZ, extra);
    }

    public <T> void spawnParticle(@NotNull Particle particle, @NotNull Location location, int count, double offsetX, double offsetY, double offsetZ, double extra, @Nullable T data) {
        handle.spawnParticle(particle, location, count, offsetX, offsetY, offsetZ, extra, data);
    }

    public <T> void spawnParticle(@NotNull Particle particle, double x, double y, double z, int count, double offsetX, double offsetY, double offsetZ, double extra, @Nullable T data) {
        handle.spawnParticle(particle, x, y, z, count, offsetX, offsetY, offsetZ, extra, data);
    }

    public @NotNull AdvancementProgress getAdvancementProgress(@NotNull Advancement advancement) {
        return handle.getAdvancementProgress(advancement);
    }

    public int getClientViewDistance() {
        return handle.getClientViewDistance();
    }

    public @NotNull Locale locale() {
        return handle.locale();
    }

    public int getPing() {
        return handle.getPing();
    }

    @Deprecated
    @NotNull
    public String getLocale() {
        return handle.getLocale();
    }

    public boolean getAffectsSpawning() {
        return handle.getAffectsSpawning();
    }

    public void setAffectsSpawning(boolean affects) {
        handle.setAffectsSpawning(affects);
    }

    @Deprecated
    public int getViewDistance() {
        return handle.getViewDistance();
    }

    @Deprecated
    public void setViewDistance(int viewDistance) {
        handle.setViewDistance(viewDistance);
    }

    public void updateCommands() {
        handle.updateCommands();
    }

    public void openBook(@NotNull ItemStack book) {
        handle.openBook(book);
    }

    public @NotNull HoverEvent<HoverEvent.ShowEntity> asHoverEvent(@NotNull UnaryOperator<HoverEvent.ShowEntity> op) {
        return handle.asHoverEvent(op);
    }

    public void setResourcePack(@NotNull String url, @NotNull String hash) {
        handle.setResourcePack(url, hash);
    }

    public PlayerResourcePackStatusEvent.@Nullable Status getResourcePackStatus() {
        return handle.getResourcePackStatus();
    }

    @Deprecated
    @Nullable
    public String getResourcePackHash() {
        return handle.getResourcePackHash();
    }

    public boolean hasResourcePack() {
        return handle.hasResourcePack();
    }

    public @NotNull PlayerProfile getPlayerProfile() {
        return handle.getPlayerProfile();
    }

    public void setPlayerProfile(@NotNull PlayerProfile profile) {
        handle.setPlayerProfile(profile);
    }

    public float getCooldownPeriod() {
        return handle.getCooldownPeriod();
    }

    public float getCooledAttackStrength(float adjustTicks) {
        return handle.getCooledAttackStrength(adjustTicks);
    }

    public void resetCooldown() {
        handle.resetCooldown();
    }

    public <T> @NotNull T getClientOption(@NotNull ClientOption<T> option) {
        return handle.getClientOption(option);
    }

    public @Nullable Firework boostElytra(@NotNull ItemStack firework) {
        return handle.boostElytra(firework);
    }

    public void sendOpLevel(byte level) {
        handle.sendOpLevel(level);
    }

    public @NotNull Set<Player> getTrackedPlayers() {
        return handle.getTrackedPlayers();
    }

    @Nullable
    public String getClientBrandName() {
        return handle.getClientBrandName();
    }

    @Override
    public Player.@NotNull Spigot spigot() {
        return handle.spigot();
    }

    @Override
    @NotNull
    public String getName() {
        return handle.getName();
    }

    public @NotNull PlayerInventory getInventory() {
        return handle.getInventory();
    }

    public @NotNull Inventory getEnderChest() {
        return handle.getEnderChest();
    }

    public @NotNull MainHand getMainHand() {
        return handle.getMainHand();
    }

    public boolean setWindowProperty(InventoryView.@NotNull Property prop, int value) {
        return handle.setWindowProperty(prop, value);
    }

    public @NotNull InventoryView getOpenInventory() {
        return handle.getOpenInventory();
    }

    public @Nullable InventoryView openInventory(@NotNull Inventory inventory) {
        return handle.openInventory(inventory);
    }

    public @Nullable InventoryView openWorkbench(@Nullable Location location, boolean force) {
        return handle.openWorkbench(location, force);
    }

    public @Nullable InventoryView openEnchanting(@Nullable Location location, boolean force) {
        return handle.openEnchanting(location, force);
    }

    public void openInventory(@NotNull InventoryView inventory) {
        handle.openInventory(inventory);
    }

    public @Nullable InventoryView openMerchant(@NotNull Villager trader, boolean force) {
        return handle.openMerchant(trader, force);
    }

    public @Nullable InventoryView openMerchant(@NotNull Merchant merchant, boolean force) {
        return handle.openMerchant(merchant, force);
    }

    public @Nullable InventoryView openAnvil(@Nullable Location location, boolean force) {
        return handle.openAnvil(location, force);
    }

    public @Nullable InventoryView openCartographyTable(@Nullable Location location, boolean force) {
        return handle.openCartographyTable(location, force);
    }

    public @Nullable InventoryView openGrindstone(@Nullable Location location, boolean force) {
        return handle.openGrindstone(location, force);
    }

    public @Nullable InventoryView openLoom(@Nullable Location location, boolean force) {
        return handle.openLoom(location, force);
    }

    public @Nullable InventoryView openSmithingTable(@Nullable Location location, boolean force) {
        return handle.openSmithingTable(location, force);
    }

    public @Nullable InventoryView openStonecutter(@Nullable Location location, boolean force) {
        return handle.openStonecutter(location, force);
    }

    public void closeInventory() {
        handle.closeInventory();
    }

    public void closeInventory(InventoryCloseEvent.@NotNull Reason reason) {
        handle.closeInventory(reason);
    }

    @Deprecated
    public @NotNull ItemStack getItemInHand() {
        return handle.getItemInHand();
    }

    @Deprecated
    public void setItemInHand(@Nullable ItemStack item) {
        handle.setItemInHand(item);
    }

    public @NotNull ItemStack getItemOnCursor() {
        return handle.getItemOnCursor();
    }

    public void setItemOnCursor(@Nullable ItemStack item) {
        handle.setItemOnCursor(item);
    }

    public boolean hasCooldown(@NotNull Material material) {
        return handle.hasCooldown(material);
    }

    public int getCooldown(@NotNull Material material) {
        return handle.getCooldown(material);
    }

    public void setCooldown(@NotNull Material material, int ticks) {
        handle.setCooldown(material, ticks);
    }

    public boolean isDeeplySleeping() {
        return handle.isDeeplySleeping();
    }

    public int getSleepTicks() {
        return handle.getSleepTicks();
    }

    public @Nullable Location getPotentialBedLocation() {
        return handle.getPotentialBedLocation();
    }

    public boolean sleep(@NotNull Location location, boolean force) {
        return handle.sleep(location, force);
    }

    public void wakeup(boolean setSpawnLocation) {
        handle.wakeup(setSpawnLocation);
    }

    public @NotNull Location getBedLocation() {
        return handle.getBedLocation();
    }

    public @NotNull GameMode getGameMode() {
        return handle.getGameMode();
    }

    public void setGameMode(@NotNull GameMode mode) {
        handle.setGameMode(mode);
    }

    public boolean isBlocking() {
        return handle.isBlocking();
    }

    public boolean isHandRaised() {
        return handle.isHandRaised();
    }

    public int getExpToLevel() {
        return handle.getExpToLevel();
    }

    public @Nullable Entity releaseLeftShoulderEntity() {
        return handle.releaseLeftShoulderEntity();
    }

    public @Nullable Entity releaseRightShoulderEntity() {
        return handle.releaseRightShoulderEntity();
    }

    public float getAttackCooldown() {
        return handle.getAttackCooldown();
    }

    public boolean discoverRecipe(@NotNull NamespacedKey recipe) {
        return handle.discoverRecipe(recipe);
    }

    public int discoverRecipes(@NotNull Collection<NamespacedKey> recipes) {
        return handle.discoverRecipes(recipes);
    }

    public boolean undiscoverRecipe(@NotNull NamespacedKey recipe) {
        return handle.undiscoverRecipe(recipe);
    }

    public int undiscoverRecipes(@NotNull Collection<NamespacedKey> recipes) {
        return handle.undiscoverRecipes(recipes);
    }

    public boolean hasDiscoveredRecipe(@NotNull NamespacedKey recipe) {
        return handle.hasDiscoveredRecipe(recipe);
    }

    public @NotNull Set<NamespacedKey> getDiscoveredRecipes() {
        return handle.getDiscoveredRecipes();
    }

    @Deprecated
    public @Nullable Entity getShoulderEntityLeft() {
        return handle.getShoulderEntityLeft();
    }

    @Deprecated
    public void setShoulderEntityLeft(@Nullable Entity entity) {
        handle.setShoulderEntityLeft(entity);
    }

    @Deprecated
    public @Nullable Entity getShoulderEntityRight() {
        return handle.getShoulderEntityRight();
    }

    @Deprecated
    public void setShoulderEntityRight(@Nullable Entity entity) {
        handle.setShoulderEntityRight(entity);
    }

    public void openSign(@NotNull Sign sign) {
        handle.openSign(sign);
    }

    public boolean dropItem(boolean dropAll) {
        return handle.dropItem(dropAll);
    }

    public float getExhaustion() {
        return handle.getExhaustion();
    }

    public void setExhaustion(float value) {
        handle.setExhaustion(value);
    }

    public float getSaturation() {
        return handle.getSaturation();
    }

    public void setSaturation(float value) {
        handle.setSaturation(value);
    }

    public int getFoodLevel() {
        return handle.getFoodLevel();
    }

    public void setFoodLevel(int value) {
        handle.setFoodLevel(value);
    }

    public int getSaturatedRegenRate() {
        return handle.getSaturatedRegenRate();
    }

    public void setSaturatedRegenRate(int ticks) {
        handle.setSaturatedRegenRate(ticks);
    }

    public int getUnsaturatedRegenRate() {
        return handle.getUnsaturatedRegenRate();
    }

    public void setUnsaturatedRegenRate(int ticks) {
        handle.setUnsaturatedRegenRate(ticks);
    }

    public int getStarvationRate() {
        return handle.getStarvationRate();
    }

    public void setStarvationRate(int ticks) {
        handle.setStarvationRate(ticks);
    }

    public double getEyeHeight() {
        return handle.getEyeHeight();
    }

    public double getEyeHeight(boolean ignorePose) {
        return handle.getEyeHeight(ignorePose);
    }

    public @NotNull Location getEyeLocation() {
        return handle.getEyeLocation();
    }

    public @NotNull List<Block> getLineOfSight(@Nullable Set<Material> transparent, int maxDistance) {
        return handle.getLineOfSight(transparent, maxDistance);
    }

    public @NotNull Block getTargetBlock(@Nullable Set<Material> transparent, int maxDistance) {
        return handle.getTargetBlock(transparent, maxDistance);
    }

    public @Nullable Block getTargetBlock(int maxDistance) {
        return handle.getTargetBlock(maxDistance);
    }

    public @Nullable Block getTargetBlock(int maxDistance, TargetBlockInfo.@NotNull FluidMode fluidMode) {
        return handle.getTargetBlock(maxDistance, fluidMode);
    }

    public @Nullable BlockFace getTargetBlockFace(int maxDistance) {
        return handle.getTargetBlockFace(maxDistance);
    }

    public @Nullable BlockFace getTargetBlockFace(int maxDistance, TargetBlockInfo.@NotNull FluidMode fluidMode) {
        return handle.getTargetBlockFace(maxDistance, fluidMode);
    }

    public @Nullable TargetBlockInfo getTargetBlockInfo(int maxDistance) {
        return handle.getTargetBlockInfo(maxDistance);
    }

    public @Nullable TargetBlockInfo getTargetBlockInfo(int maxDistance, TargetBlockInfo.@NotNull FluidMode fluidMode) {
        return handle.getTargetBlockInfo(maxDistance, fluidMode);
    }

    public @Nullable Entity getTargetEntity(int maxDistance) {
        return handle.getTargetEntity(maxDistance);
    }

    public @Nullable Entity getTargetEntity(int maxDistance, boolean ignoreBlocks) {
        return handle.getTargetEntity(maxDistance, ignoreBlocks);
    }

    public @Nullable TargetEntityInfo getTargetEntityInfo(int maxDistance) {
        return handle.getTargetEntityInfo(maxDistance);
    }

    public @Nullable TargetEntityInfo getTargetEntityInfo(int maxDistance, boolean ignoreBlocks) {
        return handle.getTargetEntityInfo(maxDistance, ignoreBlocks);
    }

    public @NotNull List<Block> getLastTwoTargetBlocks(@Nullable Set<Material> transparent, int maxDistance) {
        return handle.getLastTwoTargetBlocks(transparent, maxDistance);
    }

    public @Nullable Block getTargetBlockExact(int maxDistance) {
        return handle.getTargetBlockExact(maxDistance);
    }

    public @Nullable Block getTargetBlockExact(int maxDistance, @NotNull FluidCollisionMode fluidCollisionMode) {
        return handle.getTargetBlockExact(maxDistance, fluidCollisionMode);
    }

    public @Nullable RayTraceResult rayTraceBlocks(double maxDistance) {
        return handle.rayTraceBlocks(maxDistance);
    }

    public @Nullable RayTraceResult rayTraceBlocks(double maxDistance, @NotNull FluidCollisionMode fluidCollisionMode) {
        return handle.rayTraceBlocks(maxDistance, fluidCollisionMode);
    }

    public int getRemainingAir() {
        return handle.getRemainingAir();
    }

    public void setRemainingAir(int ticks) {
        handle.setRemainingAir(ticks);
    }

    public int getMaximumAir() {
        return handle.getMaximumAir();
    }

    public void setMaximumAir(int ticks) {
        handle.setMaximumAir(ticks);
    }

    public int getArrowCooldown() {
        return handle.getArrowCooldown();
    }

    public void setArrowCooldown(int ticks) {
        handle.setArrowCooldown(ticks);
    }

    public int getArrowsInBody() {
        return handle.getArrowsInBody();
    }

    public void setArrowsInBody(int count) {
        handle.setArrowsInBody(count);
    }

    public int getMaximumNoDamageTicks() {
        return handle.getMaximumNoDamageTicks();
    }

    public void setMaximumNoDamageTicks(int ticks) {
        handle.setMaximumNoDamageTicks(ticks);
    }

    public double getLastDamage() {
        return handle.getLastDamage();
    }

    public void setLastDamage(double damage) {
        handle.setLastDamage(damage);
    }

    public int getNoDamageTicks() {
        return handle.getNoDamageTicks();
    }

    public void setNoDamageTicks(int ticks) {
        handle.setNoDamageTicks(ticks);
    }

    @Nullable
    public Player getKiller() {
        return handle.getKiller();
    }

    public void setKiller(@Nullable Player killer) {
        handle.setKiller(killer);
    }

    public boolean addPotionEffect(@NotNull PotionEffect effect) {
        return handle.addPotionEffect(effect);
    }

    @Deprecated
    public boolean addPotionEffect(@NotNull PotionEffect effect, boolean force) {
        return handle.addPotionEffect(effect, force);
    }

    public boolean addPotionEffects(@NotNull Collection<PotionEffect> effects) {
        return handle.addPotionEffects(effects);
    }

    public boolean hasPotionEffect(@NotNull PotionEffectType type) {
        return handle.hasPotionEffect(type);
    }

    public @Nullable PotionEffect getPotionEffect(@NotNull PotionEffectType type) {
        return handle.getPotionEffect(type);
    }

    public void removePotionEffect(@NotNull PotionEffectType type) {
        handle.removePotionEffect(type);
    }

    public @NotNull Collection<PotionEffect> getActivePotionEffects() {
        return handle.getActivePotionEffects();
    }

    public boolean hasLineOfSight(@NotNull Entity other) {
        return handle.hasLineOfSight(other);
    }

    public boolean hasLineOfSight(@NotNull Location location) {
        return handle.hasLineOfSight(location);
    }

    public boolean getRemoveWhenFarAway() {
        return handle.getRemoveWhenFarAway();
    }

    public void setRemoveWhenFarAway(boolean remove) {
        handle.setRemoveWhenFarAway(remove);
    }

    public @Nullable EntityEquipment getEquipment() {
        return handle.getEquipment();
    }

    public void setCanPickupItems(boolean pickup) {
        handle.setCanPickupItems(pickup);
    }

    public boolean getCanPickupItems() {
        return handle.getCanPickupItems();
    }

    public boolean isLeashed() {
        return handle.isLeashed();
    }

    public @NotNull Entity getLeashHolder() throws IllegalStateException {
        return handle.getLeashHolder();
    }

    public boolean setLeashHolder(@Nullable Entity holder) {
        return handle.setLeashHolder(holder);
    }

    public boolean isGliding() {
        return handle.isGliding();
    }

    public void setGliding(boolean gliding) {
        handle.setGliding(gliding);
    }

    public boolean isSwimming() {
        return handle.isSwimming();
    }

    public void setSwimming(boolean swimming) {
        handle.setSwimming(swimming);
    }

    public boolean isRiptiding() {
        return handle.isRiptiding();
    }

    public boolean isSleeping() {
        return handle.isSleeping();
    }

    public void setAI(boolean ai) {
        handle.setAI(ai);
    }

    public boolean hasAI() {
        return handle.hasAI();
    }

    public void attack(@NotNull Entity target) {
        handle.attack(target);
    }

    public void swingMainHand() {
        handle.swingMainHand();
    }

    public void swingOffHand() {
        handle.swingOffHand();
    }

    public void setCollidable(boolean collidable) {
        handle.setCollidable(collidable);
    }

    public boolean isCollidable() {
        return handle.isCollidable();
    }

    public @NotNull Set<UUID> getCollidableExemptions() {
        return handle.getCollidableExemptions();
    }

    public <T> @Nullable T getMemory(@NotNull MemoryKey<T> memoryKey) {
        return handle.getMemory(memoryKey);
    }

    public <T> void setMemory(@NotNull MemoryKey<T> memoryKey, @Nullable T memoryValue) {
        handle.setMemory(memoryKey, memoryValue);
    }

    public @NotNull EntityCategory getCategory() {
        return handle.getCategory();
    }

    public void setInvisible(boolean invisible) {
        handle.setInvisible(invisible);
    }

    public boolean isInvisible() {
        return handle.isInvisible();
    }

    public int getArrowsStuck() {
        return handle.getArrowsStuck();
    }

    public void setArrowsStuck(int arrows) {
        handle.setArrowsStuck(arrows);
    }

    public int getShieldBlockingDelay() {
        return handle.getShieldBlockingDelay();
    }

    public void setShieldBlockingDelay(int delay) {
        handle.setShieldBlockingDelay(delay);
    }

    public @Nullable ItemStack getActiveItem() {
        return handle.getActiveItem();
    }

    public void clearActiveItem() {
        handle.clearActiveItem();
    }

    public int getItemUseRemainingTime() {
        return handle.getItemUseRemainingTime();
    }

    public int getHandRaisedTime() {
        return handle.getHandRaisedTime();
    }

    public @NotNull EquipmentSlot getHandRaised() {
        return handle.getHandRaised();
    }

    public boolean isJumping() {
        return handle.isJumping();
    }

    public void setJumping(boolean jumping) {
        handle.setJumping(jumping);
    }

    public void playPickupItemAnimation(@NotNull Item item) {
        handle.playPickupItemAnimation(item);
    }

    public void playPickupItemAnimation(@NotNull Item item, int quantity) {
        handle.playPickupItemAnimation(item, quantity);
    }

    public float getHurtDirection() {
        return handle.getHurtDirection();
    }

    public void setHurtDirection(float hurtDirection) {
        handle.setHurtDirection(hurtDirection);
    }

    public @Nullable AttributeInstance getAttribute(@NotNull Attribute attribute) {
        return handle.getAttribute(attribute);
    }

    public void registerAttribute(@NotNull Attribute attribute) {
        handle.registerAttribute(attribute);
    }

    public void damage(double amount) {
        handle.damage(amount);
    }

    public void damage(double amount, @Nullable Entity source) {
        handle.damage(amount, source);
    }

    public double getHealth() {
        return handle.getHealth();
    }

    public void setHealth(double health) {
        handle.setHealth(health);
    }

    public double getAbsorptionAmount() {
        return handle.getAbsorptionAmount();
    }

    public void setAbsorptionAmount(double amount) {
        handle.setAbsorptionAmount(amount);
    }

    @Deprecated
    public double getMaxHealth() {
        return handle.getMaxHealth();
    }

    @Deprecated
    public void setMaxHealth(double health) {
        handle.setMaxHealth(health);
    }

    @Deprecated
    public void resetMaxHealth() {
        handle.resetMaxHealth();
    }

    public @NotNull Location getLocation() {
        return handle.getLocation();
    }

    @Contract("null -> null; !null -> !null")
    public @Nullable Location getLocation(@Nullable Location loc) {
        return handle.getLocation(loc);
    }

    public void setVelocity(@NotNull org.bukkit.util.Vector velocity) {
        handle.setVelocity(velocity);
    }

    public @NotNull org.bukkit.util.Vector getVelocity() {
        return handle.getVelocity();
    }

    public double getHeight() {
        return handle.getHeight();
    }

    public double getWidth() {
        return handle.getWidth();
    }

    public @NotNull BoundingBox getBoundingBox() {
        return handle.getBoundingBox();
    }

    public boolean isInWater() {
        return handle.isInWater();
    }

    public @NotNull World getWorld() {
        return handle.getWorld();
    }

    public void setRotation(float yaw, float pitch) {
        handle.setRotation(yaw, pitch);
    }

    public boolean teleport(@NotNull Location location) {
        return handle.teleport(location);
    }

    public boolean teleport(@NotNull Location location, PlayerTeleportEvent.@NotNull TeleportCause cause) {
        return handle.teleport(location, cause);
    }

    public boolean teleport(@NotNull Entity destination) {
        return handle.teleport(destination);
    }

    public boolean teleport(@NotNull Entity destination, PlayerTeleportEvent.@NotNull TeleportCause cause) {
        return handle.teleport(destination, cause);
    }

    public @NotNull CompletableFuture<Boolean> teleportAsync(@NotNull Location loc) {
        return handle.teleportAsync(loc);
    }

    public @NotNull CompletableFuture<Boolean> teleportAsync(@NotNull Location loc, PlayerTeleportEvent.@NotNull TeleportCause cause) {
        return handle.teleportAsync(loc, cause);
    }

    public @NotNull List<Entity> getNearbyEntities(double x, double y, double z) {
        return handle.getNearbyEntities(x, y, z);
    }

    public int getEntityId() {
        return handle.getEntityId();
    }

    public int getFireTicks() {
        return handle.getFireTicks();
    }

    public int getMaxFireTicks() {
        return handle.getMaxFireTicks();
    }

    public void setFireTicks(int ticks) {
        handle.setFireTicks(ticks);
    }

    public void remove() {
        handle.remove();
    }

    public boolean isDead() {
        return handle.isDead();
    }

    public boolean isValid() {
        return handle.isValid();
    }

    @Override
    public @NotNull Server getServer() {
        return handle.getServer();
    }

    public boolean isPersistent() {
        return handle.isPersistent();
    }

    public void setPersistent(boolean persistent) {
        handle.setPersistent(persistent);
    }

    @Deprecated
    public @Nullable Entity getPassenger() {
        return handle.getPassenger();
    }

    @Deprecated
    public boolean setPassenger(@NotNull Entity passenger) {
        return handle.setPassenger(passenger);
    }

    public @NotNull List<Entity> getPassengers() {
        return handle.getPassengers();
    }

    public boolean addPassenger(@NotNull Entity passenger) {
        return handle.addPassenger(passenger);
    }

    public boolean removePassenger(@NotNull Entity passenger) {
        return handle.removePassenger(passenger);
    }

    public boolean isEmpty() {
        return handle.isEmpty();
    }

    public boolean eject() {
        return handle.eject();
    }

    public float getFallDistance() {
        return handle.getFallDistance();
    }

    public void setFallDistance(float distance) {
        handle.setFallDistance(distance);
    }

    public void setLastDamageCause(@Nullable EntityDamageEvent event) {
        handle.setLastDamageCause(event);
    }

    public @Nullable EntityDamageEvent getLastDamageCause() {
        return handle.getLastDamageCause();
    }

    public @NotNull UUID getUniqueId() {
        return handle.getUniqueId();
    }

    public int getTicksLived() {
        return handle.getTicksLived();
    }

    public void setTicksLived(int value) {
        handle.setTicksLived(value);
    }

    public void playEffect(@NotNull EntityEffect type) {
        handle.playEffect(type);
    }

    public @NotNull EntityType getType() {
        return handle.getType();
    }

    public boolean isInsideVehicle() {
        return handle.isInsideVehicle();
    }

    public boolean leaveVehicle() {
        return handle.leaveVehicle();
    }

    public @Nullable Entity getVehicle() {
        return handle.getVehicle();
    }

    public void setCustomNameVisible(boolean flag) {
        handle.setCustomNameVisible(flag);
    }

    public boolean isCustomNameVisible() {
        return handle.isCustomNameVisible();
    }

    public void setGlowing(boolean flag) {
        handle.setGlowing(flag);
    }

    public boolean isGlowing() {
        return handle.isGlowing();
    }

    public void setInvulnerable(boolean flag) {
        handle.setInvulnerable(flag);
    }

    public boolean isInvulnerable() {
        return handle.isInvulnerable();
    }

    public boolean isSilent() {
        return handle.isSilent();
    }

    public void setSilent(boolean flag) {
        handle.setSilent(flag);
    }

    public boolean hasGravity() {
        return handle.hasGravity();
    }

    public void setGravity(boolean gravity) {
        handle.setGravity(gravity);
    }

    public int getPortalCooldown() {
        return handle.getPortalCooldown();
    }

    public void setPortalCooldown(int cooldown) {
        handle.setPortalCooldown(cooldown);
    }

    public @NotNull Set<String> getScoreboardTags() {
        return handle.getScoreboardTags();
    }

    public boolean addScoreboardTag(@NotNull String tag) {
        return handle.addScoreboardTag(tag);
    }

    public boolean removeScoreboardTag(@NotNull String tag) {
        return handle.removeScoreboardTag(tag);
    }

    public @NotNull PistonMoveReaction getPistonMoveReaction() {
        return handle.getPistonMoveReaction();
    }

    public @NotNull BlockFace getFacing() {
        return handle.getFacing();
    }

    public @NotNull Pose getPose() {
        return handle.getPose();
    }

    public @Nullable Location getOrigin() {
        return handle.getOrigin();
    }

    public boolean fromMobSpawner() {
        return handle.fromMobSpawner();
    }

    public @NotNull Chunk getChunk() {
        return handle.getChunk();
    }

    public CreatureSpawnEvent.@NotNull SpawnReason getEntitySpawnReason() {
        return handle.getEntitySpawnReason();
    }

    public boolean isInRain() {
        return handle.isInRain();
    }

    public boolean isInBubbleColumn() {
        return handle.isInBubbleColumn();
    }

    public boolean isInWaterOrRain() {
        return handle.isInWaterOrRain();
    }

    public boolean isInWaterOrBubbleColumn() {
        return handle.isInWaterOrBubbleColumn();
    }

    public boolean isInWaterOrRainOrBubbleColumn() {
        return handle.isInWaterOrRainOrBubbleColumn();
    }

    public boolean isInLava() {
        return handle.isInLava();
    }

    public boolean isTicking() {
        return handle.isTicking();
    }

    public void setMetadata(@NotNull String metadataKey, @NotNull MetadataValue newMetadataValue) {
        handle.setMetadata(metadataKey, newMetadataValue);
    }

    public @NotNull List<MetadataValue> getMetadata(@NotNull String metadataKey) {
        return handle.getMetadata(metadataKey);
    }

    public boolean hasMetadata(@NotNull String metadataKey) {
        return handle.hasMetadata(metadataKey);
    }

    public void removeMetadata(@NotNull String metadataKey, @NotNull Plugin owningPlugin) {
        handle.removeMetadata(metadataKey, owningPlugin);
    }

    @Override
    public void sendMessage(@NotNull String message) {
        handle.sendMessage(message);
    }

    @Override
    public void sendMessage(@NotNull String[] messages) {
        handle.sendMessage(messages);
    }

    @Override
    public void sendMessage(@Nullable UUID sender, @NotNull String message) {
        handle.sendMessage(sender, message);
    }

    @Override
    public void sendMessage(@Nullable UUID sender, @NotNull String[] messages) {
        handle.sendMessage(sender, messages);
    }

    @Override
    public void sendMessage(@NotNull Identity identity, @NotNull Component message, @NotNull MessageType type) {
        handle.sendMessage(identity, message, type);
    }

    @NonNull
    public static Audience empty() {
        return Audience.empty();
    }

    @NonNull
    public static Audience audience(@NonNull Audience @NonNull ... audiences) {
        return Audience.audience(audiences);
    }

    public static @NonNull ForwardingAudience audience(@NonNull Iterable<? extends Audience> audiences) {
        return Audience.audience(audiences);
    }

    public static @NonNull Collector<? super Audience, ?, ForwardingAudience> toAudience() {
        return Audience.toAudience();
    }

    @Override
    public void sendMessage(@NonNull ComponentLike message) {
        handle.sendMessage(message);
    }

    @Override
    public void sendMessage(@NonNull Identified source, @NonNull ComponentLike message) {
        handle.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NonNull Identity source, @NonNull ComponentLike message) {
        handle.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NonNull Component message) {
        handle.sendMessage(message);
    }

    @Override
    public void sendMessage(@NonNull Identified source, @NonNull Component message) {
        handle.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NonNull Identity source, @NonNull Component message) {
        handle.sendMessage(source, message);
    }

    @Override
    public void sendMessage(@NonNull ComponentLike message, @NonNull MessageType type) {
        handle.sendMessage(message, type);
    }

    @Override
    public void sendMessage(@NonNull Identified source, @NonNull ComponentLike message, @NonNull MessageType type) {
        handle.sendMessage(source, message, type);
    }

    @Override
    public void sendMessage(@NonNull Identity source, @NonNull ComponentLike message, @NonNull MessageType type) {
        handle.sendMessage(source, message, type);
    }

    @Override
    public void sendMessage(@NonNull Component message, @NonNull MessageType type) {
        handle.sendMessage(message, type);
    }

    @Override
    public void sendMessage(@NonNull Identified source, @NonNull Component message, @NonNull MessageType type) {
        handle.sendMessage(source, message, type);
    }

    @Override
    public void sendActionBar(@NonNull ComponentLike message) {
        handle.sendActionBar(message);
    }

    @Override
    public void sendActionBar(@NonNull Component message) {
        handle.sendActionBar(message);
    }

    @Override
    public void sendPlayerListHeader(@NonNull ComponentLike header) {
        handle.sendPlayerListHeader(header);
    }

    @Override
    public void sendPlayerListHeader(@NonNull Component header) {
        handle.sendPlayerListHeader(header);
    }

    @Override
    public void sendPlayerListFooter(@NonNull ComponentLike footer) {
        handle.sendPlayerListFooter(footer);
    }

    @Override
    public void sendPlayerListFooter(@NonNull Component footer) {
        handle.sendPlayerListFooter(footer);
    }

    @Override
    public void sendPlayerListHeaderAndFooter(@NonNull ComponentLike header, @NonNull ComponentLike footer) {
        handle.sendPlayerListHeaderAndFooter(header, footer);
    }

    @Override
    public void sendPlayerListHeaderAndFooter(@NonNull Component header, @NonNull Component footer) {
        handle.sendPlayerListHeaderAndFooter(header, footer);
    }

    @Override
    public void showTitle(net.kyori.adventure.title.@NonNull Title title) {
        handle.showTitle(title);
    }

    @Override
    public void clearTitle() {
        handle.clearTitle();
    }

    @Override
    public void showBossBar(@NonNull BossBar bar) {
        handle.showBossBar(bar);
    }

    @Override
    public void hideBossBar(@NonNull BossBar bar) {
        handle.hideBossBar(bar);
    }

    @Override
    public void playSound(net.kyori.adventure.sound.@NonNull Sound sound) {
        handle.playSound(sound);
    }

    @Override
    public void playSound(net.kyori.adventure.sound.@NonNull Sound sound, double x, double y, double z) {
        handle.playSound(sound, x, y, z);
    }

    @Override
    public void stopSound(@NonNull SoundStop stop) {
        handle.stopSound(stop);
    }

    @Override
    public void openBook(Book.@NonNull Builder book) {
        handle.openBook(book);
    }

    @Override
    public void openBook(@NonNull Book book) {
        handle.openBook(book);
    }

    @Override
    public boolean isPermissionSet(@NotNull String name) {
        return handle.isPermissionSet(name);
    }

    @Override
    public boolean isPermissionSet(@NotNull Permission perm) {
        return handle.isPermissionSet(perm);
    }

    @Override
    public boolean hasPermission(@NotNull String name) {
        return handle.hasPermission(name);
    }

    @Override
    public boolean hasPermission(@NotNull Permission perm) {
        return handle.hasPermission(perm);
    }

    @Override
    public @NotNull PermissionAttachment addAttachment(@NotNull Plugin plugin, @NotNull String name, boolean value) {
        return handle.addAttachment(plugin, name, value);
    }

    @Override
    public @NotNull PermissionAttachment addAttachment(@NotNull Plugin plugin) {
        return handle.addAttachment(plugin);
    }

    @Override
    public @Nullable PermissionAttachment addAttachment(@NotNull Plugin plugin, @NotNull String name, boolean value, int ticks) {
        return handle.addAttachment(plugin, name, value, ticks);
    }

    @Override
    public @Nullable PermissionAttachment addAttachment(@NotNull Plugin plugin, int ticks) {
        return handle.addAttachment(plugin, ticks);
    }

    @Override
    public void removeAttachment(@NotNull PermissionAttachment attachment) {
        handle.removeAttachment(attachment);
    }

    @Override
    public void recalculatePermissions() {
        handle.recalculatePermissions();
    }

    @Override
    public @NotNull Set<PermissionAttachmentInfo> getEffectivePermissions() {
        return handle.getEffectivePermissions();
    }

    @Override
    public boolean isOp() {
        return handle.isOp();
    }

    @Override
    public void setOp(boolean value) {
        handle.setOp(value);
    }

    public @Nullable Component customName() {
        return handle.customName();
    }

    public void customName(@Nullable Component customName) {
        handle.customName(customName);
    }

    @Nullable
    public String getCustomName() {
        return handle.getCustomName();
    }

    public void setCustomName(@Nullable String name) {
        handle.setCustomName(name);
    }

    public @NotNull PersistentDataContainer getPersistentDataContainer() {
        return handle.getPersistentDataContainer();
    }

    public static @org.checkerframework.checker.nullness.qual.Nullable <V> HoverEvent<V> unbox(@org.checkerframework.checker.nullness.qual.Nullable HoverEventSource<V> source) {
        return HoverEventSource.unbox(source);
    }

    public @NonNull HoverEvent<HoverEvent.ShowEntity> asHoverEvent() {
        return handle.asHoverEvent();
    }

    public <T extends Projectile> @NotNull T launchProjectile(@NotNull Class<? extends T> projectile) {
        return handle.launchProjectile(projectile);
    }

    public <T extends Projectile> @NotNull T launchProjectile(@NotNull Class<? extends T> projectile, @Nullable Vector velocity) {
        return handle.launchProjectile(projectile, velocity);
    }

    public boolean isConversing() {
        return handle.isConversing();
    }

    public void acceptConversationInput(@NotNull String input) {
        handle.acceptConversationInput(input);
    }

    public boolean beginConversation(@NotNull Conversation conversation) {
        return handle.beginConversation(conversation);
    }

    public void abandonConversation(@NotNull Conversation conversation) {
        handle.abandonConversation(conversation);
    }

    public void abandonConversation(@NotNull Conversation conversation, @NotNull ConversationAbandonedEvent details) {
        handle.abandonConversation(conversation, details);
    }

    public void sendRawMessage(@Nullable UUID sender, @NotNull String message) {
        handle.sendRawMessage(sender, message);
    }

    public boolean isOnline() {
        return handle.isOnline();
    }

    public boolean isBanned() {
        return handle.isBanned();
    }

    public @NotNull BanEntry banPlayer(@Nullable String reason) {
        return handle.banPlayer(reason);
    }

    public @NotNull BanEntry banPlayer(@Nullable String reason, @Nullable String source) {
        return handle.banPlayer(reason, source);
    }

    public @NotNull BanEntry banPlayer(@Nullable String reason, @Nullable Date expires) {
        return handle.banPlayer(reason, expires);
    }

    public @NotNull BanEntry banPlayer(@Nullable String reason, @Nullable Date expires, @Nullable String source) {
        return handle.banPlayer(reason, expires, source);
    }

    public @NotNull BanEntry banPlayer(@Nullable String reason, @Nullable Date expires, @Nullable String source, boolean kickIfOnline) {
        return handle.banPlayer(reason, expires, source, kickIfOnline);
    }

    public boolean isWhitelisted() {
        return handle.isWhitelisted();
    }

    public void setWhitelisted(boolean value) {
        handle.setWhitelisted(value);
    }

    @Nullable
    public Player getPlayer() {
        return handle.getPlayer();
    }

    public long getFirstPlayed() {
        return handle.getFirstPlayed();
    }

    @Deprecated
    public long getLastPlayed() {
        return handle.getLastPlayed();
    }

    public boolean hasPlayedBefore() {
        return handle.hasPlayedBefore();
    }

    public long getLastLogin() {
        return handle.getLastLogin();
    }

    public long getLastSeen() {
        return handle.getLastSeen();
    }

    public void incrementStatistic(@NotNull Statistic statistic) throws IllegalArgumentException {
        handle.incrementStatistic(statistic);
    }

    public void decrementStatistic(@NotNull Statistic statistic) throws IllegalArgumentException {
        handle.decrementStatistic(statistic);
    }

    public void incrementStatistic(@NotNull Statistic statistic, int amount) throws IllegalArgumentException {
        handle.incrementStatistic(statistic, amount);
    }

    public void decrementStatistic(@NotNull Statistic statistic, int amount) throws IllegalArgumentException {
        handle.decrementStatistic(statistic, amount);
    }

    public void setStatistic(@NotNull Statistic statistic, int newValue) throws IllegalArgumentException {
        handle.setStatistic(statistic, newValue);
    }

    public int getStatistic(@NotNull Statistic statistic) throws IllegalArgumentException {
        return handle.getStatistic(statistic);
    }

    public void incrementStatistic(@NotNull Statistic statistic, @NotNull Material material) throws IllegalArgumentException {
        handle.incrementStatistic(statistic, material);
    }

    public void decrementStatistic(@NotNull Statistic statistic, @NotNull Material material) throws IllegalArgumentException {
        handle.decrementStatistic(statistic, material);
    }

    public int getStatistic(@NotNull Statistic statistic, @NotNull Material material) throws IllegalArgumentException {
        return handle.getStatistic(statistic, material);
    }

    public void incrementStatistic(@NotNull Statistic statistic, @NotNull Material material, int amount) throws IllegalArgumentException {
        handle.incrementStatistic(statistic, material, amount);
    }

    public void decrementStatistic(@NotNull Statistic statistic, @NotNull Material material, int amount) throws IllegalArgumentException {
        handle.decrementStatistic(statistic, material, amount);
    }

    public void setStatistic(@NotNull Statistic statistic, @NotNull Material material, int newValue) throws IllegalArgumentException {
        handle.setStatistic(statistic, material, newValue);
    }

    public void incrementStatistic(@NotNull Statistic statistic, @NotNull EntityType entityType) throws IllegalArgumentException {
        handle.incrementStatistic(statistic, entityType);
    }

    public void decrementStatistic(@NotNull Statistic statistic, @NotNull EntityType entityType) throws IllegalArgumentException {
        handle.decrementStatistic(statistic, entityType);
    }

    public int getStatistic(@NotNull Statistic statistic, @NotNull EntityType entityType) throws IllegalArgumentException {
        return handle.getStatistic(statistic, entityType);
    }

    public void incrementStatistic(@NotNull Statistic statistic, @NotNull EntityType entityType, int amount) throws IllegalArgumentException {
        handle.incrementStatistic(statistic, entityType, amount);
    }

    public void decrementStatistic(@NotNull Statistic statistic, @NotNull EntityType entityType, int amount) {
        handle.decrementStatistic(statistic, entityType, amount);
    }

    public void setStatistic(@NotNull Statistic statistic, @NotNull EntityType entityType, int newValue) {
        handle.setStatistic(statistic, entityType, newValue);
    }

    public @NotNull Map<String, Object> serialize() {
        return handle.serialize();
    }

    public void sendPluginMessage(@NotNull Plugin source, @NotNull String channel, @NotNull byte[] message) {
        handle.sendPluginMessage(source, channel, message);
    }

    public @NotNull Set<String> getListeningPluginChannels() {
        return handle.getListeningPluginChannels();
    }

    public int getProtocolVersion() {
        return handle.getProtocolVersion();
    }

    public @Nullable InetSocketAddress getVirtualHost() {
        return handle.getVirtualHost();
    }
}
