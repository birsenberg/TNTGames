package ru.MrSinkaaa.TNTGames.common.model.util;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class Cooldown<T> {

    private long durationMillis;
    private Map<T, Long> invocationMap;

    public Cooldown(Duration duration) {
        this(duration.toMillis());
    }

    public Cooldown(long durationMillis) {
        this.durationMillis = durationMillis;
        invocationMap = new HashMap<>();
    }

    /**
     * Checks if object can invoke an action and record it
     *
     * @param object specified object
     * @return true if can invoke, false otherwise
     */
    public boolean tryInvoke(T object) {
        long currentTime = System.currentTimeMillis();
        long lastTime = invocationMap.getOrDefault(object, -1L);

        if (currentTime - lastTime > durationMillis) {
            invocationMap.put(object, currentTime);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if object can invoke an action and record it
     *
     * @param object specified object
     * @return 0 if can invoke, otherwise returns how many millis left
     */
    public long tryInvokeLeft(T object) {
        long currentTime = System.currentTimeMillis();
        long lastTime = invocationMap.getOrDefault(object, -1L);

        if (currentTime - lastTime > durationMillis) {
            invocationMap.put(object, currentTime);
            return 0;
        } else {
            return durationMillis - (currentTime - lastTime);
        }
    }

    /**
     * Checks if object can invoke an action
     *
     * @param object specified object
     * @return true if can invoke, false otherwise
     */
    public boolean canInvoke(T object) {
        long currentTime = System.currentTimeMillis();
        long lastTime = invocationMap.getOrDefault(object, -1L);
        return currentTime - lastTime > durationMillis;
    }

    /**
     * Checks if object can invoke an action
     *
     * @param object specified object
     * @return 0 if can invoke, otherwise returns how many millis left
     */
    public long canInvokeLeft(T object) {
        long currentTime = System.currentTimeMillis();
        long lastTime = invocationMap.getOrDefault(object, -1L);
        return durationMillis - (currentTime - lastTime);
    }

    /**
     * Reset cooldowns for all entries
     */
    public void reset() {
        invocationMap.clear();
    }
}
