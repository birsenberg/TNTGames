package ru.MrSinkaaa.TNTGames.common.misc;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

import java.util.Map;

/**
 *
 * @author MrSinkaaa
 */
public abstract class PlayerDataLoader {
    
    protected LollipopTNTGames plugin;

    public PlayerDataLoader(LollipopTNTGames plugin) {
        this.plugin = plugin;
    }
    
    protected void register() {
        GamePlayer.registerDataLoader(this::load);
    }
    
    protected abstract void load(String name, Map<Class<?>, Object> map);
}
