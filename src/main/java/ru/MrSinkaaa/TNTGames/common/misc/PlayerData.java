package ru.MrSinkaaa.TNTGames.common.misc;

/**
 *
 * @author MrSinkaaa
 */
public abstract class PlayerData {
    
    protected String playerName;
    protected Runnable updater;

    public PlayerData(String playerName) {
        this.playerName = playerName;
    }
    
    public String getPlayerName() {
        return playerName;
    }
    
    public void setUpdater(Runnable updater) {
        this.updater = updater;
    }
    
    public void update() {
        if(updater != null)
            updater.run();
    }
}
