package ru.MrSinkaaa.TNTGames.shared.config.model.optional;

import java.util.Objects;

public class OptReference<T> implements Opt {

    private boolean present;
    private T value;

    private OptReference() {}

    private OptReference(T value, boolean present) {
        this.value = value;
        this.present = present;
    }

    // getters

    @Override
    public boolean isPresent() {
        return present;
    }

    public T get() {
        return value;
    }

    public T get(T def) {
        if (!present) return def;
        else return value;
    }

    // equality

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptReference<?> that)) return false;
        return isPresent() == that.isPresent() && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isPresent(), value);
    }

    // static

    public static <T> OptReference<T> of(T value) {
        return new OptReference<T>(value, true);
    }

    public static <T> OptReference<T> empty() {
        return new OptReference<T>();
    }

    public static <T> OptReference<T> empty(T def) {
        return new OptReference<T>(def, false);
    }
}
