package ru.MrSinkaaa.TNTGames.shared.config;

import org.bukkit.plugin.Plugin;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.shared.config.serializer.Serializers;
import ua.i0xhex.lib.configurate.CommentedConfigurationNode;
import ua.i0xhex.lib.configurate.NodePath;
import ua.i0xhex.lib.configurate.serialize.TypeSerializerCollection;
import ua.i0xhex.lib.configurate.transformation.ConfigurationTransformation;
import ua.i0xhex.lib.configurate.util.MapFactories;
import ua.i0xhex.lib.configurate.yaml.NodeStyle;
import ua.i0xhex.lib.configurate.yaml.YamlConfigurationLoader;

import java.io.File;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static ru.MrSinkaaa.TNTGames.common.util.MiscUtils.sneakyThrow;

public class ConfigLoader<T extends ConfigObject> {

    private static final Plugin PLUGIN = LollipopTNTGames.getInstance();

    private Path path;
    private Class<T> type;

    private boolean save;
    private List<TypeSerializerCollection> serializers;
    private ConfigurationTransformation transformation;

    private ConfigLoader(Path path, Class<T> type) {
        this.path = path;
        this.type = type;
    }

    // func

    public T load() {
        try {
            YamlConfigurationLoader.Builder builder = YamlConfigurationLoader.builder()
                  .indent(2)
                  .nodeStyle(NodeStyle.BLOCK)
                  .path(path)
                  .defaultOptions(opts -> opts
                        .implicitInitialization(false)
                        .mapFactory(MapFactories.insertionOrdered())
                        .serializers(b -> b.registerAll(Serializers.COMMON_COLLECTION)));

            // register serializers
            if (!serializers.isEmpty()) {
                builder.defaultOptions(opts -> opts
                      .serializers(b -> {
                          for (TypeSerializerCollection collection : serializers)
                              b.registerAll(collection);
                      }));
            }

            // load
            YamlConfigurationLoader loader = builder.build();
            CommentedConfigurationNode node = loader.load();

            // transform
            if (transformation != null)
                transformation.apply(node);

            // get config object
            T config = node.get(type);

            // handle if config is empty or does not exist (because implicit init is 'false')
            if (config == null) {
                Constructor<T> constructor = type.getDeclaredConstructor();
                constructor.setAccessible(true);
                config = constructor.newInstance();
            }

            // apply generic params & actions
            config.setLoader(loader);
            config.setRootNode(node);
            config.onPostLoad();

            // save
            if (save) {
                node.set(type, config);
                if (!Files.isRegularFile(path) || Files.isWritable(path))
                    loader.save(node);
            }

            config.onPostSave();
            return config;
        } catch (Exception ex) {
            return sneakyThrow(ex);
        }
    }

    // static

    public static <T extends ConfigObject> ConfigLoader.Builder<T> builder(Class<T> type, String path) {
        return new Builder<>(type, path);
    }

    public static <T extends ConfigObject> ConfigLoader.Builder<T> builder(Class<T> type, File path) {
        return new Builder<>(type, path);
    }

    public static <T extends ConfigObject> ConfigLoader.Builder<T> builder(Class<T> type, Path path) {
        return new Builder<>(type, path);
    }

    public static <T extends ConfigObject> T load(Class<T> type, String path, TypeSerializerCollection... serializers) {
        return builder(type, path)
              .serializers(serializers)
              .build().load();
    }

    public static <T extends ConfigObject> T load(Class<T> type, File file, boolean save, TypeSerializerCollection... serializers) {
        return builder(type, file)
              .save(save)
              .serializers(serializers)
              .build().load();
    }

    public static String generateNodePath(NodePath path) {
        return generateNodePath(path, ".");
    }

    public static String generateNodePath(NodePath path, String separator) {
        return Arrays
              .stream(path.array())
              .map(String::valueOf)
              .collect(Collectors.joining(separator));
    }

    // classes : builder

    public static class Builder<T extends ConfigObject> {

        private Path path;
        private Class<T> type;

        private boolean save;
        private List<TypeSerializerCollection> serializers;
        private ConfigurationTransformation transformation;

        private Builder(Class<T> type) {
            this.type = type;
            this.save = true;
            this.serializers = new ArrayList<>();
        }

        public Builder(Class<T> type, Path path) {
            this(type);
            this.path = path;
        }

        public Builder(Class<T> type, File path) {
            this(type);
            this.path = path.toPath();
        }

        public Builder(Class<T> type, String path) {
            this(type);
            this.path = new File(PLUGIN.getDataFolder(), path).toPath();
        }

        // setters

        public Builder<T> save(boolean save) {
            this.save = save;
            return this;
        }

        public Builder<T> transformation(ConfigurationTransformation transformation) {
            this.transformation = transformation;
            return this;
        }

        public Builder<T> serializers(TypeSerializerCollection... serializers) {
            this.serializers = Arrays.asList(serializers);
            return this;
        }

        // result

        public ConfigLoader<T> build() {
            ConfigLoader<T> loader = new ConfigLoader<>(path, type);
            loader.save = save;
            loader.serializers = serializers;
            loader.transformation = transformation;
            return loader;
        }
    }

}
