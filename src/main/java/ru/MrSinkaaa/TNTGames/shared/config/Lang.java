package ru.MrSinkaaa.TNTGames.shared.config;

import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import net.kyori.adventure.title.Title;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;
import ua.i0xhex.lib.configurate.CommentedConfigurationNode;
import ua.i0xhex.lib.configurate.util.MapFactories;
import ua.i0xhex.lib.configurate.yaml.NodeStyle;
import ua.i0xhex.lib.configurate.yaml.YamlConfigurationLoader;
import ua.i0xhex.lib.geantyref.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import static ru.MrSinkaaa.TNTGames.common.util.MiscUtils.sneakyThrow;

/**
 *
 * @author MrSinkaaa
 */
public class Lang extends ConfigObject {
    
    private Map<String, String> raw = new HashMap<>();
    private Map<String, String> messages = new HashMap<>();
    private Map<String, TitleTemplate> titles = new HashMap<>();

    private Lang(Map<String, String> raw) {
        raw.forEach((key, value) -> {
            try {
                var prefix = extractPrefix(key);
                switch (prefix) {
                    case "title" -> {
                        registerMessage(key, value,
                              TitleTemplate::fromString,
                              titles);
                    }
                    default -> {
                        registerMessage(key, value,
                              (v, f) -> f.toMiniMessage(v),
                              messages);
                    }
                }
            } catch (Exception ex) {
                sneakyThrow(ex);
            }
        });
    }

    /**
     * Return true if raw message by given id empty or null
     *
     * @param id message id
     * @return true if raw message is empty or null
     */
    public boolean isEmpty(String id) {
        String message = raw.get(id);
        return message == null || message.isEmpty();
    }

    /**
     * Generate component message
     *
     * @param id message id
     * @return component message
     */
    public Component msg(String id) {
        String message = messages.get(id);
        if (message == null)
            message = generateNotFoundMessage(id, false);
        return toComponent(id, message);
    }

    /**
     * Generate component message using replacer
     *
     * @param id message id
     * @param replacer replacer
     * @return component message
     */
    public Component msg(String id, Replacer replacer) {
        String message = messages.get(id);
        if (message == null)
            message = generateNotFoundMessage(id, false);
        return toComponent(id, replacer.apply(message));
    }

    /**
     * Return raw message
     *
     * @param id message id
     * @return raw message
     */
    public String msgRaw(String id) {
        String message = raw.get(id);
        if (message == null)
            message = generateNotFoundMessage(id, true);
        return message;
    }

    /**
     * Return raw message using replacer
     *
     * @param id message id
     * @param replacer replacer
     * @return raw message
     */
    public String msgRaw(String id, Replacer replacer) {
        String message = raw.get(id);
        if (message == null)
            message = generateNotFoundMessage(id, true);
        return replacer.apply(message);
    }

    /**
     * Send message to given audience
     *
     * @param target target
     * @param id message id
     */
    public void sendMessage(Audience target, String id) {
        if (!isEmpty(id))
            target.sendMessage(msg(id));
    }

    /**
     * Send message to given audience using replacer
     *
     * @param target target
     * @param id message id
     * @param replacer replacer
     */
    public void sendMessage(Audience target, String id, Replacer replacer) {
        if (!isEmpty(id))
            target.sendMessage(msg(id, replacer));
    }

    /**
     * Return title template
     *
     * @param id message id
     * @return title template
     */
    public TitleTemplate title(String id) {
        TitleTemplate template = titles.get(id);
        if (template == null)
            template = new TitleTemplate(generateNotFoundMessage(id, false), "");
        return template;
    }

    /**
     * Return prepared title template using replacer
     *
     * @param id message id
     * @param replacer replacer
     * @return title template
     */
    public TitleTemplate title(String id, Replacer replacer) {
        TitleTemplate template = title(id);
        return template.prepare(replacer);
    }

    /**
     * Send title to given audience
     *
     * @param target audience
     * @param id message id
     */
    public void sendTitle(Audience target, String id) {
        target.showTitle(title(id).toTitle());
    }

    /**
     * Send prepared title using replacer to given audience
     *
     * @param target audience
     * @param id message id
     * @param replacer replacer
     */
    public void sendTitle(Audience target, String id, Replacer replacer) {
        target.showTitle(title(id, replacer).toTitle());
    }

    // private

    private String extractPrefix(String str) {
        int index = str.indexOf('.');
        return index == -1 ? str : str.substring(0, index);
    }

    private String extractSuffix(String str) {
        int index = str.lastIndexOf('.');
        return index == -1 ? str : str.substring(index + 1);
    }

    private <R> void registerMessage(String key, String raw,
                                     BiFunction<String, MessageFormat, R> messageConvertFunction,
                                     Map<String, R> map) {
        var suffix = extractSuffix(key);
        var format = MessageFormat.getBySuffix(suffix);

        if (format == null) {
            format = MessageFormat.LEGACY;
        } else {
            key = key.substring(0, key.lastIndexOf('.'));
        }

        R message = messageConvertFunction.apply(raw, format);
        map.put(key, message);

        this.raw.put(key, raw);
    }

    private Component toComponent(String id, String message) {
        try {
            return MiniMessage.get().deserialize(message);
        } catch (Exception ex) {
            ex.printStackTrace();
            return MiniMessage.get().deserialize(generateErrorMessage(id, false));
        }
    }

    private String generateNotFoundMessage(String id, boolean legacy) {
        if (legacy) return "§7$[" + id + "]";
        else return "<gray>$[" + id + "]";
    }

    private String generateErrorMessage(String id, boolean legacy) {
        if (legacy) return "§c$[" + id + "]";
        else return "<red>$[" + id + "]";
    }

    // static

    public static Lang load() {
        try {
            String name = "lang.yml";
            LollipopTNTGames plugin = LollipopTNTGames.getInstance();
            Path path = new File(plugin.getDataFolder(), name).toPath();

            CommentedConfigurationNode resourceNode = YamlConfigurationLoader.builder()
                  .source(() -> {
                      InputStream stream = plugin.getClass().getResourceAsStream("/" + name);
                      InputStreamReader reader = new InputStreamReader(stream);
                      return new BufferedReader(reader);
                  })
                  .build().load();

            YamlConfigurationLoader loader = YamlConfigurationLoader.builder()
                  .indent(2)
                  .nodeStyle(NodeStyle.BLOCK)
                  .path(path)
                  .defaultOptions(opts -> opts.mapFactory(MapFactories.sortedNatural()))
                  .build();

            CommentedConfigurationNode node = loader.load();
            node.mergeFrom(resourceNode);

            if (!Files.isRegularFile(path) || Files.isWritable(path))
                loader.save(node);

            Map<String, String> raw = node.get(new TypeToken<Map<String, String>>() {});
            return new Lang(raw);
        } catch (Exception ex) {
            return sneakyThrow(ex);
        }
    }

    /**
     * Convert message from legacy format to minimessage
     *
     * @param message message
     * @return message in minimessage format
     */
    public static String fromLegacy(String message) {
        return fromLegacy(message, false);
    }

    /**
     * Convert message from legacy format to minimessage
     *
     * @param message message
     * @param escape escape formatting (colors etc)
     * @return message in minimessage format
     */
    public static String fromLegacy(String message, boolean escape) {
        var result = MessageFormat.LEGACY.toMiniMessage(message);
        if (escape) result = "<pre>" + result + "</pre>";
        return result;
    }

    // classes

    public static class TitleTemplate {

        private String title;
        private String subtitle;
        private Title.Times times;

        public TitleTemplate(String title, String subtitle, Title.Times times) {
            this.title = title;
            this.subtitle = subtitle;
            this.times = times;
        }

        public TitleTemplate(String title, String subtitle) {
            this(title, subtitle, Title.DEFAULT_TIMES);
        }

        // func

        public TitleTemplate prepare(Replacer replacer) {
            String title = replacer.apply(this.title);
            String subtitle = this.subtitle != null ? replacer.apply(this.subtitle) : null;
            return new TitleTemplate(title, subtitle, times);
        }

        public @org.jetbrains.annotations.NotNull Title toTitle() {
            Component title = MiniMessage.get().parse(getTitle());
            Component subtitle = MiniMessage.get().parse(getSubtitle());
            return Title.title(title, subtitle, times);
        }

        // getters

        public String getTitle() {
            return title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public Title.Times getTimes() {
            return times;
        }

        // static

        public static TitleTemplate fromString(String value, MessageFormat format) {
            String[] data = value.split("\n");

            String title = format.toMiniMessage(data[0]);
            String subtitle = data.length > 1 ? format.toMiniMessage(data[1]) : "";

            Title.Times times;
            if (data.length > 2) {
                String[] nums = data[2].split(" ");

                int fadeIn = Integer.parseInt(nums[0]);
                int stay = Integer.parseInt(nums[1]);
                int fadeOut = Integer.parseInt(nums[2]);

                times = Title.Times.of(
                      Duration.ofMillis(fadeIn * 50L),
                      Duration.ofMillis(stay * 50L),
                      Duration.ofMillis(fadeOut * 50L));
            } else {
                times = Title.DEFAULT_TIMES;
            }

            return new TitleTemplate(title, subtitle, times);
        }
    }

    // classes : private

    private static enum MessageFormat {

        LEGACY("#legacy"),
        JSON("#json"),
        MINIMESSAGE("#mm");

        private String suffix;

        private MessageFormat(String suffix) {
            this.suffix = suffix;
        }

        public String getSuffix() {
            return suffix;
        }

        // func

        private String toMiniMessage(String raw) {
            switch (this) {
                case LEGACY -> {
                    Component component = LegacyComponentSerializer.legacySection().deserialize(raw);
                    return MiniMessage.get().serialize(component);
                }
                case JSON -> {
                    Component component = GsonComponentSerializer.gson().deserialize(raw);
                    return MiniMessage.get().serialize(component);
                }
                case MINIMESSAGE -> {
                    Component component = MiniMessage.get().deserialize(raw);
                    return MiniMessage.get().serialize(component);
                }
                default -> {
                    throw new RuntimeException("Unhandled message type: " + name());
                }
            }
        }

        // static

        private static Map<String, MessageFormat> MAP;

        static {
            MAP = new HashMap<>();
            for (var format : MessageFormat.values())
                MAP.put(format.getSuffix(), format);
        }

        public static MessageFormat getBySuffix(String suffix) {
            return MAP.get(suffix);
        }
    }
    
}
