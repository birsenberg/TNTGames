package ru.MrSinkaaa.TNTGames.shared.config.serializer.serializers;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.checkerframework.checker.nullness.qual.Nullable;

import ua.i0xhex.lib.configurate.ConfigurationNode;
import ua.i0xhex.lib.configurate.serialize.SerializationException;
import ua.i0xhex.lib.configurate.serialize.TypeSerializer;

public class ItemStackSerializer implements TypeSerializer<ItemStack> {

    @SuppressWarnings("unchecked")
    @Override
    public ItemStack deserialize(Type type, ConfigurationNode node)
          throws SerializationException {
        Map<String, Object> map = toStringKeyedMap((Map<Object, Object>) node.raw());

        // replace map with deserialized meta
        if (map.containsKey("meta")) {
            Map<String, Object> metaMap = (Map<String, Object>) map.get("meta");
            ItemMeta meta = (ItemMeta) ConfigurationSerialization.deserializeObject(metaMap, ItemMeta.class);
            map.put("meta", meta);
        }

        return ItemStack.deserialize(map);
    }

    @Override
    public void serialize(Type type, @Nullable ItemStack obj, ConfigurationNode node)
          throws SerializationException {
        Map<String, Object> map = obj.serialize();

        // replace meta with serialized map
        if (map.containsKey("meta")) {
            ItemMeta meta = (ItemMeta) map.get("meta");
            map.put("meta", meta.serialize());
        }

        node.set(map);
    }

    // private

    private Map<String, Object> toStringKeyedMap(Map<Object, Object> map) {
        Map<String, Object> output = new LinkedHashMap<>();
        map.forEach((k, v) -> output.put(String.valueOf(k), v));
        return output;
    }
}

