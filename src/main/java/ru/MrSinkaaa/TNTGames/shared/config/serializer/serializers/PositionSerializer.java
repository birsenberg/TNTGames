package ru.MrSinkaaa.TNTGames.shared.config.serializer.serializers;

import java.lang.reflect.Type;

import org.checkerframework.checker.nullness.qual.Nullable;


import ru.MrSinkaaa.TNTGames.common.model.Position;
import ua.i0xhex.lib.configurate.ConfigurationNode;
import ua.i0xhex.lib.configurate.serialize.SerializationException;
import ua.i0xhex.lib.configurate.serialize.TypeSerializer;

public class PositionSerializer implements TypeSerializer<Position> {

    @Override
    public Position deserialize(Type type, ConfigurationNode node)
          throws SerializationException {
        return Position.deserialize(node.getString());
    }

    @Override
    public void serialize(Type type, @Nullable Position obj, ConfigurationNode node)
          throws SerializationException {
        node.set(obj.serialize());
    }
}


