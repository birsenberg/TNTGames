package ru.MrSinkaaa.TNTGames.shared.config.model;

import com.destroystokyo.paper.Title;

import java.util.function.Function;

public class TitleTemplate {

    private String title;
    private String subtitle;

    private int fadeIn;
    private int stay;
    private int fadeOut;

    public TitleTemplate(String title, String subtitle, int fadeIn, int stay, int fadeOut) {
        this.title = title;
        this.subtitle = subtitle;

        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    //func
    public TitleTemplate prepare(Function<String, String> replacement) {
        String title = replacement.apply(this.title);
        String subtitle = this.subtitle != null ? replacement.apply(this.subtitle) : null;
        return new TitleTemplate(title,subtitle,fadeIn,stay,fadeOut);
    }

    public Title toTitle() {
        return new Title(title, subtitle, fadeIn, stay, fadeOut);
    }

    //getters
    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public int getFadeIn() {
        return fadeIn;
    }

    public int getStay() {
        return stay;
    }

    public int getFadeOut() {
        return fadeOut;
    }

    //static
    public static TitleTemplate fromString(String raw) {
        String[] data = raw.split("\n");

        String title = data[0];
        String subtitle = data.length > 1 ? data[1] : null;

        int fadeIn = Title.DEFAULT_FADE_IN;
        int stay = Title.DEFAULT_STAY;
        int fadeOut = Title.DEFAULT_FADE_OUT;

        if(data.length > 2) {
            String[] nums = data[2].split(" ");

            fadeIn = Integer.parseInt(nums[0]);
            stay = Integer.parseInt(nums[1]);
            fadeOut = Integer.parseInt(nums[2]);
        }

        return new TitleTemplate(title, subtitle, fadeIn, stay, fadeOut);
    }
}
