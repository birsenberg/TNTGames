package ru.MrSinkaaa.TNTGames.shared.config.serializer.serializers;

import java.lang.reflect.Type;

import org.checkerframework.checker.nullness.qual.Nullable;


import ru.MrSinkaaa.TNTGames.common.model.TickDuration;
import ua.i0xhex.lib.configurate.ConfigurationNode;
import ua.i0xhex.lib.configurate.serialize.SerializationException;
import ua.i0xhex.lib.configurate.serialize.TypeSerializer;

public class TickDurationSerializer implements TypeSerializer<TickDuration> {

    @Override
    public TickDuration deserialize(Type type, ConfigurationNode node)
          throws SerializationException {
        return TickDuration.fromSeconds(node.getDouble());
    }

    @Override
    public void serialize(Type type, @Nullable TickDuration obj, ConfigurationNode node)
          throws SerializationException {
        node.set(obj.getSeconds());
    }
}

