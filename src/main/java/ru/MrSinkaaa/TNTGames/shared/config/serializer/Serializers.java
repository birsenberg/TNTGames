package ru.MrSinkaaa.TNTGames.shared.config.serializer;

import org.bukkit.inventory.ItemStack;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.common.model.TickDuration;
import ru.MrSinkaaa.TNTGames.shared.config.model.optional.Opt;
import ru.MrSinkaaa.TNTGames.shared.config.serializer.serializers.*;
import ua.i0xhex.lib.configurate.serialize.TypeSerializerCollection;

import java.time.Duration;

public class Serializers {

    public final static TypeSerializerCollection COMMON_COLLECTION;

    static {
        COMMON_COLLECTION = TypeSerializerCollection.builder()
              .register(Opt.class, new OptSerializer())
              .register(Position.class, new PositionSerializer())
              .register(BlockPosition.class, new BlockPositionSerializer())
              .register(TickDuration.class, new TickDurationSerializer())
              .register(Duration.class, new DurationSerializer())
              .register(ItemStack.class, new ItemStackSerializer())
              .build();
    }
}
