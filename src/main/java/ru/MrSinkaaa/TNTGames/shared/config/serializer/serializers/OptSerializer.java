package ru.MrSinkaaa.TNTGames.shared.config.serializer.serializers;

import org.checkerframework.checker.nullness.qual.Nullable;
import ru.MrSinkaaa.TNTGames.shared.config.model.optional.*;
import ua.i0xhex.lib.configurate.ConfigurationNode;
import ua.i0xhex.lib.configurate.serialize.SerializationException;
import ua.i0xhex.lib.configurate.serialize.TypeSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class OptSerializer implements TypeSerializer<Opt> {

    @Override
    public Opt deserialize(Type type, ConfigurationNode node)
          throws SerializationException {
        if (type instanceof ParameterizedType ptype) {
            var typeName = ((Class<?>) ptype.getRawType()).getSimpleName();
            switch (typeName) {
                case "OptReference" -> {
                    var gtype = ptype.getActualTypeArguments()[0];
                    return OptReference.of(node.get(gtype));
                }
                default -> {
                    throw new IllegalArgumentException("Unknown type: " + typeName);
                }
            }
        } else {
            var typeName = ((Class<?>) type).getSimpleName();
            return switch (typeName) {
                case "OptInt" -> OptInt.of(node.getInt());
                case "OptLong" -> OptLong.of(node.getLong());
                case "OptFloat" -> OptFloat.of(node.getFloat());
                case "OptDouble" -> OptDouble.of(node.getDouble());
                case "OptBoolean" -> OptBoolean.of(node.getBoolean());
                case "OptReference" -> OptReference.of(node.rawScalar());
                default -> throw new IllegalArgumentException("Unknown type: " + typeName);
            };
        }
    }

    @Override
    public void serialize(Type type, @Nullable Opt obj, ConfigurationNode node)
          throws SerializationException {
        if (!obj.isPresent()) {
            node.set(null);
        } else {
            if (obj instanceof OptInt o) {
                node.set(o.get());
            } else if (obj instanceof OptLong o) {
                node.set(o.get());
            } else if (obj instanceof OptFloat o) {
                node.set(o.get());
            } else if (obj instanceof OptDouble o) {
                node.set(o.get());
            } else if (obj instanceof OptBoolean o) {
                node.set(o.get());
            } else if (obj instanceof OptReference o) {
                node.set(o.get());
            } else {
                throw new IllegalArgumentException("Unknown type: " + type.getTypeName());
            }
        }
    }
}
