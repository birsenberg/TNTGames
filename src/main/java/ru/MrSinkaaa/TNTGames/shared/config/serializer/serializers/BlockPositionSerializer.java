package ru.MrSinkaaa.TNTGames.shared.config.serializer.serializers;

import org.checkerframework.checker.nullness.qual.Nullable;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ua.i0xhex.lib.configurate.ConfigurationNode;
import ua.i0xhex.lib.configurate.serialize.SerializationException;
import ua.i0xhex.lib.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public class BlockPositionSerializer implements TypeSerializer<BlockPosition> {

    @Override
    public BlockPosition deserialize(Type type, ConfigurationNode node)
          throws SerializationException {
        return BlockPosition.deserialize(node.getString());
    }

    @Override
    public void serialize(Type type, @Nullable BlockPosition obj, ConfigurationNode node)
          throws SerializationException {
        node.set(obj.serialize());
    }
}
