package ru.MrSinkaaa.TNTGames.shared.command;

import org.bukkit.command.CommandSender;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ua.i0xhex.lib.command.Command;
import ua.i0xhex.lib.command.CommandManager;

/**
 *
 * @author MrSinkaaa
 */
public abstract class SubCmd {
    
    protected LollipopTNTGames plugin;
    
    protected CommandManager<CommandSender> manager;
    protected Command.Builder<CommandSender> builder;
    
    public SubCmd(LollipopTNTGames plugin, CommandManager<CommandSender> manager, Command.Builder<CommandSender> builder) {
        this.plugin = plugin;
        this.manager = manager;
        this.builder = builder;
        construct();
    }
    
    public abstract void construct();
}
