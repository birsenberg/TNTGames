package ru.MrSinkaaa.TNTGames.shared.command;

import org.bukkit.command.CommandSender;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ua.i0xhex.lib.command.Command;
import ua.i0xhex.lib.command.CommandManager;

/**
 *
 * @author MrSinkaaa
 */
public abstract class Cmd {
    
    protected LollipopTNTGames plugin;
    
    protected CommandManager<CommandSender> manager;
    protected Command.Builder<CommandSender> builder;
    
    public Cmd(LollipopTNTGames plugin, CommandManager<CommandSender> manager, String name, String... aliases) {
        this.plugin = plugin;
        this.manager = manager;
        this.builder = manager.commandBuilder(name, aliases);
        construct();
    }
    
    public abstract void construct();
}
