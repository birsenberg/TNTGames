package ru.MrSinkaaa.TNTGames.shared.game.settings;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.config.AbstractConfig;

import java.util.ArrayList;
import java.util.List;

public class GameConfig extends AbstractConfig {

    public String name;
    public String worldName;

    public BlockPosition[] bounds;
    public int minPlayers;
    public int maxPlayers;
    public SpawnSection spawnSection;
    public LobbySection lobby;
    public WorldBorderSection worldBorder;

    private GameConfig(JavaPlugin plugin, String id, boolean read) {
        super(plugin, "games/" + id + ".yml", false);
        if(read) read();
    }

    //func
    public void write() {
        ConfigurationSection section;

        //write
        config.set("name", name);
        config.set("world", worldName);

        //write : bounds
        config.set("bounds", String.format(
              "%d %d %d -> %d %d %d",
                bounds[0].getX(), bounds[0].getY(), bounds[0].getZ(),
                bounds[1].getX(), bounds[1].getY(), bounds[1].getZ()));

        config.set("min-players", minPlayers);
        config.set("max-players", maxPlayers);

        section = config.createSection("spawnLocations");
        spawnSection.write(section);

        List<String> lines = new ArrayList<>();
        for(Position spawn : spawnSection.spawns)
            lines.add(serializePositionNoWorld(spawn, true));
        section.set("spawns", lines);

        lobby.write(config);
        worldBorder.write(config);

        saveConfig();
    }

    //private
    private void read() {
        load();
        ConfigurationSection section;

        this.name = config.getString("name");
        this.worldName = config.getString("world");

        //read : bounds
        this.bounds = new BlockPosition[2];
        String[] data = config.getString("bounds").split(" -> ");
        this.bounds[0] = deserializeBlockPositionNoWorld(data[0], worldName);
        this.bounds[1] = deserializeBlockPositionNoWorld(data[1], worldName);

        this.minPlayers = config.getInt("min-players");
        this.maxPlayers = config.getInt("max-players");

        this.spawnSection = SpawnSection.load(this,config);
        this.spawnSection.spawns = new ArrayList<Position>();
        section = config.getConfigurationSection("spawnLocations");
        for(String raw : section.getStringList("spawns")) {

            Position pos = deserializePositionNoWorld(raw, worldName);
            if(pos == null) {
                MiscUtils.warning("Incorrect spawn location %s", raw);
                continue;
            }

            this.spawnSection.spawns.add(pos);
        }

        this.lobby = LobbySection.load(this, config);
        this.worldBorder = WorldBorderSection.load(this, config);
    }

    //static

    /**
     * @param id id
     * @param worldName world name
     * @return game config
     */
    public static GameConfig create(String id, String worldName) {
        GameConfig config = new GameConfig(LollipopTNTGames.getInstance(), id, false);
        config.worldName = worldName;
        return config;
    }

    /**
     * Load config from file by id.
     * /games/{id}.yml
     *
     * @param id id
     * @return game config
     */
    public static GameConfig load(String id) {
        return new GameConfig(LollipopTNTGames.getInstance(), id, true);
    }

    private static String serializePositionNoWorld(Position position, boolean includeYawPitch) {
        String result = position.serialize(includeYawPitch);
        return result.substring(result.indexOf(' ') + 1);
    }

    private static String serializeBlockPositionNoWorld(BlockPosition position) {
        String result = position.serialize();
        return result.substring(result.indexOf(' ') + 1);
    }

    private static Position deserializePositionNoWorld(String data, String worldName) {
        return Position.deserialize(worldName + " " + data);
    }

    private static BlockPosition deserializeBlockPositionNoWorld(String data, String worldName) {
        return BlockPosition.deserialize(worldName + " " + data);
    }

    //sections

    public static class SpawnSection extends Section {

        private GameConfig config;

        public List<Position> spawns;

        private SpawnSection(GameConfig config) {
            this.config = config;
        }

        private SpawnSection(GameConfig config, ConfigurationSection parent) {
            super(parent, "spawnsLocations");
            this.config = config;
            String worldName = this.config.worldName;

            this.spawns = new ArrayList<>();
            for(String raw : section.getStringList("spawns"))
                this.spawns.add(deserializePositionNoWorld(raw, worldName));
        }

        public void write(ConfigurationSection parent) {
            section = parent.createSection("spawnsLocations");

            List<String> lines = new ArrayList<>();
            for(Position position : spawns)
                lines.add(serializePositionNoWorld(position, true));
            section.set("spawns", lines);
        }

        //static
        public static SpawnSection create(GameConfig config) {
            return new SpawnSection(config);
        }

        public static SpawnSection load(GameConfig config, ConfigurationSection parent) {
            return new SpawnSection(config, parent);
        }
    }

    public static class LobbySection extends Section {

        private GameConfig config;

        public Position spawn;

        private LobbySection(GameConfig config) {
            this.config = config;
        }

        private LobbySection(GameConfig config, ConfigurationSection parent) {
            super(parent, "lobby");
            this.config = config;
            String worldName = this.config.worldName;

            spawn = deserializePositionNoWorld(section.getString("spawn"), worldName);
        }

        public void write(ConfigurationSection parent) {
            section = parent.createSection("lobby");

            section.set("spawn", serializePositionNoWorld(spawn, true));
        }

        //static

        public static LobbySection create(GameConfig config) {
            return new LobbySection(config);
        }

        public static LobbySection load(GameConfig config, ConfigurationSection parent) {
            return new LobbySection(config, parent);
        }
    }

    public static class WorldBorderSection extends Section {

        private GameConfig config;

        public int radiusFrom;
        public int radiusTo;

        private WorldBorderSection(GameConfig config) {
            this.config = config;
        }

        private WorldBorderSection(GameConfig config, ConfigurationSection parent) {
            super(parent, "world-border");
            this.config = config;

            this.radiusFrom = section.getInt("radius-from");
            this.radiusTo = section.getInt("radius-to");
        }

        public void write(ConfigurationSection parent) {
            section = parent.createSection("world-border");

            section.set("radius-from", radiusFrom);
            section.set("radius-to", radiusTo);
        }

        // static

        public static WorldBorderSection create(GameConfig config) {
            return new WorldBorderSection(config);
        }

        public static WorldBorderSection load(GameConfig config, ConfigurationSection parent) {
            return new WorldBorderSection(config, parent);
        }
    }
}
