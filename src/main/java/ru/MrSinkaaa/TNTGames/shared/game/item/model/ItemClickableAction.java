package ru.MrSinkaaa.TNTGames.shared.game.item.model;

import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.MrSinkaaa.TNTGames.common.action.Action;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.item.ItemInfo;

public class ItemClickableAction extends ItemClickable {

    private Action action;

    public ItemClickableAction(ItemInfo info) {
        super(info);
        ConfigurationSection properties = info.getProperties();
        action = Action.fromConfig(properties.getConfigurationSection("custom-action"));
    }

    @Override
    protected void onRightClick(PlayerInteractEvent e, GamePlayer player, Block block) {
        e.setCancelled(true);
        if(action != null)
            action.execute(player);
    }

}
