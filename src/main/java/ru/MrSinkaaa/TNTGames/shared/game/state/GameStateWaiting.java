package ru.MrSinkaaa.TNTGames.shared.game.state;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.debug.DebugType;
import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.common.util.InvUtils;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.common.util.model.TickTimer;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.config.configs.Config;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.GameState;
import ru.MrSinkaaa.TNTGames.shared.game.item.ItemClickableManager;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettings;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.waiting.StatePlayerListener;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.waiting.StateWorldListener;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardManager;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardTemplate;

import java.util.List;
import java.util.Set;

import static ru.MrSinkaaa.TNTGames.LollipopTNTGames.debugInfo;

public class GameStateWaiting extends GameState {

    private SideboardTemplate sideboardTemplate;
    private List<BukkitListener> listeners = List.of(
          new StatePlayerListener(this),
          new StateWorldListener(this));

    private TickTimer startTimer;

    public GameStateWaiting(Game game) {
        super(game);

        //cache waiting sideboard template
        Lang lang = plugin.lang();
        sideboardTemplate = new SideboardTemplate(lang.msgRaw("misc.sideboard.waiting.template"));

        //setup start timer
        int ticksStart = 60 * 20;
        startTimer = new TickTimer(plugin, ticksStart);
        startTimer.addTriggerPerSecond(this::triggerStartTimerSecond);
    }

    //func
    @Override
    public void start() {
        debugInfo(DebugType.STATE, "waiting(start): '%s' %d", game.getSettings().getName(), Game.ID);

        super.start();

        Lang lang = plugin.lang();
        GameSettings settings = game.getSettings();

        //set difficulty
        game.getWorld().setDifficulty(Difficulty.PEACEFUL);

        //add current online players & kick others
        int joined = 0;
        int max = settings.getMaxPlayers();

        for(Player p : Bukkit.getOnlinePlayers()) {
            GamePlayer player = manager.getPlayer(p);
            if(joined >= max) {
                player.kick(lang.msg("misc.kick.full"));
            } else {
                player.teleport(game.getLobbyLocation());
                processPlayerJoin(player);
            }
        }
    }

    @Override
    public void stop() {
        debugInfo(DebugType.STATE, "waiting(stop): '%s' %d", game.getSettings().getName(), Game.ID);

        super.stop();
        startTimer.stop();
    }

    public void processPlayerJoin(GamePlayer player) {
        Config config = plugin.config();
        Lang lang = plugin.lang();

        //add to the game
        game.addPlayer(player);

        //prepare player state
        resetPlayerState(player);
        player.setGameMode(GameMode.ADVENTURE);

        //unhide players
        for(GamePlayer p : game.getPlayers()) {
            if(p == player) continue;
            p.showPlayer(plugin, player);
            player.showPlayer(plugin, p);
        }

        //give lobby items
        PlayerInventory inventory = player.getInventory();
        ItemClickableManager itemClickableManager = plugin.getManager(ItemClickableManager.class);
        config.lobbyItems.forEach((id, slot) -> {
            ItemClickable clickable = itemClickableManager.getItem(id);
            if(clickable != null) {
                ItemStack item = clickable.createItem();
                inventory.setItem(slot, item);
            }
        });

        //broadcast message to chat
        Set<GamePlayer> players = game.getPlayers();
        int online = players.size();
        int max = game.getSettings().getMaxPlayers();
        for(GamePlayer p : players) {
            lang.sendMessage(p,
                  "chat.notify.waiting.player-joined",
                  new StaticReplacer().set("player", player.getName()).set("online", online).set("max", max));
        }

        //handle timer
        int min = game.getSettings().getMinPlayers();
        if(online == min) {
            Sound sound = config.sounds.get("waiting-timer-start");
            if(sound != null) {
                game.getPlayers().forEach(p -> p.playSound(p.getLocation(), sound, 1.0F, 1.0F));
            }
            startTimer.start();
        }

        if(online == max) {
            int ticksStartFull = 10 * 20;
            if(startTimer.getTick() > ticksStartFull)
                startTimer.setTick(ticksStartFull);
        }

        //update sideboard
        updateSideboard();
    }

    public void processPlayerQuit(GamePlayer player) {
        Lang lang = plugin.lang();

        //remove from game
        game.removePlayer(player);

        //broadcast message to chat
        Set<GamePlayer> players = game.getPlayers();
        int online = players.size();
        int max = game.getSettings().getMaxPlayers();
        for(GamePlayer p : players) {
            lang.sendMessage(p,
                  "chat.notify.waiting.player-quit",
                  new StaticReplacer().set("player", player.getName()).set("online", online).set("max", max));
        }

        //handle timer
        int min = game.getSettings().getMinPlayers();
        if(online < min) {
            startTimer.stop();
            startTimer.reset();
        }

        //update sideboard
        updateSideboard();
    }

    //private
    private void triggerStartTimerSecond(int second) {
        //experience bar change
        for(GamePlayer player : game.getPlayers()) {
            player.setExp((float) second / 60);
            player.setLevel(second);
        }

        //broadcast message on key moments
        if(second == 60 || second == 30 || (second > 0 && second <= 5)) {
            Lang lang = plugin.lang();
            String secondsFormat = MiscUtils.getNumberEndingFormat(second,
                  lang.msgRaw("misc.format.seconds.accusative").split(" ", 3));
            Component message = lang.msg(
                  "chat.notify.waiting.start-timer-run",
                  new StaticReplacer().set("seconds", second).set("seconds_format", secondsFormat));
            game.getPlayers().forEach(p -> p.sendMessage(message));


            //title && sound on last 5 seconds
            if (second <= 5) {
                String[] titles = new String[]{"§c1", "§62", "§63", "§e4", "§a5"};
                for (GamePlayer p : game.getPlayers()) {
                    p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1.0F, 1.0F);
                    p.sendTitle(titles[second - 1], "", 5, 20, 20);
                }
            }
        }

        //actions on last second
        if(second == 0) {
            game.switchState(new GameStateRunning(game));

            Lang lang = plugin.lang();
            Component message = lang.msg("chat.notify.waiting.start-timer-done");
            game.getPlayers().forEach(player -> {
                lang.sendTitle(player, "title.notify.waiting.start-timer-done");
                player.sendMessage(message);
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F);
            });
        }
    }

    private void updateSideboard() {
        Lang lang = plugin.lang();

        String map = game.getSettings().getName();
        String playersOnline = String.valueOf(game.getPlayers().size());
        String playersMax = String.valueOf(game.getSettings().getMaxPlayers());
        String playersRequired = String.valueOf(game.getSettings().getMinPlayers());

        Replacer replacer = new StaticReplacer().set("map", map)
              .set("players_online", playersOnline)
              .set("players_max", playersMax)
              .set("players_required", playersRequired);

        SideboardManager sideboardManager = plugin.getManager(SideboardManager.class);
        sideboardManager.updateBoard(sideboardTemplate, replacer);
    }

    private void resetPlayerState(GamePlayer player) {
        InvUtils.clearInventory(player);
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
        player.setFoodLevel(20);
        player.setSaturation(5.0F);
        player.setAllowFlight(false);
        player.setFlying(false);
        player.setFlySpeed(0.1F);
        player.setFireTicks(0);

        for(PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
    }

    @Override
    protected List<BukkitListener> getListeners() {
        return listeners;
    }

}
