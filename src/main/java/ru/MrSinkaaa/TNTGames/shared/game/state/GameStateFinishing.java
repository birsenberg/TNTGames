package ru.MrSinkaaa.TNTGames.shared.game.state;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.attribute.Attribute;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.debug.DebugType;
import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.CombinedReplacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.DynamicReplacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.common.util.InvUtils;
import ru.MrSinkaaa.TNTGames.common.util.model.TickTimer;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.GameState;
import ru.MrSinkaaa.TNTGames.shared.game.misc.replacer.StaticReplacerPlayer;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.finishing.StatePlayerListener;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.finishing.StateWorldListener;
import ru.MrSinkaaa.TNTGames.shared.misc.MetadataKeys;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardManager;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardTemplate;

import java.util.List;

import static ru.MrSinkaaa.TNTGames.LollipopTNTGames.debugInfo;
import static ru.MrSinkaaa.TNTGames.common.util.MiscUtils.removeMetadata;

public class GameStateFinishing extends GameState {

    private SideboardTemplate sideboardTemplate;
    private List<BukkitListener> listeners = List.of(
          new StatePlayerListener(this),
          new StateWorldListener(this));

    private TickTimer timer;
    private GamePlayer winner;

    public GameStateFinishing(Game game, GamePlayer winner) {
        super(game);

        this.winner = winner;

        //cache sideboard template
        Lang lang = plugin.lang();
        sideboardTemplate = new SideboardTemplate(lang.msgRaw("misc.sideboard.finishing.template"));
    }

    //func
    @Override
    public void start() {
        debugInfo(DebugType.STATE, "finishing (start): '%s' %d", game.getSettings().getName(), Game.ID);

        super.start();

        //give points
        var gameType = game.getSettings().getType();
        var configRewardSection = plugin.config().reward;
//        var configPointsSection = plugin.config().points.modes.get(gameType);
        double amount = configRewardSection.amountPerMode.getOrDefault(gameType, 0D);

        if(amount > 0 && winner != null) {
//            boolean proportionalToPoints = configRewardSection.proportionalToPoints && configPointsSection != null;

            Economy economy = plugin.getEconomy();
            
        }

        // reset player's state (inventory, metadata etc.) & set fly
        for (GamePlayer player : game.getPlayers()) {
            resetPlayerState(player);
            player.setAllowFlight(true);
            player.setFlying(true);
        }

        // prepare new game after {seconds} seconds
        int seconds = 10;
        timer = new TickTimer(plugin, seconds * 20);
        timer.addTriggerPerSecond(second -> {
            updateSideboard();
            if (second == 0) {
                // reset player's internal state (kills, deaths ...)
                for (GamePlayer player : game.getPlayers())
                    player.reset();

                // start preparing new game
                game.switchState(new GameStatePreparing(game));
            }
        });
        timer.start();
    }

    @Override
    public void stop() {
        debugInfo(DebugType.STATE, "finishing (stop): '%s' %d",
              game.getSettings().getName(), Game.ID);

        super.stop();
    }

    // private

    @Override
    protected List<BukkitListener> getListeners() {
        return listeners;
    }

    private void resetPlayerState(GamePlayer player) {
        // remove all metadata
        removeMetadata(player, MetadataKeys.PLAYER_DAMAGER);
        removeMetadata(player, MetadataKeys.PLAYER_EQUIPMENT);
        removeMetadata(player, MetadataKeys.PLAYER_DAMAGE_TIME);
        removeMetadata(player, MetadataKeys.PLAYER_NO_PVP_EXPIRE_TIME);

        // other
        InvUtils.clearInventory(player);
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
        player.setFoodLevel(20);
        player.setSaturation(5.0F);
        player.setAllowFlight(false);
        player.setFlying(false);
        player.setFireTicks(0);
    }

    private void updateSideboard() {
        Lang lang = plugin.lang();

        String map = game.getSettings().getName();
        String result = (winner == null)
              ? lang.msgRaw("misc.sideboard.finishing.p.result.tie")
              : lang.msgRaw("misc.sideboard.finishing.p.result.won", new StaticReplacerPlayer(winner));
        String secondsLeft = String.valueOf(timer.getSecond());

        Replacer replacer = new CombinedReplacer(
              new StaticReplacer()
                    .set("map", map)
                    .set("result", result)
                    .set("seconds_left", secondsLeft),
              new DynamicReplacer<GamePlayer>()
                    .set("points", GamePlayer::getPoints));

        SideboardManager sideboardManager = plugin.getManager(SideboardManager.class);
        sideboardManager.updateBoard(sideboardTemplate, replacer);
    }
}
