package ru.MrSinkaaa.TNTGames.shared.game.state.listener.waiting;

import io.papermc.paper.chat.ChatRenderer;
import io.papermc.paper.event.player.AsyncChatEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.jetbrains.annotations.NotNull;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateWaiting;
import ru.MrSinkaaa.TNTGames.shared.menu.Menu;

public class StatePlayerListener extends BukkitListener {

    private GameStateWaiting state;

    public StatePlayerListener(GameStateWaiting state) {
        super(state.getPlugin());
        this.state = state;
    }

    //process join & quit

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());
        state.processPlayerJoin(player);
    }

    @EventHandler
    public void onPlayerSpawnLocation(PlayerSpawnLocationEvent e) {
        GameManager manager = plugin.getManager(GameManager.class);
        Game game = manager.getCurrentGame();

        GamePlayer player = manager.getPlayer(e.getPlayer());
        if(manager.isGameStarted())
            e.setSpawnLocation(game.getLobbyLocation());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());
        state.processPlayerQuit(player);
    }

    //chat

    @EventHandler
    public void onAsyncChat(AsyncChatEvent e) {
        Lang lang = plugin.lang();
        LegacyComponentSerializer serializer = LegacyComponentSerializer.legacySection();

        GamePlayer player = plugin.getPlayer(e.getPlayer());

        String message = serializer.serialize(e.message());
        String format = lang.msgRaw("chat.notify.chat.lobby",
              new StaticReplacer()
                    .set("player", player.getName())
                    .set("message",message));

        Component formattedMessage = serializer.deserialize(format);
        e.renderer(ChatRenderer.viewerUnaware(new ChatRenderer.ViewerUnaware() {
            @Override
            public @NotNull Component render(@NotNull Player player, @NotNull Component component, @NotNull Component component2) {
                return formattedMessage;
            }
        }));
    }

    @EventHandler
    public void onPlayerSwapHandItems(PlayerSwapHandItemsEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        //cancel if player clicks outside any menu
        Inventory inventory = e.getClickedInventory();
        if(inventory == null || !(inventory.getHolder() instanceof Menu))
            e.setCancelled(true);
    }

    //protect lobby

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        e.setCancelled(true);
    }

    //protect
    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent e) {
        e.setCancelled(true);
    }
}
