package ru.MrSinkaaa.TNTGames.shared.game;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettings;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateStopped;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateWaiting;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class Game {

    //base
    private LollipopTNTGames plugin;
    private GameSettings settings;

    //runtime

    private GameState state;
    private Set<GamePlayer> players;

    private Reference<World> world;
    private Location lobbyLocation;

    public static int ID = 0;

    public Game(LollipopTNTGames plugin, GameSettings settings) {
        ID++;

        //base
        this.plugin = plugin;
        this.settings = settings;

        //runtime
        this.players = new HashSet<>();
        this.world = new WeakReference<>(Bukkit.getWorld(settings.getWorldName()));
        this.lobbyLocation = settings.getLobby().getSpawn().toLocation();

        this.state = new GameStateStopped(this);
        this.state.start();
    }

    //func

    /**
     * Return alive players in this game
     *
     * @return stream of players
     */
    public Stream<GamePlayer> players() {
        return players.stream().filter(p -> !p.isSpectator());
    }

    //getters : base
    public LollipopTNTGames getPlugin() {
        return plugin;
    }

    public GameSettings getSettings() {
        return settings;
    }

    //getters : runtime
    public GameState getState() {
        return state;
    }

    public Set<GamePlayer> getPlayers() {
        return Collections.unmodifiableSet(players);
    }

    public World getWorld() {
        return world.get();
    }

    public Location getLobbyLocation() {
        return lobbyLocation;
    }

    //modifiers
    public boolean addPlayer(GamePlayer player) {
        player.setGame(this);
        return players.add(player);
    }

    public boolean removePlayer(GamePlayer player) {
        player.setGame(null);

        return players.remove(player);
    }

    //func
    public void start() {
        switchState(new GameStateWaiting(this));
    }

    public void stop() {
        //kick online players
        Lang lang = plugin.lang();
        Component message = lang.msg("misc.kick.stopped");
        for(Player player : Bukkit.getOnlinePlayers()) {
            player.kick(message);
        }
        //switch state
        switchState(new GameStateStopped(this));
    }

    public void switchState(GameState state) {
        this.state.stop();
        this.state = state;
        this.state.start();
    }

    public boolean isInBounds(Location location) {
        BlockPosition[] bounds = settings.getBounds();
        BlockPosition min = bounds[0];
        BlockPosition max = bounds[1];

        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();

        return x >= min.getX() && y >= -10 && z >= min.getZ()
              && x <= max.getX() && y <= max.getY() && z <= max.getZ();
    }

    /**
     * Completely invalidate (clean) this object, assuming it will never be used again
     */
    public void invalidate() {
        settings = null;

        state = null;
        players = null;
    }
}
