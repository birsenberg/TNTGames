package ru.MrSinkaaa.TNTGames.shared.game.state;

import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.debug.DebugType;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GameState;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardManager;

import java.util.Collections;
import java.util.List;

import static ru.MrSinkaaa.TNTGames.LollipopTNTGames.debugInfo;

public class GameStateStopped extends GameState {

    public GameStateStopped(Game game) {
        super(game);
    }

    //func
    @Override
    public void start() {
        debugInfo(DebugType.STATE, "stopped (start): '%s' %d", game.getSettings().getName(), Game.ID);

        super.start();

        //remove sideboard
        plugin.getManager(SideboardManager.class).removeBoard();
    }

    @Override
    public void stop() {
        debugInfo(DebugType.STATE, "stopped (stop): '%s' %d", game.getSettings().getName(), Game.ID);

        super.stop();
    }

    //private
    @Override
    protected List<BukkitListener> getListeners() {
        return Collections.emptyList();
    }
}
