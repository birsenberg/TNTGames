package ru.MrSinkaaa.TNTGames.shared.game.misc;

public class Layer {

    private final int layerY;
    private final int layerId;
    private final int layerPoints;

    public Layer(int layerId, int layerPoints, int layerY) {
        this.layerId = layerId;
        this.layerPoints = layerPoints;
        this.layerY = layerY;
    }

    public int getLayerId() {
        return this.layerId;
    }

    public int getLayerY() {
        return this.layerY;
    }

    public int getLayerPoints() {
        return this.layerPoints;
    }

}
