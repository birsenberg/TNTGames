package ru.MrSinkaaa.TNTGames.shared.game.item.items.misc;

import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.MrSinkaaa.TNTGames.common.Permission;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;
import ru.MrSinkaaa.TNTGames.shared.game.misc.SpectatorSpeed;

public class ItemSpectatorSpeed extends ItemClickable {

    public ItemSpectatorSpeed() {
        super("spectator_speed");
    }

    @Override
    protected void onRightClick(PlayerInteractEvent e, GamePlayer player, Block block) {
        super.onRightClick(e, player, block);
        Lang lang = plugin.lang();

        if(!player.hasPermission(Permission.SPECTATOR_SPEED)) {
            lang.sendMessage(player, "chat.notify.item.spectator_speed.permission");
            return;
        }

        SpectatorSpeed speed = player.getSpectatorSpeed();
        speed = speed.prev();
        String name = speed.getName();

        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1.0F, 1.0F);
        player.setSpectatorSpeed(speed);
        lang.sendTitle(player,"title.notify.running.spectator-speed", new StaticReplacer().set("speed", speed.getName()));
    }

    @Override
    protected void onLeftClick(PlayerInteractEvent e, GamePlayer player, Block block) {
        super.onLeftClick(e, player, block);
        Lang lang = plugin.lang();

        if(!player.hasPermission(Permission.SPECTATOR_SPEED)) {
            lang.sendMessage(player,"chat.notify.item.spectator_speed.permission");
            return;
        }

        SpectatorSpeed speed = player.getSpectatorSpeed();
        speed = speed.next();
        String name = speed.getName();

        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1.0F, 1.0F);
        player.setSpectatorSpeed(speed);
        lang.sendTitle(player, "title.notify.running.spectator-speed", new StaticReplacer().set("speed", speed.getName()));
    }
}
