package ru.MrSinkaaa.TNTGames.shared.game.state.listener.running;

import io.papermc.paper.chat.ChatRenderer;
import io.papermc.paper.event.player.AsyncChatEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.jetbrains.annotations.NotNull;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ru.MrSinkaaa.TNTGames.common.model.util.Cooldown;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.CombinedReplacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.config.configs.Config;
import ru.MrSinkaaa.TNTGames.shared.event.GamePlayerDeathEvent;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.item.ItemClickableManager;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;
import ru.MrSinkaaa.TNTGames.shared.game.misc.replacer.StaticReplacerPlayer;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateRunning;
import ru.MrSinkaaa.TNTGames.shared.misc.MetadataKeys;

import java.util.UUID;

import static ru.MrSinkaaa.TNTGames.common.util.MiscUtils.getMetadata;
import static ru.MrSinkaaa.TNTGames.common.util.MiscUtils.removeMetadata;

public class StatePlayerListener extends BukkitListener {

    private GameStateRunning state;
    private Cooldown<UUID> debugMoveCooldown = new Cooldown<>(500);

    public StatePlayerListener(GameStateRunning state) {
        super(state.getPlugin());
        this.state = state;
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());

        BlockPosition positionFrom = BlockPosition.fromLocation(e.getFrom());
        BlockPosition positionTo = BlockPosition.fromLocation(e.getTo());
        boolean movedOneBlock = !positionFrom.equals(positionTo);

        if (movedOneBlock) {
            Lang lang = plugin.lang();

            // check : moved under -2 vertical coord
            if (positionTo.getY() < -2) {
                e.setCancelled(true);
                if (!player.isSpectator())
                    player.damage(Integer.MAX_VALUE);
                return;
            }

            // check : moved out of arena bounds
            Game game = player.getGame();
            if (!game.isInBounds(e.getTo())) {
                // debug
                e.setCancelled(true);
                lang.sendMessage(player, "chat.notify.running.outside-bounds-move");
                return;
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());

        //filter : player is spectator
        if(player.isSpectator()) return;

        //broadcast quit message
        Lang lang = plugin.lang();
        Component message = lang.msg("chat.notify.running.player-quit",
              new CombinedReplacer(
                    new StaticReplacerPlayer(player)
              ));
        for(GamePlayer p : player.getGame().getPlayers())
            p.sendMessage(message);

        //process leave
        player.setQuitWhenAlive(true);
        player.setSpectator(true);
        state.processPlayerLeaveGame(player);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void inInventoryClickEvent(InventoryClickEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerPickupArrow(PlayerPickupArrowEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerAttemptPickupItem(PlayerAttemptPickupItemEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());

        // [temp] check : player is null
        if (player == null) {
            Player p = e.getPlayer();
            MiscUtils.warning(
                  "[PlayerAttemptPickupItemEvent] Player '%s' is not registered as GamePlayer (online = %b, valid = %b).",
                  p.getName(), p.isOnline(), p.isValid());
            e.setCancelled(true);
            return;
        }

        // check : spectator
        if (player.isSpectator()) {
            e.setCancelled(true);
            return;
        }
    }

    @EventHandler
    public void onCraftItem(CraftItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        //Check game type to allow pvp (todo)
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onAsyncChat(AsyncChatEvent e) {
        Lang lang = plugin.lang();
        LegacyComponentSerializer serializer = LegacyComponentSerializer.legacySection();

        GameManager gameManager = plugin.getManager(GameManager.class);
        GamePlayer player = gameManager.getPlayer(e.getPlayer());


        String formatMessageId;
        String message = serializer.serialize(e.message());
        if(player.isSpectator()) {
            formatMessageId = "chat.notify.chat.game-spectator";
            e.viewers().removeIf(audience -> {
                if(!(audience instanceof Player))
                    return false;
                GamePlayer recipient = gameManager.getPlayer((Player) audience);
                return !recipient.isSpectator();
            });
        } else {
            formatMessageId = "chat.notify.chat.game-global";
            e.message(serializer.deserialize(message));
        }

        String format = lang.msgRaw(formatMessageId,
              new StaticReplacer()
                    .set("player", player.getName())
                    .set("message", message));

        Component formattedMessage = serializer.deserialize(format);
        e.renderer(ChatRenderer.viewerUnaware(new ChatRenderer.ViewerUnaware() {
            @Override
            public @NotNull Component render(@NotNull Player player, @NotNull Component component, @NotNull Component component2) {
                return formattedMessage;
            }
        }));
    }

    //private
    private void handlePlayerDeath(GamePlayer player, EntityDamageEvent.DamageCause cause) {

        Lang lang = plugin.lang();

        //call event
        Bukkit.getPluginManager().callEvent(new GamePlayerDeathEvent(player));

        player.incrementDeaths();

        broadcastDeathMessage(player, cause);

        // message
        lang.sendMessage(player, "chat.notify.running.final-death");
        setSpectatorMode(player);
        state.processPlayerLeaveGame(player);

    }


    private void setSpectatorMode(GamePlayer player) {
        // set player properties
        player.setSpectator(true);

        player.setGameMode(GameMode.ADVENTURE);
        player.setCollidable(false);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY,
              Integer.MAX_VALUE, 0, false, false, false));

        // teleport
        Location locationToTeleport = state.getGame().getLobbyLocation();
        player.teleport(locationToTeleport);

        // give spectator items
        Config config = plugin.config();
        PlayerInventory inventory = player.getInventory();

        GameManager gameManager = plugin.getManager(GameManager.class);
        ItemClickableManager itemClickableManager = plugin.getManager(ItemClickableManager.class);

        config.spectatorItems.forEach((id, slot) -> {
            ItemClickable clickable = itemClickableManager.getItem(id);
            if (clickable != null) {
                ItemStack item = clickable.createItem();
                inventory.setItem(slot, item);
            }
        });

        // setup invisibility
        for (GamePlayer p : state.getGame().getPlayers()) {
            // filter : this player
            if (p == player) continue;

            if (p.isSpectator()) {
                // show spectators to this player
                player.showPlayer(plugin, p);
            } else {
                // hide this player from alive players
                p.hidePlayer(plugin, player);
            }
        }
    }

    private void broadcastDeathMessage(GamePlayer player, EntityDamageEvent.DamageCause cause) {
        Lang lang = plugin.lang();

        String victimName = player.getName();
        GamePlayer killer = getDamager(player);

        if (killer != null) {
            String killerName = killer.getName();
            switch (cause) {
                case VOID:
                    state.broadcastToAll(lang.msg(
                          "chat.notify.running.player-died.kill-void",
                          new StaticReplacer()
                                .set("killer", killerName)
                                .set("victim", victimName)));
                    break;
                default:
                    state.broadcastToAll(lang.msg(
                          "chat.notify.running.player-died.kill-base",
                          new StaticReplacer()
                                .set("killer", killerName)
                                .set("victim", victimName)));
            }
        } else {
            switch (cause) {
                case VOID:
                    state.broadcastToAll(lang.msg(
                          "chat.notify.running.player-died.void",
                          new StaticReplacer()
                                .set("victim", victimName)));
                    break;
                default:
                    state.broadcastToAll(lang.msg(
                          "chat.notify.running.player-died.base",
                          new StaticReplacer()
                                .set("victim", victimName)));
            }
        }
    }

    private GamePlayer getDamager(GamePlayer player) {
        GamePlayer damager = getMetadata(player, MetadataKeys.PLAYER_DAMAGER, GamePlayer.class);
        Long damageTime = getMetadata(player, MetadataKeys.PLAYER_DAMAGE_TIME, Long.class);
        long damageTimePassed = damager != null ? System.currentTimeMillis() - damageTime : 0L;
        long damageTimeMax = 15 * 1000L; // 15 seconds
        boolean has = damager != null && damageTimePassed <= damageTimeMax;

        return has ? damager : null;
    }

    private void removeDamager(GamePlayer player) {
        removeMetadata(player, MetadataKeys.PLAYER_DAMAGER);
    }
}
