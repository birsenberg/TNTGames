package ru.MrSinkaaa.TNTGames.shared.world;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.ChunkLoadEvent;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;

public class WorldListener extends BukkitListener {

    private WorldManager manager;

    public WorldListener(WorldManager manager) {
        super(manager.getPlugin());
        this.manager = manager;
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e) {
        Chunk chunk = e.getChunk();
        World world = chunk.getWorld();
        if(manager.isLoaded(world.getName()))
            chunk.addPluginChunkTicket(plugin);
    }
}
