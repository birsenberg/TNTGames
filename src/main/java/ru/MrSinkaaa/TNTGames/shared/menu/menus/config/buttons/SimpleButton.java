package ru.MrSinkaaa.TNTGames.shared.menu.menus.config.buttons;

import ru.MrSinkaaa.TNTGames.shared.item.ItemTemplate;
import ua.i0xhex.lib.configurate.objectmapping.ConfigSerializable;

@ConfigSerializable
public class SimpleButton {

    private int slot = 0;
    private ItemTemplate itemTemplate;

    private SimpleButton() {}

    public SimpleButton(ItemTemplate itemTemplate) {
        this.itemTemplate = itemTemplate;
    }

    public SimpleButton(ItemTemplate itemTemplate, int slot) {
        this.itemTemplate = itemTemplate;
        this.slot = slot;
    }

    //getters
    public ItemTemplate getItemTemplate() {
        return itemTemplate;
    }

    public int getSlot() {
        return slot;
    }
}
