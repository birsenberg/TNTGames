package ru.MrSinkaaa.TNTGames.shared.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.shared.menu.menus.MenuSpectatePlayers;

import java.util.LinkedList;
import java.util.List;

public class MenuManager extends Manager {

    public MenuManager(LollipopTNTGames plugin) {
        super(plugin);
    }

    @Override
    public void onEnable() {
        initMenus();
        new MenuListener(plugin).register();
    }

    @Override
    public void onDisable() {
        //close menus
        getOpenedMenus().forEach(Menu::close);
    }

    //func
    public List<Menu> getOpenedMenus() {
        List<Menu> menus = new LinkedList<>();
        for(Player player : Bukkit.getOnlinePlayers()) {
            Menu menu = getOpenedMenu(player);
            if(menu != null)
                menus.add(menu);
        }
        return menus;
    }

    public Menu getOpenedMenu(Player player) {
        InventoryView view = player.getOpenInventory();
        Inventory inventory = view.getTopInventory();
        if(inventory != null && inventory.getHolder() instanceof Menu)
            return (Menu) inventory.getHolder();
        else return null;
    }

    //private
    private void initMenus() {
        MenuSpectatePlayers.init();
    }
}
