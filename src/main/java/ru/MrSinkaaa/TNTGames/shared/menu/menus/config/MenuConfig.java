package ru.MrSinkaaa.TNTGames.shared.menu.menus.config;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.shared.config.AbstractConfig;

public abstract class MenuConfig extends AbstractConfig {

    protected LollipopTNTGames plugin;
    protected String id;

    public MenuConfig(LollipopTNTGames plugin, String id) {
        super(plugin, "configs/menu/" + id + ".yml", true);
        this.plugin = plugin;
        this.id = id;
        load();
    }

    //getters
    public String getId() {
        return id;
    }
}
