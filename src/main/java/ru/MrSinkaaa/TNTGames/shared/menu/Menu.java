package ru.MrSinkaaa.TNTGames.shared.menu;

import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public abstract class Menu implements InventoryHolder {

    protected LollipopTNTGames plugin;
    protected MenuManager manager;

    protected Menu parent;
    protected GamePlayer player;
    protected Inventory inventory;
    protected Map<Integer, Consumer<InventoryClickEvent>> buttons;

    public Menu(GamePlayer player, Menu parent) {
        this.plugin = LollipopTNTGames.getInstance();
        this.manager = plugin.getManager(MenuManager.class);

        this.parent = parent;
        this.player = player;
        this.buttons = new HashMap<>();
    }

    public Menu(GamePlayer player) {
        this(player, null);
    }

    //event
    public void onClick(InventoryClickEvent e) {
        e.setCancelled(true);
        executeButton(e);
    }

    public void onDrag(InventoryDragEvent e) {
        e.setCancelled(true);
    }

    public void onClose(InventoryCloseEvent e) { }

    //getters
    @Override
    public Inventory getInventory() {
        return inventory;
    }

    //func
    public void open() {
        createInventory();
        player.openInventory(getInventory());
    }

    public void close() {
        player.getOpenInventory().close();
    }

    //private
    protected void setButton(int slot, ItemStack item, Consumer<InventoryClickEvent> consumer) {
        inventory.setItem(slot, item);
        buttons.put(slot, consumer);
    }

    protected void clearButtons() {
        buttons.clear();
    }

    public abstract void createInventory();

    public abstract void updateInventory();

    protected boolean executeButton(InventoryClickEvent e) {
        int slot = e.getRawSlot();
        Consumer<InventoryClickEvent> consumer = buttons.get(slot);
        if(consumer != null) {
            consumer.accept(e);
            return true;
        } else return false;
    }

    protected int getSlot(int x, int y) {
        return (x - 1) + (y-1) * 9;
    }

    protected void playSound(Sound sound) {
        playSound(sound, 1.0F, 1.0F);
    }

    protected void playSound(Sound sound, float volume, float pitch) {
        player.playSound(player.getLocation(), sound, volume, pitch);
    }
}


