package ru.MrSinkaaa.TNTGames.shared.menu.menus;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.menu.Menu;
import ru.MrSinkaaa.TNTGames.shared.menu.menus.config.MenuSimpleConfig;
import ru.MrSinkaaa.TNTGames.shared.menu.menus.config.buttons.SimpleButton;

import java.util.Map;

public class MenuSpectatePlayers extends Menu {

    private static MenuSimpleConfig MCONFIG;

    public MenuSpectatePlayers(GamePlayer player) {
        super(player);
    }

    @Override
    public void createInventory() {
        //create
        Component title = LegacyComponentSerializer.legacySection().deserialize(MCONFIG.title);
        inventory = Bukkit.createInventory(this,9 * MCONFIG.lines, title);

        //items
        updateInventory();
    }

    @Override
    public void updateInventory() {
        Game game = player.getGame();

        Map<String, SimpleButton> buttons = MCONFIG.buttons;
        SimpleButton button = buttons.get("player");

        int slot = 0;
        for(GamePlayer target : game.getPlayers()) {
            if(target == player || target.isSpectator()) continue;

            String targetName = target.getName();

            ItemStack item = button.getItemTemplate().getStaticItem(new StaticReplacer()
                  .set("player_name", targetName));
            setButton(slot++, item, e -> {
                playSound(Sound.UI_BUTTON_CLICK);
                if(target.isValid() && !target.isSpectator()) {
                    close();
                    player.teleport(target.getLocation());
                }
            });
        }
    }

    public static void init() {
        MCONFIG = new MenuSimpleConfig(LollipopTNTGames.getInstance(), "spectate_players");
    }
}
