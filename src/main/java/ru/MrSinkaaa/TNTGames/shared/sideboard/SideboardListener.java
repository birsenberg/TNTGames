package ru.MrSinkaaa.TNTGames.shared.sideboard;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

public class SideboardListener extends BukkitListener {

    private SideboardManager manager;

    public SideboardListener(SideboardManager manager) {
        super(manager.getPlugin());
        this.manager = manager;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());
        manager.removeBoard(player);
    }
}
