package ru.MrSinkaaa.TNTGames.shared.sideboard;

public class SideboardTemplate {

    private String title;
    private String body;

    public SideboardTemplate(String data) {
        int index = data.indexOf('\n');
        title = data.substring(0, index);
        body = data.substring(index+1);
    }

    public SideboardTemplate(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public SideboardTemplate(SideboardTemplate other) {
        this.title = other.title;
        this.body = other.body;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
}
