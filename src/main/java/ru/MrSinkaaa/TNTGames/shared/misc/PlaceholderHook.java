package ru.MrSinkaaa.TNTGames.shared.misc;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

import java.util.Locale;

public class PlaceholderHook extends PlaceholderExpansion {

    private LollipopTNTGames plugin;

    public PlaceholderHook(LollipopTNTGames plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public String getIdentifier() {
        return "tntgames";
    }

    @Override
    public String getAuthor() {
        return "MrSinkaaa";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player p, String params) {
        GamePlayer player = plugin.getPlayer(p);
        switch (params.toLowerCase()) {
            case "tab_sort" : {
                //check : player is null
                if(player == null) return "";

                //do
                String prefixTop = "A";
                String prefixBottom = "Z";

                if(player.isSpectator()) {
                    return prefixBottom;
                } else {
                    return prefixTop;
                }
            }
            case "health": {
                // check : player is null
                if (player == null) return "";

                // do
                Lang lang = plugin.lang();
                long timeCurrent = System.currentTimeMillis();
                Long time = MiscUtils.getMetadata(player, MetadataKeys.PLAYER_DAMAGE_TIME, Long.class);

                if (time != null && timeCurrent - time < 10_000L) {
                    return lang.msgRaw("misc.player-health.active",
                          new StaticReplacer()
                                .set("health", (int) player.getHealth()));
                } else {
                    return lang.msgRaw("misc.player-health.inactive",
                          new StaticReplacer()
                                .set("health", (int) player.getHealth()));
                }
            }
            default:
                return "{ERR_PARAM}";
        }
    }
}
