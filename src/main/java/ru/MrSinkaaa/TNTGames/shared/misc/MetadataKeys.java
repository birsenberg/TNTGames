package ru.MrSinkaaa.TNTGames.shared.misc;

public class MetadataKeys {

    public static final String BLOCK_PLACED = "placed";
    public static final String BLOCK_EXPLOSION_RESISTANT = "explosion_resistant";

    public static final String PLAYER_EQUIPMENT = "equipment";
    public static final String PLAYER_DAMAGER = "damager";
    public static final String PLAYER_DAMAGE_TIME = "damage_time";
    public static final String PLAYER_NO_PVP_EXPIRE_TIME = "respawn_time";
}
