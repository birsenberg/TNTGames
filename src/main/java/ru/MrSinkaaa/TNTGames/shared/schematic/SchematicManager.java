package ru.MrSinkaaa.TNTGames.shared.schematic;

import org.bukkit.Location;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.common.util.FileUtils;

import java.io.File;

public abstract class SchematicManager extends Manager {

    public SchematicManager(LollipopTNTGames plugin) {
        super(plugin);
    }

    @Override
    public void onEnable() {
        loadSchematics();
    }

    /**
     * Paste loaded schematic
     *
     * @param path relative path (/PluginName/schematics/{path}
     * @param location location to paste to
     */
    public abstract void pasteSchematic(String path, Location location);

    //private
    private void loadSchematics() {
        File directorySchematics = new File(plugin.getDataFolder(), "schematics");
        FileUtils.createDirectoryIfNotExists(directorySchematics.toPath());
        loadSchematics(directorySchematics, directorySchematics);
    }

    private void loadSchematics(File directory, File directoryRoot) {
        File[] files = directory.listFiles();
        if(files == null) return;
        for(File file : files) {
            if(file.isFile()) loadSchematic(file, directoryRoot);
            else if (file.isDirectory()) loadSchematics(file, directoryRoot);
        }
    }

    protected abstract void loadSchematic(File file, File directoryRoot);
}
