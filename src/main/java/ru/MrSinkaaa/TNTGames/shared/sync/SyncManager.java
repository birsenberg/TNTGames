package ru.MrSinkaaa.TNTGames.shared.sync;

import org.bukkit.Bukkit;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.GameState;
import ru.MrSinkaaa.TNTGames.shared.game.misc.GameType;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettings;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateStopped;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateWaiting;
import ua.i0xhex.sync.client.bukkit.LollipopSyncClient;
import ua.i0xhex.sync.client.bukkit.modules.minigame.MinigameModule;
import ua.i0xhex.sync.client.bukkit.modules.minigame.info.server.MinigameServerState;
import ua.i0xhex.sync.client.bukkit.modules.minigame.info.server.MinigameServerUpdate;
import ua.i0xhex.sync.client.core.common.model.utility.WrappedMessagePacker;

import java.util.Locale;

public class SyncManager extends Manager {

    private boolean supported;

    public SyncManager(LollipopTNTGames plugin) {
        super(plugin);
        supported = Bukkit.getPluginManager().isPluginEnabled("LollipopSyncClient");
    }

    @Override
    public void onEnable() {
        if (supported) {
            MinigameModule module = LollipopSyncClient.get().module(MinigameModule.class);
            module.registerMinigame("tntgames",
                  this::updateMinigameInfo,
                  this::attemptToRestart,
                  this::attemptToSleep,
                  this::attemptToChangeMode);
        }
    }

    // getters

    public boolean isSupported() {
        return supported;
    }

    // private

    private void updateMinigameInfo(MinigameServerUpdate update) {
        // check current game
        GameManager manager = plugin.getManager(GameManager.class);
        Game game = manager.getCurrentGame();
        if (game == null) {
            if (manager.isGameSleeping()) {
                update.setState(MinigameServerState.SLEEP);
            } else {
                update.setState(MinigameServerState.STOPPED);
            }
            update.setMode("");
            update.setCustomData("state", WrappedMessagePacker.pack(p -> p.packString("stopped")));
            update.setCustomData("map", WrappedMessagePacker.pack(p -> p.packString("")));
            return;
        }

        // setup arena info
        GameSettings settings = game.getSettings();

        // set mode
        // TODO make configurable mode selection
        update.setMode(settings.getType().name().toLowerCase(Locale.ENGLISH));

        // set status
        GameState state = game.getState();
        if (state instanceof GameStateWaiting) {
            update.setState(MinigameServerState.WAITING);
        } else if (!(state instanceof GameStateStopped)) {
            update.setState(MinigameServerState.RUNNING);
        } else {
            update.setState(MinigameServerState.STOPPED);
        }

        // set named status
        String stateName;
        if (state instanceof GameStateWaiting) {
            if (game.getPlayers().size() > settings.getMinPlayers()) {
                stateName = "starting";
            } else {
                stateName = "waiting";
            }
        } else if (!(state instanceof GameStateStopped)) {
            stateName = "running";
        } else {
            stateName = "stopped";
        }
        update.setCustomData("state", WrappedMessagePacker.pack(p -> p.packString(stateName)));

        // set map
        String map = settings.getName();
        update.setCustomData("map", WrappedMessagePacker.pack(p -> p.packString(map)));
    }

    // private : attempt

    private boolean attemptToRestart() {
        Game game = plugin.getManager(GameManager.class).getCurrentGame();
        if (game == null) {
            restartServer();
            return true;
        } else {
            GameState state = game.getState();
            if (state instanceof GameStateStopped) {
                restartServer();
                return true;
            } else if (state instanceof GameStateWaiting) {
                int playersAmount = game.getPlayers().size();
                if (playersAmount < 3) {
                    restartServer();
                    return true;
                }
            }
        }
        return false;
    }

    private boolean attemptToSleep() {
        GameManager manager = plugin.getManager(GameManager.class);
        Game game = manager.getCurrentGame();
        if (game == null && manager.isGameSleeping()) {
            // already sleeping
            return true;
        } else {
            GameState state = game.getState();
            if (state instanceof GameStateWaiting) {
                int playersAmount = game.getPlayers().size();
                if (playersAmount < 3) {
                    sleep();
                    return true;
                }
            }
        }
        return false;
    }

    private boolean attemptToChangeMode(String mode) {
        GameType type = GameType.get(mode);
        if (type == null) return false;

        GameManager manager = plugin.getManager(GameManager.class);
        Game game = manager.getCurrentGame();
        if (game == null && manager.isGameSleeping()) {
            changeMode(type);
            return true;
        } else {
            GameState state = game.getState();
            if (state instanceof GameStateWaiting) {
                int playersAmount = game.getPlayers().size();
                if (playersAmount < 3) {
                    changeMode(type);
                    return true;
                }
            }
        }
        return false;
    }

    // private : actions

    private void restartServer() {
        Bukkit.getScheduler().runTaskLater(plugin, Bukkit::shutdown, 1L);
    }

    private void sleep() {
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            GameManager manager = plugin.getManager(GameManager.class);
            manager.sleepCurrentGame();
        }, 1L);
    }

    private void changeMode(GameType type) {
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            GameManager manager = plugin.getManager(GameManager.class);
            manager.stopCurrentGame();
            manager.startNextGame(type);
        }, 1L);
    }
}
