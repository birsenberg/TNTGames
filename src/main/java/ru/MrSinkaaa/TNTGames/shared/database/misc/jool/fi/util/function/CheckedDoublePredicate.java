package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoublePredicate;

@FunctionalInterface
public interface CheckedDoublePredicate {

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param value the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    boolean test(double value) throws Throwable;

    /**
     * @see {@link Sneaky#doublePredicate(CheckedDoublePredicate)}
     */
    static DoublePredicate sneaky(CheckedDoublePredicate predicate) {
        return Sneaky.doublePredicate(predicate);
    }

    /**
     * @see {@link Unchecked#doublePredicate(CheckedDoublePredicate)}
     */
    static DoublePredicate unchecked(CheckedDoublePredicate predicate) {
        return Unchecked.doublePredicate(predicate);
    }

    /**
     * @see {@link Unchecked#doublePredicate(CheckedDoublePredicate, Consumer)}
     */
    static DoublePredicate unchecked(CheckedDoublePredicate function, Consumer<Throwable> handler) {
        return Unchecked.doublePredicate(function, handler);
    }
}

