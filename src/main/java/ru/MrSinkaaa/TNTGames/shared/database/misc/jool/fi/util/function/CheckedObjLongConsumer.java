package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ObjLongConsumer;

@FunctionalInterface
public interface CheckedObjLongConsumer<T> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t the first input argument
     * @param value the second input argument
     */
    void accept(T t, long value) throws Throwable;

    /**
     * @see {@link Sneaky#objLongConsumer(CheckedObjLongConsumer)}
     */
    static <T> ObjLongConsumer<T> sneaky(CheckedObjLongConsumer<T> consumer) {
        return Sneaky.objLongConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#objLongConsumer(CheckedObjLongConsumer)}
     */
    static <T> ObjLongConsumer<T> unchecked(CheckedObjLongConsumer<T> consumer) {
        return Unchecked.objLongConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#objLongConsumer(CheckedObjLongConsumer, Consumer)}
     */
    static <T> ObjLongConsumer<T> unchecked(CheckedObjLongConsumer<T> consumer, Consumer<Throwable> handler) {
        return Unchecked.objLongConsumer(consumer, handler);
    }
}
