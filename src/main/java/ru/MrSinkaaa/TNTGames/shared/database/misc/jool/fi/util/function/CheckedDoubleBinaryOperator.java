package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoubleBinaryOperator;

@FunctionalInterface
public interface CheckedDoubleBinaryOperator {

    /**
     * Applies this operator to the given operands.
     *
     * @param left the first operand
     * @param right the second operand
     * @return the operator result
     */
    double applyAsDouble(double left, double right) throws Throwable;

    /**
     * @see {@link Sneaky#doubleBinaryOperator(CheckedDoubleBinaryOperator)}
     */
    static DoubleBinaryOperator sneaky(CheckedDoubleBinaryOperator operator) {
        return Sneaky.doubleBinaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#doubleBinaryOperator(CheckedDoubleBinaryOperator)}
     */
    static DoubleBinaryOperator unchecked(CheckedDoubleBinaryOperator operator) {
        return Unchecked.doubleBinaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#doubleBinaryOperator(CheckedDoubleBinaryOperator, Consumer)}
     */
    static DoubleBinaryOperator unchecked(CheckedDoubleBinaryOperator operator, Consumer<Throwable> handler) {
        return Unchecked.doubleBinaryOperator(operator, handler);
    }
}
