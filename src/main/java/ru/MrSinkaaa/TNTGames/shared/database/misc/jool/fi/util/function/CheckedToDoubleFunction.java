package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ToDoubleFunction;

@FunctionalInterface
public interface CheckedToDoubleFunction<T> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    double applyAsDouble(T value) throws Throwable;

    /**
     * @see {@link Sneaky#toDoubleFunction(CheckedToDoubleFunction)}
     */
    static <T> ToDoubleFunction<T> sneaky(CheckedToDoubleFunction<T> function) {
        return Sneaky.toDoubleFunction(function);
    }

    /**
     * @see {@link Unchecked#toDoubleFunction(CheckedToDoubleFunction)}
     */
    static <T> ToDoubleFunction<T> unchecked(CheckedToDoubleFunction<T> function) {
        return Unchecked.toDoubleFunction(function);
    }

    /**
     * @see {@link Unchecked#toDoubleFunction(CheckedToDoubleFunction, Consumer)}
     */
    static <T> ToDoubleFunction<T> unchecked(CheckedToDoubleFunction<T> function, Consumer<Throwable> handler) {
        return Unchecked.toDoubleFunction(function, handler);
    }
}

