package ru.MrSinkaaa.TNTGames.shared.database;

public class DatabaseException extends RuntimeException {

    public DatabaseException(Throwable cause) {
        super(cause);
    }

    public DatabaseException(String description) {
        super(description);
    }

    public DatabaseException(String description, Throwable cause) {
        super(description, cause);
    }
}
