package ru.MrSinkaaa.TNTGames.shared.database;

import org.bukkit.Bukkit;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function.CheckedConsumer;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function.CheckedFunction;
import ua.i0xhex.lib.hikari.HikariConfig;
import ua.i0xhex.lib.hikari.HikariDataSource;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

public abstract class Engine<T> {

    protected HikariConfig config;
    protected HikariDataSource source;

    public Engine() {

    }

    protected abstract T self();

    // getters

    public HikariConfig getConfig() {
        return config;
    }

    public HikariDataSource getDataSource() {
        return source;
    }

    public Connection getConnection() {
        try {
            return source.getConnection();
        } catch (Exception ex) {
            throw new DatabaseException("Failed to self connection.", ex);
        }
    }

    // builder

    /**
     * Set the maximum pool size
     *
     * @param maxPoolSize maximum pool size
     * @return this
     */
    public T withPoolSize(int maxPoolSize) {
        config.setMaximumPoolSize(maxPoolSize);
        return self();
    }

    /**
     * Set the connection timeout
     *
     * @param connectionTimeout connection time out in millis
     * @return this
     */
    public T withConnectionTimeout(int connectionTimeout) {
        config.setConnectionTimeout(connectionTimeout);
        return self();
    }

    /**
     * Set the maximum time to detect connection leak.<br>
     * Minimum millis allowed - 2000.
     *
     * @param millis time
     * @return this
     */
    public T withLeakDetectionThreshold(long millis) {
        config.setLeakDetectionThreshold(millis);
        return self();
    }

    /**
     * Set custom property
     *
     * @param key key
     * @param value value
     * @return this
     */
    public T withProperty(String key, String value) {
        config.addDataSourceProperty(key, value);
        return self();
    }

    // functionality

    /**
     * Open connection pool
     *
     * @return this
     */
    public T open() {
        close();
        source = new HikariDataSource(config);
        return self();
    }

    /**
     * Close connection pool
     */
    public void close() {
        if (source == null) return;
        source.close();
        source = null;
    }

    /**
     * Execute async query.<br>
     * Closes connection on finish.
     *
     * @param sql query sql
     * @param data data to replace in query sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeQuery(String sql, Object... data) {
        return executeQuery(r -> { }, sql, data);
    }

    /**
     * Execute async query.<br>
     * Closes connection on finish.
     *
     * @param function function to perform with ResultSet, can be null
     * @param sql query sql
     * @param data data to replace in query sql
     * @param <R> custom return type
     * @return future with custom return data
     */
    public <R> CompletableFuture<R> executeQuery(CheckedFunction<ResultSet, R> function, String sql, Object... data) {
        return CompletableFuture.supplyAsync(() -> {
            try (SQLResult result = executeSync(sql, data)) {
                return function == null ? null : function.apply(result.getResultSet());
            } catch (Throwable ex) {
                throw new DatabaseException("Execute query failed.", ex);
            }
        });
    }

    /**
     * Execute async query.<br>
     * Closes connection on finish.
     *
     * @param consumer consumer to perform actions with ResultSet, can be null
     * @param sql query sql
     * @param data data to replace in query sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeQuery(CheckedConsumer<ResultSet> consumer, String sql, Object... data) {
        return executeQuery(r -> {
            consumer.accept(r);
            return null;
        }, sql, data);
    }

    /**
     * Execute async query with given connection.<br>
     * Does not close connection on finish.
     *
     * @param connection given connection
     * @param sql query sql
     * @param data data to replace in query sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeQuery(Connection connection, String sql, Object... data) {
        return executeQuery(connection, r -> { }, sql, data);
    }

    /**
     * Execute async query with given connection.<br>
     * Does not close connection on finish.
     *
     * @param connection given connection
     * @param function function to perform with ResultSet, can be null
     * @param sql query sql
     * @param data data to replace in query sql
     * @param <R> custom return type
     * @return future with custom return data
     */
    public <R> CompletableFuture<R> executeQuery(Connection connection, CheckedFunction<ResultSet, R> function, String sql, Object... data) {
        return CompletableFuture.supplyAsync(() -> {
            try (SQLResult result = executeSync(connection, sql, data)) {
                return function == null ? null : function.apply(result.getResultSet());
            } catch (Throwable ex) {
                throw new DatabaseException("Execute query failed.", ex);
            }
        });
    }

    /**
     * Execute async query with given connection.<br>
     * Does not close connection on finish.
     *
     * @param connection given connection
     * @param consumer consumer to perform action with ResultSet, can be null
     * @param sql query sql
     * @param data data to replace in query sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeQuery(Connection connection, CheckedConsumer<ResultSet> consumer, String sql, Object... data) {
        return executeQuery(connection, r -> {
            consumer.accept(r);
            return null;
        }, sql, data);
    }

    /**
     * Execute async update.<br>
     * Closes connection on finish.
     *
     * @param sql update sql
     * @param data data to replace in update sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeUpdate(String sql, Object... data) {
        return executeUpdate(r -> { }, sql, data);
    }

    /**
     * Execute async update.<br>
     * Closes connection on finish.
     *
     * @param function function to perform with ResultSet, can be null
     * @param sql update sql
     * @param data data to replace in update sql
     * @param <R> custom return type
     * @return future with custom return data
     */
    public <R> CompletableFuture<R> executeUpdate(CheckedFunction<Integer, R> function, String sql, Object... data) {
        return CompletableFuture.supplyAsync(() -> {
            try (SQLResult result = executeSync(sql, data)) {
                return function == null ? null : function.apply(result.getUpdateCount());
            } catch (Throwable ex) {
                throw new DatabaseException("Execute update failed.", ex);
            }
        });
    }

    /**
     * Execute async update.<br>
     * Closes connection on finish.
     *
     * @param consumer consumer to perform action with ResultSet, can be null
     * @param sql update sql
     * @param data data to replace in update sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeUpdate(CheckedConsumer<Integer> consumer, String sql, Object... data) {
        return executeUpdate(r -> {
            consumer.accept(r);
            return null;
        }, sql, data);
    }

    /**
     * Execute async update with given connection.<br>
     * Does not close connection on finish.
     *
     * @param connection given connection
     * @param sql update sql
     * @param data data to replace in update sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeUpdate(Connection connection, String sql, Object... data) {
        return executeUpdate(connection, r -> { }, sql, data);
    }

    /**
     * Execute async update with given connection.<br>
     * Does not close connection on finish.
     *
     * @param connection given connection
     * @param function function to perform with ResultSet, can be null
     * @param sql update sql
     * @param data data to replace in update sql
     * @param <R> custom return type
     * @return future with custom return data
     */
    public <R> CompletableFuture<R> executeUpdate(Connection connection, CheckedFunction<Integer, R> function, String sql, Object... data) {
        return CompletableFuture.supplyAsync(() -> {
            try (SQLResult result = executeSync(connection, sql, data)) {
                return function == null ? null : function.apply(result.getUpdateCount());
            } catch (Throwable ex) {
                throw new DatabaseException("Execute update failed.", ex);
            }
        });
    }

    /**
     * Execute async update with given connection.<br>
     * Does not close connection on finish.
     *
     * @param connection given connection
     * @param consumer consumer to perform action with ResultSet, can be null
     * @param sql update sql
     * @param data data to replace in update sql
     * @return future with custom return data
     */
    public CompletableFuture<Void> executeUpdate(Connection connection, CheckedConsumer<Integer> consumer, String sql, Object... data) {
        return executeUpdate(connection, r -> {
            consumer.accept(r);
            return null;
        }, sql, data);
    }

    // internal

    protected SQLResult executeSync(String sql, Object ... data) {
        Connection connection = null;
        try { connection = source.getConnection(); } catch (Exception ex) { /**/ }
        return executeSync(connection, true, sql, data);
    }

    protected SQLResult executeSync(Connection connection, String sql, Object ... data) {
        return executeSync(connection, false, sql, data);
    }

    private SQLResult executeSync(Connection connection, boolean connectionCloseable, String sql, Object ... data) {
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < data.length; i++)
                statement.setObject(i + 1, data[i]);
            statement.execute();
            return new SQLResult(statement, connectionCloseable);
        } catch (Exception ex) {
            close(statement, true);

            if (connection == null) {
                throw new DatabaseException("Connection not opened.");
            }

            throw new DatabaseException(String.format("" +
                        "Unknown cause, details:\n" +
                        "Query: \"%s\"\n" +
                        "Data: \"%s\"",
                  sql, Arrays.toString(data)), ex);
        }
    }

    public static <T> T printException(Throwable ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        Bukkit.getLogger().warning(sw.toString());
        return null;
    }

    public static void close(Statement statement, boolean closeConnection) {
        try { statement.close(); } catch (Exception ex) { /**/ }
        if (closeConnection)
            try { statement.getConnection().close(); } catch (Exception ex) { /**/ }
    }

    public static void close(Connection connection) {
        try { connection.close(); } catch (Exception ex) { /**/ }
    }
}

