package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedBooleanSupplier {

    /**
     * Gets a result.
     *
     * @return a result
     */
    boolean getAsBoolean() throws Throwable;

    /**
     * @see {@link Sneaky#booleanSupplier(CheckedBooleanSupplier)}
     */
    static BooleanSupplier sneaky(CheckedBooleanSupplier supplier) {
        return Sneaky.booleanSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#booleanSupplier(CheckedBooleanSupplier)}
     */
    static BooleanSupplier unchecked(CheckedBooleanSupplier supplier) {
        return Unchecked.booleanSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#booleanSupplier(CheckedBooleanSupplier, Consumer)}
     */
    static BooleanSupplier unchecked(CheckedBooleanSupplier supplier, Consumer<Throwable> handler) {
        return Unchecked.booleanSupplier(supplier, handler);
    }
}
