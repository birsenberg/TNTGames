package ru.MrSinkaaa.TNTGames.shared.database.engine;

import ru.MrSinkaaa.TNTGames.shared.database.Engine;
import ua.i0xhex.lib.hikari.HikariConfig;

import java.io.File;

public class H2 extends Engine<H2> {

    /**
     * Creates new H2 instance.
     *
     * @param file file without extension
     */

    public H2(File file) {
        super();

        this.config = new HikariConfig();
        this.config.setJdbcUrl("jdbc:h2:" + file.getAbsolutePath());
        this.config.setDriverClassName("org.h2.Driver");
        this.config.setMaximumPoolSize(3);
        this.config.setConnectionTimeout(3000);
        this.config.setLeakDetectionThreshold(2000);
    }

    @Override
    protected H2 self() {
        return this;
    }
}
