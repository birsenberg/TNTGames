package ru.MrSinkaaa.TNTGames.shared.database.misc.jool;

import java.util.Spliterator;

@FunctionalInterface
interface FunctionalSpliterator<T> extends Spliterator<T> {

    @Override
    default Spliterator<T> trySplit() {
        return null;
    }

    @Override
    default long estimateSize() {
        return Long.MAX_VALUE;
    }

    @Override
    default int characteristics() {
        return Spliterator.ORDERED;
    }
}