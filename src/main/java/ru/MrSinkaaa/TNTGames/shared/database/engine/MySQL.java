package ru.MrSinkaaa.TNTGames.shared.database.engine;

import ru.MrSinkaaa.TNTGames.shared.database.Engine;
import ua.i0xhex.lib.hikari.HikariConfig;

public class MySQL extends Engine<MySQL> {

    /**
     * Creates new MySQL instance.
     *
     * @param url url (ex. 127.0.01:3306)
     * @param username username
     * @param password password
     */

    public MySQL(String url, String username, String password) {
        super();

        this.config = new HikariConfig();
        this.config.setJdbcUrl("jdbc:mysql://" + url);
        this.config.setUsername(username);
        this.config.setPassword(password);
        this.config.setMaximumPoolSize(3);
        this.config.setConnectionTimeout(3000);
        this.config.setLeakDetectionThreshold(2000);

        this.config.addDataSourceProperty("cachePrepStmts", "true");
        this.config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
    }

    @Override
    protected MySQL self() {
        return this;
    }
}
