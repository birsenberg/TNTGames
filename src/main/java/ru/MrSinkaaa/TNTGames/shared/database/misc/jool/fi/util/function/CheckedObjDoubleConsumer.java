package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ObjDoubleConsumer;

@FunctionalInterface
public interface CheckedObjDoubleConsumer<T> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t the first input argument
     * @param value the second input argument
     */
    void accept(T t, double value) throws Throwable;

    /**
     * @see {@link Sneaky#objDoubleConsumer(CheckedObjDoubleConsumer)}
     */
    static <T> ObjDoubleConsumer<T> sneaky(CheckedObjDoubleConsumer<T> consumer) {
        return Sneaky.objDoubleConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#objDoubleConsumer(CheckedObjDoubleConsumer)}
     */
    static <T> ObjDoubleConsumer<T> unchecked(CheckedObjDoubleConsumer<T> consumer) {
        return Unchecked.objDoubleConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#objDoubleConsumer(CheckedObjDoubleConsumer, Consumer)}
     */
    static <T> ObjDoubleConsumer<T> unchecked(CheckedObjDoubleConsumer<T> consumer, Consumer<Throwable> handler) {
        return Unchecked.objDoubleConsumer(consumer, handler);
    }
}
