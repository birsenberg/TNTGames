package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedBiConsumer<T, U> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t the first input argument
     * @param u the second input argument
     */
    void accept(T t, U u) throws Throwable;

    /**
     * @see {@link Sneaky#biConsumer(CheckedBiConsumer)}
     */
    static <T, U> BiConsumer<T, U> sneaky(CheckedBiConsumer<T, U> consumer) {
        return Sneaky.biConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#biConsumer(CheckedBiConsumer)}
     */
    static <T, U> BiConsumer<T, U> unchecked(CheckedBiConsumer<T, U> consumer) {
        return Unchecked.biConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#biConsumer(CheckedBiConsumer, Consumer)}
     */
    static <T, U> BiConsumer<T, U> unchecked(CheckedBiConsumer<T, U> consumer, Consumer<Throwable> handler) {
        return Unchecked.biConsumer(consumer, handler);
    }
}
