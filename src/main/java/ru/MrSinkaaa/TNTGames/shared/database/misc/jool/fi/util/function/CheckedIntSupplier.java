package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntSupplier;

@FunctionalInterface
public interface CheckedIntSupplier {

    /**
     * Gets a result.
     *
     * @return a result
     */
    int getAsInt() throws Throwable;

    /**
     * @see {@link Sneaky#intSupplier(CheckedIntSupplier)}
     */
    static IntSupplier sneaky(CheckedIntSupplier supplier) {
        return Sneaky.intSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#intSupplier(CheckedIntSupplier)}
     */
    static IntSupplier unchecked(CheckedIntSupplier supplier) {
        return Unchecked.intSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#intSupplier(CheckedIntSupplier, Consumer)}
     */
    static IntSupplier unchecked(CheckedIntSupplier supplier, Consumer<Throwable> handler) {
        return Unchecked.intSupplier(supplier, handler);
    }
}
