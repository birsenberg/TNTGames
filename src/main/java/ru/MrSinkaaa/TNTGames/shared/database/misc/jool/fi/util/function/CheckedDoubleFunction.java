package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoubleFunction;

@FunctionalInterface
public interface CheckedDoubleFunction<R> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    R apply(double value) throws Throwable;

    /**
     * @see {@link Sneaky#doubleFunction(CheckedDoubleFunction)}
     */
    static <R> DoubleFunction<R> sneaky(CheckedDoubleFunction<R> function) {
        return Sneaky.doubleFunction(function);
    }

    /**
     * @see {@link Unchecked#doubleFunction(CheckedDoubleFunction)}
     */
    static <R> DoubleFunction<R> unchecked(CheckedDoubleFunction<R> function) {
        return Unchecked.doubleFunction(function);
    }

    /**
     * @see {@link Unchecked#doubleFunction(CheckedDoubleFunction, Consumer)}
     */
    static <R> DoubleFunction<R> unchecked(CheckedDoubleFunction<R> function, Consumer<Throwable> handler) {
        return Unchecked.doubleFunction(function, handler);
    }
}
