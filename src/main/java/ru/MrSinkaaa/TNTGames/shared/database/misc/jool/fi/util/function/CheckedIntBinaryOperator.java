package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntBinaryOperator;

@FunctionalInterface
public interface CheckedIntBinaryOperator {

    /**
     * Applies this operator to the given operands.
     *
     * @param left the first operand
     * @param right the second operand
     * @return the operator result
     */
    int applyAsInt(int left, int right) throws Throwable;

    /**
     * @see {@link Sneaky#intBinaryOperator(CheckedIntBinaryOperator)}
     */
    static IntBinaryOperator sneaky(CheckedIntBinaryOperator operator) {
        return Sneaky.intBinaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#intBinaryOperator(CheckedIntBinaryOperator)}
     */
    static IntBinaryOperator unchecked(CheckedIntBinaryOperator operator) {
        return Unchecked.intBinaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#intBinaryOperator(CheckedIntBinaryOperator, Consumer)}
     */
    static IntBinaryOperator unchecked(CheckedIntBinaryOperator operator, Consumer<Throwable> handler) {
        return Unchecked.intBinaryOperator(operator, handler);
    }
}
