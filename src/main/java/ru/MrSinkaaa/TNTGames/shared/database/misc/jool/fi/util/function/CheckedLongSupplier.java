package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongSupplier;

@FunctionalInterface
public interface CheckedLongSupplier {

    /**
     * Gets a result.
     *
     * @return a result
     */
    long getAsLong() throws Throwable;

    /**
     * @see {@link Sneaky#longSupplier(CheckedLongSupplier)}
     */
    static LongSupplier sneaky(CheckedLongSupplier supplier) {
        return Sneaky.longSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#longSupplier(CheckedLongSupplier)}
     */
    static LongSupplier unchecked(CheckedLongSupplier supplier) {
        return Unchecked.longSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#longSupplier(CheckedLongSupplier, Consumer)}
     */
    static LongSupplier unchecked(CheckedLongSupplier supplier, Consumer<Throwable> handler) {
        return Unchecked.longSupplier(supplier, handler);
    }
}
