package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongConsumer;

@FunctionalInterface
public interface CheckedLongConsumer {

    /**
     * Performs this operation on the given argument.
     *
     * @param value the input argument
     */
    void accept(long value) throws Throwable;

    /**
     * @see {@link Sneaky#longConsumer(CheckedLongConsumer)}
     */
    static LongConsumer sneaky(CheckedLongConsumer consumer) {
        return Sneaky.longConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#longConsumer(CheckedLongConsumer)}
     */
    static LongConsumer unchecked(CheckedLongConsumer consumer) {
        return Unchecked.longConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#longConsumer(CheckedLongConsumer, Consumer)}
     */
    static LongConsumer unchecked(CheckedLongConsumer consumer, Consumer<Throwable> handler) {
        return Unchecked.longConsumer(consumer, handler);
    }
}
