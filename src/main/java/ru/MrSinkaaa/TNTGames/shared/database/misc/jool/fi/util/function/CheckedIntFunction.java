package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntFunction;

@FunctionalInterface
public interface CheckedIntFunction<R> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    R apply(int value) throws Throwable;

    /**
     * @see {@link Sneaky#intFunction(CheckedIntFunction)}
     */
    static <R> IntFunction<R> sneaky(CheckedIntFunction<R> function) {
        return Sneaky.intFunction(function);
    }

    /**
     * @see {@link Unchecked#intFunction(CheckedIntFunction)}
     */
    static <R> IntFunction<R> unchecked(CheckedIntFunction<R> function) {
        return Unchecked.intFunction(function);
    }

    /**
     * @see {@link Unchecked#intFunction(CheckedIntFunction, Consumer)}
     */
    static <R> IntFunction<R> unchecked(CheckedIntFunction<R> function, Consumer<Throwable> handler) {
        return Unchecked.intFunction(function, handler);
    }
}

