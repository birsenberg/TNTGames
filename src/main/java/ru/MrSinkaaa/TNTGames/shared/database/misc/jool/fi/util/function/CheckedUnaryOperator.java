package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.UnaryOperator;

@FunctionalInterface
public interface CheckedUnaryOperator<T> extends CheckedFunction<T, T> {

    /**
     * @see {@link Sneaky#unaryOperator(CheckedUnaryOperator)}
     */
    static <T> UnaryOperator<T> sneaky(CheckedUnaryOperator<T> operator) {
        return Sneaky.unaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#unaryOperator(CheckedUnaryOperator)}
     */
    static <T> UnaryOperator<T> unchecked(CheckedUnaryOperator<T> operator) {
        return Unchecked.unaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#unaryOperator(CheckedUnaryOperator, Consumer)}
     */
    static <T> UnaryOperator<T> unchecked(CheckedUnaryOperator<T> operator, Consumer<Throwable> handler) {
        return Unchecked.unaryOperator(operator, handler);
    }
}

