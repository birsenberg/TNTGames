package ru.MrSinkaaa.TNTGames.shared.database.misc.jool;

public class UncheckedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UncheckedException(Throwable cause) {
        super(cause);
    }
}
