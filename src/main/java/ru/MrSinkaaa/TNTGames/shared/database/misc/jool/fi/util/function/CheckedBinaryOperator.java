package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.BinaryOperator;
import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedBinaryOperator<T> extends CheckedBiFunction<T, T, T> {

    /**
     * @see {@link Sneaky#binaryOperator(CheckedBinaryOperator)}
     */
    static <T> BinaryOperator<T> sneaky(CheckedBinaryOperator<T> operator) {
        return Sneaky.binaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#binaryOperator(CheckedBinaryOperator)}
     */
    static <T> BinaryOperator<T> unchecked(CheckedBinaryOperator<T> operator) {
        return Unchecked.binaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#binaryOperator(CheckedBinaryOperator, Consumer)}
     */
    static <T> BinaryOperator<T> unchecked(CheckedBinaryOperator<T> operator, Consumer<Throwable> handler) {
        return Unchecked.binaryOperator(operator, handler);
    }
}

