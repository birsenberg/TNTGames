package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoubleConsumer;

@FunctionalInterface
public interface CheckedDoubleConsumer {

    /**
     * Performs this operation on the given argument.
     *
     * @param value the input argument
     */
    void accept(double value) throws Throwable;

    /**
     * @see {@link Sneaky#doubleConsumer(CheckedDoubleConsumer)}
     */
    static DoubleConsumer sneaky(CheckedDoubleConsumer consumer) {
        return Sneaky.doubleConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#doubleConsumer(CheckedDoubleConsumer)}
     */
    static DoubleConsumer unchecked(CheckedDoubleConsumer consumer) {
        return Unchecked.doubleConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#doubleConsumer(CheckedDoubleConsumer, Consumer)}
     */
    static DoubleConsumer unchecked(CheckedDoubleConsumer consumer, Consumer<Throwable> handler) {
        return Unchecked.doubleConsumer(consumer, handler);
    }
}

