package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ToIntFunction;

@FunctionalInterface
public interface CheckedToIntFunction<T> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    int applyAsInt(T value) throws Throwable;

    /**
     * @see {@link Sneaky#toIntFunction(CheckedToIntFunction)}
     */
    static <T> ToIntFunction<T> sneaky(CheckedToIntFunction<T> function) {
        return Sneaky.toIntFunction(function);
    }

    /**
     * @see {@link Unchecked#toIntFunction(CheckedToIntFunction)}
     */
    static <T> ToIntFunction<T> unchecked(CheckedToIntFunction<T> function) {
        return Unchecked.toIntFunction(function);
    }

    /**
     * @see {@link Unchecked#toIntFunction(CheckedToIntFunction, Consumer)}
     */
    static <T> ToIntFunction<T> unchecked(CheckedToIntFunction<T> function, Consumer<Throwable> handler) {
        return Unchecked.toIntFunction(function, handler);
    }
}
