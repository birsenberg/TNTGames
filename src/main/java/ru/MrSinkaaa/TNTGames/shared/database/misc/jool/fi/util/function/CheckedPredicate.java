package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.Predicate;

@FunctionalInterface
public interface CheckedPredicate<T> {

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param t the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    boolean test(T t) throws Throwable;

    /**
     * @see {@link Sneaky#predicate(CheckedPredicate)}
     */
    static <T> Predicate<T> sneaky(CheckedPredicate<T> predicate) {
        return Sneaky.predicate(predicate);
    }

    /**
     * @see {@link Unchecked#predicate(CheckedPredicate)}
     */
    static <T> Predicate<T> unchecked(CheckedPredicate<T> predicate) {
        return Unchecked.predicate(predicate);
    }

    /**
     * @see {@link Unchecked#predicate(CheckedPredicate, Consumer)}
     */
    static <T> Predicate<T> unchecked(CheckedPredicate<T> function, Consumer<Throwable> handler) {
        return Unchecked.predicate(function, handler);
    }
}
