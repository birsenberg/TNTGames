package ru.MrSinkaaa.TNTGames.shared.item;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;
import ru.MrSinkaaa.TNTGames.shared.config.AbstractConfig;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ItemConfig extends AbstractConfig {

    public Map<String, ItemInfo> items;

    public ItemConfig(JavaPlugin plugin, File file) {
        super(plugin, file);
        load();

        items = new HashMap<>();

        for(String id : config.getKeys(false)) {
            ConfigurationSection section = config.getConfigurationSection(id);
            ItemInfo info = ItemInfo.fromConfig(id, section);
            items.put(id, info);
        }
    }
}
