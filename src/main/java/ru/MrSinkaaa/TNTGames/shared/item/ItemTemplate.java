package ru.MrSinkaaa.TNTGames.shared.item;


import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;
import ru.MrSinkaaa.TNTGames.common.util.ItemUtils;
import ru.MrSinkaaa.TNTGames.common.util.ItemUtils.Builder;
import ua.i0xhex.lib.configurate.ConfigurationNode;
import ua.i0xhex.lib.configurate.objectmapping.ConfigSerializable;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

@ConfigSerializable
public class ItemTemplate {

    private String name;
    private String lore;
    private ItemStack item;
    private Map<String, Map<String, String>> placeholderMap;

    private ItemTemplate() {}

    public ItemTemplate(ItemStack item) {
        this.item = item;
    }

    public ItemTemplate(ItemStack item, String name, String lore) {
        this(item);
        this.name = name;
        this.lore = lore;

        if(isEmpty(name)) this.name = null;
        if(isEmpty(lore)) this.lore = null;
    }

    //func

    public void setPlaceholder(String id, String subId, String value) {
        if(placeholderMap == null) {
            placeholderMap = new LinkedHashMap<>();
        }

        placeholderMap.compute(id, (k,v) -> {
            if(v == null) v = new HashMap<>();
            v.put(subId, value);
            return v;
        });
    }

    public void setItemId(String id) {
        ItemUtils.setId(item,id);
    }

    //getters
    public Material getType() {
        return item.getType();
    }

    public ItemStack getItem() {
        return item.clone();
    }

    public String getName() {
        return name;
    }

    public String getLore() {
        return lore;
    }

    public Map<String, Map<String, String>> getPlaceholderMap() {
        return placeholderMap;
    }

    public String getPlaceholder(String id, String subId) {
        if(placeholderMap == null) return null;

        Map<String, String> map = placeholderMap.get(id);
        if(map == null) return null;
        else return map.get(subId);
    }

    public Builder getBuilder() {
        return ItemUtils.builder(item);
    }

    public Builder getBuilder(Function<String, String> replacement) {
        Builder builder = ItemUtils.builder(item);
        if(name != null) builder.setName(replacement.apply(name));
        if(lore != null) builder.setLoreBlock(replacement.apply(lore));
        return builder;
    }

    public Builder getBuilder(Replacer replacer) {
        Builder builder = ItemUtils.builder(item);
        if(name != null) builder.setName(replacer.apply(name));
        if(lore != null) builder.setLoreBlock(replacer.apply(lore));
        return builder;
    }

    /**
     * Return static item with name & lore set
     *
     * @return item
     */
    public ItemStack getStaticItem() {
        return getBuilder(s -> s).build();
    }

    /**
     * Return static item with name & lore replaced set
     *
     * @param replacement replacement function
     * @return item
     */
    public ItemStack getStaticItem(Function<String, String> replacement) {
        return getBuilder(replacement).build();
    }

    public ItemStack getStaticItem(Replacer replacer) {
        return getBuilder(replacer).build();
    }

    //setters

    public void setName(String name) {
        if(isEmpty(name)) this.name = null;
        else this.name = name;
    }

    public void setLore(String lore) {
        if(isEmpty(lore)) this.lore = null;
        else this.lore = lore;
    }

    //private
    private boolean isEmpty(String str) {
        return str != null && str.isEmpty();
    }

    //static

    public static ItemTemplate fromConfig(ConfigurationSection section) {
        Material type = Material.matchMaterial(section.getString("type", "minecraft:stone"));
        int damage = section.getInt("damage", 0);
        int amount = section.getInt("amount", 1);
        Builder builder = ItemUtils.builder(type, amount, damage);

        int customModelData = section.getInt("custom-model-data", 0);
        if(customModelData > 0) {
            builder.setCustomModelData(customModelData);
        }
        if(section.contains("enchantments")) {
            ConfigurationSection s = section.getConfigurationSection("enchantments");
            for(String name : s.getKeys(false)) {
                Enchantment enchantment = Enchantment.getByKey(NamespacedKey.minecraft(name));
                int level = s.getInt(name);
                builder.addEnchantment(enchantment, level);
            }
        }
        if(section.contains("potion-effects")) {
            ConfigurationSection sectionEffects = section.getConfigurationSection("potion-effects");
            for(String key : sectionEffects.getKeys(false)) {
                ConfigurationSection s = sectionEffects.getConfigurationSection(key);
                switch (s.getString("type", "main")) {
                    case "main": {
                        PotionType t = PotionType.valueOf(key.toUpperCase());
                        boolean extended = s.getBoolean("extended");
                        boolean upgraded = s.getBoolean("upgraded");
                        builder.setMainPotionEffect(t, extended, upgraded);
                        break;
                    }
                    case "custom": {
                        PotionEffectType t = PotionEffectType.getByName(key.toUpperCase());
                        int duration = (int) (s.getDouble("duration") + 20);
                        int amplifier = s.getInt("amplifier");
                        builder.addPotionEffect(t,duration,amplifier);
                        break;
                    }
                }
            }
        }

        String name = section.getString("name");
        String lore = section.getString("lore");
        ItemTemplate template = new ItemTemplate(builder.build(), name, lore);

        if(section.contains("placeholders")) {
            ConfigurationSection sectionPlaceholderList = section.getConfigurationSection("placeholders");
            for(String id : sectionPlaceholderList.getKeys(false)) {
                ConfigurationSection sectionPlaceholder = sectionPlaceholderList.getConfigurationSection(id);
                for(String subId : sectionPlaceholder.getKeys(false)) {
                    template.setPlaceholder(id, subId, sectionPlaceholder.getString(subId));
                }
            }
        }
        return template;
    }

    public static ItemTemplate fromNewConfig(ConfigurationNode node) {
        Material type = Material.matchMaterial(node.node("type").getString("minecraft:stone"));
        int damage = node.node("damage").getInt(0);
        int amount = node.node("amount").getInt(1);
        Builder builder = ItemUtils.builder(type, amount, damage);

        int customModelData = node.node("custom-model-data").getInt(0);
        if (customModelData > 0)
            builder.setCustomModelData(customModelData);

        if (node.hasChild("enchantments")) {
            ConfigurationNode enchantmentListNode = node.node("enchantments");
            for (ConfigurationNode enchantmentNode : enchantmentListNode.childrenList()) {
                String key = enchantmentNode.key().toString();
                Enchantment enchantment = Enchantment.getByKey(NamespacedKey.minecraft(key));
                int level = enchantmentNode.getInt();
                builder.addEnchantment(enchantment, level);
            }
        }

        if (node.hasChild("potion-effects")) {
            ConfigurationNode potionEffectListNode = node.node("potion-effects");
            for (ConfigurationNode potionEffectNode : potionEffectListNode.childrenList()) {
                String key = potionEffectNode.key().toString();
                switch (potionEffectNode.node("type").getString("main")) {
                    case "main": {
                        PotionType t = PotionType.valueOf(key.toUpperCase());
                        boolean extended = potionEffectNode.node("extended").getBoolean();
                        boolean upgraded = potionEffectNode.node("upgraded").getBoolean();
                        builder.setMainPotionEffect(t, extended, upgraded);
                        break;
                    }
                    case "custom": {
                        PotionEffectType t = PotionEffectType.getByName(key.toUpperCase());
                        int duration = (int) (potionEffectNode.node("duration").getDouble() * 20);
                        int amplifier = potionEffectNode.node("amplifier").getInt();
                        builder.addPotionEffect(t, duration, amplifier);
                        break;
                    }
                }
            }
        }

        String name = node.node("name").getString();
        String lore = node.node("lore").getString();
        ItemTemplate template = new ItemTemplate(builder.build(), name, lore);

        if (node.hasChild("placeholders")) {
            ConfigurationNode placeholderListNode = node.node("placeholders");
            for (ConfigurationNode placeholderNode : placeholderListNode.childrenList()) {
                String id = placeholderNode.key().toString();
                for (ConfigurationNode subIdNode : placeholderNode.childrenList()) {
                    String subid = subIdNode.key().toString();
                    template.setPlaceholder(id, subid, subIdNode.getString());
                }
            }
        }

        return template;
    }
}
